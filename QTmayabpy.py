import maya
import sys
__maya_term__ = sys.stdout 
__terminal__ = sys.__stdout__  # maya replaces sys.stdout
def print(*args):
	for a in args:
		__terminal__.write('%s\n'%a)
def maya_print(*args):
	for a in args:
		__maya_term__.write('%s\n'%a)
sys.path.append('/tmp')
import bpy
print('blender python module loaded:', bpy)

BMOD_TYPES = [
'BOOLEAN','NODES','REMESH', 'ARRAY', 
'DECIMATE', 'EDGE_SPLIT',
'SKIN', 'SOLIDIFY','SMOOTH',
'WAVE', 
'WIREFRAME', 'CAST',
'MESH_DEFORM', 'SHRINKWRAP', 'SIMPLE_DEFORM',
]

__TODO=[
'CURVE', 'DISPLACE',
'ARMATURE', 'HOOK', 'LATTICE',
'LAPLACIANDEFORM',
'BUILD',
 'CORRECTIVE_SMOOTH', 'LAPLACIANSMOOTH', 'SURFACE_DEFORM', 
'WARP', 'VOLUME_DISPLACE', 
#'CLOTH', 'COLLISION', 
'DYNAMIC_PAINT', 'EXPLODE', 
#'FLUID', 'OCEAN', 'PARTICLE_INSTANCE', 
#'PARTICLE_SYSTEM', 'SOFT_BODY', 
'SURFACE', 'SCREW', 'DATA_TRANSFER', 'MESH_CACHE', 'MESH_SEQUENCE_CACHE', 
'NORMAL_EDIT', 'WEIGHTED_NORMAL', 'UV_PROJECT', 'UV_WARP', 
'VERTEX_WEIGHT_EDIT', 'VERTEX_WEIGHT_MIX', 'VERTEX_WEIGHT_PROXIMITY', 
'SUBSURF', 'TRIANGULATE','BEVEL', 'MASK', 'MIRROR', 'MESH_TO_VOLUME', 'MULTIRES',
'VOLUME_TO_MESH', 'WELD', 
]

import maya.OpenMayaUI as omui
from PySide2 import QtCore, QtWidgets
from shiboken2 import wrapInstance

MWIN=None
def mayaWindow():
	global MWIN
	main_window_ptr = omui.MQtUtil.mainWindow()  ## swig
	MWIN=wrapInstance(int(main_window_ptr),QtWidgets.QWidget)
	return MWIN

###############


'''
Template class for docking a Qt widget to maya 2017+.
Author: Lior ben horin
12-1-2017
'''
import weakref
from maya.app.general.mayaMixin import *
class BroMainWindow_Dockable(MayaQWidgetDockableMixin, QMainWindow):
	DOCK_LABEL_NAME = 'no name window' # Window display name
	CONTROL_NAME = 'no_name_window' # Window unique object name
	instances = list()

	def __init__(self):
		super(BroMainWindow_Dockable, self).__init__()
		self.delete_instances()
		self.__class__.instances.append(weakref.proxy(self))
		# Not sure, but I suppose that we better keep track of instances of our window and keep Maya environment clean.
		# So we'll remove all instances before creating a new one.
		if maya.cmds.window(self.CONTROL_NAME + "WorkspaceControl", ex=True):
			print("Removing", self.CONTROL_NAME + "WorkspaceControl")
			maya.cmds.deleteUI(self.CONTROL_NAME + "WorkspaceControl")
			print("Removed", self.CONTROL_NAME + "WorkspaceControl")

		self.setAttribute(Qt.WA_DeleteOnClose, True)
		# Set object name and window title
		self.setObjectName(self.CONTROL_NAME)
		self.setWindowTitle(self.DOCK_LABEL_NAME)
		self.central_widget = QWidget()
		self.setCentralWidget(self.central_widget)
		self.main_layout = QVBoxLayout()
		self.central_widget.setLayout(self.main_layout)
		self.build_ui()

	@staticmethod
	def delete_instances():
		for ins in BroMainWindow_Dockable.instances:
			try:
				print('Delete {}'.format(ins))
			except:
				print('Window reference seems to be removed already, ignore.')
			try:
				ins.setParent(None)
				ins.deleteLater()
			except:
				# ignore the fact that the actual parent has already been deleted by Maya...
				pass
			try:
				BroMainWindow_Dockable.instances.remove(ins)
				del ins
			except:
				# Supress error
				pass

	def build_ui(self):
		"""
		This function is called at the end of window initialization and creates your actual UI.
		Override it with your UI.
		"""
		pass

MDOCK_MODS=None
def mbpy_dock_start():
	global MDOCK_MODS
	MDOCK_MODS = MayaDockBpyMods().show(dockable=True)
	MayaDockBpyRen().show(dockable=True)

def get_blender_scene():
	print('get_blender_scene')
	if 'Scene' in bpy.data.scenes:
		return bpy.data.scenes['Scene']
	else:
		return None

class MayaDockBpyRen(BroMainWindow_Dockable):
	DOCK_LABEL_NAME = 'blender render'  # Window display name
	instances = list()
	CONTROL_NAME = 'mbpy_ren_widget'  # Window unique object name
	def __init__(self):
		super(MayaDockBpyRen, self).__init__()
		self.resize(240,180)

	def ren_frame(self):
		print('blender render frame')
		bpy.ops.render.render(animation=False, write_still=True)
		print(get_blender_scene().render.filepath)

	def ren_anim(self):
		print('blender render animation')
		print(get_blender_scene().render.filepath)
		bpy.ops.render.render(animation=True, write_still=False)
		print(get_blender_scene().render.filepath)

	def build_ui(self):
		bscn = get_blender_scene()
		if bscn:
			self.main_layout.addWidget(QtWidgets.QLabel('render output path'))
			ipt = QtWidgets.QLineEdit()
			ipt.setText(bscn.render.filepath)
			self.main_layout.addWidget(ipt)

			self.main_layout.addWidget(QtWidgets.QLabel('render output format'))
			ipt = QtWidgets.QLineEdit()
			ipt.setText(bscn.render.image_settings.file_format)
			self.main_layout.addWidget(ipt)

		btn=QtWidgets.QPushButton('render frame')
		self.main_layout.addWidget(btn)
		btn.clicked.connect(self.ren_frame)
		btn=QtWidgets.QPushButton('render animation')
		self.main_layout.addWidget(btn)
		btn.clicked.connect(self.ren_anim)

		#self.menuBar = QMenuBar()
		#self.presetsMenu = self.menuBar.addMenu(("&Presets"))
		#self.saveConfigAction = QAction(("&Save Settings"), self)
		#self.presetsMenu.addAction(self.saveConfigAction)
		#self.setMenuBar(self.menuBar)

		self.statusBar = QStatusBar()
		self.statusBar.showMessage("render engine Eevee.")
		self.setStatusBar(self.statusBar)
		self.statusBar.setObjectName("statusBar")
		self.setStyleSheet("#statusBar {background-color:#faa300;color:#fff}")

###############

MWS_NAME='MayaBPY'
#class MayaBpyWindow(QtWidgets.QDialog):
#	def __init__(self,parent=mayaWindow()):
class MayaDockBpyMods(BroMainWindow_Dockable):
	DOCK_LABEL_NAME = 'blender modifiers'  # Window display name
	instances = list()
	CONTROL_NAME = 'blender_modifiers'  # Window unique object name
	def __init__(self):
		super(MayaDockBpyMods, self).__init__()
		#super(MayaBpyWindow,self).__init__(parent)
		
		self.layout = QtWidgets.QVBoxLayout()
		self.mbpy_mods_widget = QtWidgets.QWidget()
		self.main_layout.addWidget(self.mbpy_mods_widget)
		self.mbpy_mods_widget.setLayout(self.layout)

		#self.layout=self.main_layout

	def build_ui(self):
		self.setWindowTitle("blender modifiers")
		self.resize(160,420)
		#self.layout = QtWidgets.QVBoxLayout()
		#self.setLayout(self.layout)

		self.statusBar = QStatusBar()
		self.statusBar.showMessage("Status bar ready.")
		self.setStatusBar(self.statusBar)
		self.statusBar.setObjectName("statusBar")
		self.setStyleSheet("#statusBar {background-color:#faa300;color:#fff}")


		self.timer = QtCore.QTimer(self)
		self.timer.timeout.connect(self.loop)
		self.timer.start(800)
		self.selected = None
		self.blender_modifiers = {}
		#self.mk_render_tab()

	def mk_render_tab(self):
		print('making render tab')
		self.dock_render_tab= tab = mayaDock('eevee', clear=True)

	def loop(self):
		if MWIN and False:
			mpos = MWIN.pos()
			mx=mpos.x()
			my=mpos.y()
			#print(mx,my)
			if mx > 200:
				self.move(mx-240, my+80)
			else:
				self.move( (mx+MWIN.width())-10, my+80)

		s = getsel()
		#s.append(self.DOCK_LABEL_NAME)
		if len(s):
			name = s[0]
			if name != self.selected:
				self.statusBar.showMessage(name)
				self.selected=name
				self.update_selected_ui(name)

	def add_mod(self):
		if self.selected in bpy.data.objects:
			bob = bpy.data.objects[self.selected]
		else:
			bob = bpy.data.objects['Cube']

		mod = bob.modifiers.new(name=self.bmod_box.currentText(), type=self.bmod_box.currentText())
		self.blender_modifiers[self.selected].append(mod)
		self.update_selected_ui(self.selected)

	def update_selected_ui(self, name):
		#self.setWindowTitle(name)
		clearLayout(self.layout)
		btn=QtWidgets.QPushButton("add modifier")
		self.layout.addWidget(btn)
		btn.clicked.connect(self.add_mod)
		self.bmod_box = QtWidgets.QComboBox()
		self.bmod_box.addItems(BMOD_TYPES)
		self.layout.addWidget(self.bmod_box)

		if name not in self.blender_modifiers:
			self.blender_modifiers[name] = []

		for mod in self.blender_modifiers[name]:
			header = QtWidgets.QHBoxLayout()
			hwid = QtWidgets.QWidget()
			hwid.setLayout(header)

			btn=QtWidgets.QPushButton(mod.name)
			self.layout.addWidget(hwid)
			header.addWidget(btn)
			header.addStretch()

			for prop in mod.bl_rna.properties:
				if prop.identifier in 'rna_type type show_viewport show_render show_in_editmode show_on_cage is_override_data use_apply_on_spline'.split():
					continue
				if prop.is_readonly or prop.is_hidden:
					continue
				if prop.is_runtime or prop.is_skip_save:
					continue
				label=QtWidgets.QLabel(prop.identifier)
				widget = None
				if prop.type=='STRING':
					widget=QtWidgets.QLineEdit( getattr(mod,prop.identifier) )
				elif prop.type=='BOOLEAN':
					label=None
					if prop.identifier=='show_expanded':
						if mod.show_expanded:
							widget=QtWidgets.QPushButton('▶')
						else:
							widget=QtWidgets.QPushButton('◀')
					elif prop.identifier=='is_active':
						if mod.is_active:
							widget=QtWidgets.QPushButton('▣')
						else:
							widget=QtWidgets.QPushButton('▢')

					else:
						widget=QtWidgets.QPushButton(prop.identifier)
					widget.setCheckable(True)
					widget.setChecked(getattr(mod,prop.identifier))
					widget.clicked.connect(
						lambda m=mod,n=prop.identifier,w=widget: self.set_mod(m,n,w)
					)
				elif prop.type=='INT':
					widget=QtWidgets.QSpinBox()
					widget.setValue(getattr(mod,prop.identifier))
				elif prop.type=='FLOAT':
					widget=QtWidgets.QSlider(QtCore.Qt.Horizontal)  ## note ints only TODO scale to float
					widget.setRange(0,100)
					#widget.setSingleStep(1)
					v = getattr(mod,prop.identifier)
					print(v*100)
					widget.setValue( int(v*100) )
				else:
					print(prop.type)
					print(dir(prop))
					print(prop.subtype)
					continue

				if label and mod.show_expanded:
					self.layout.addWidget(label)
				if widget:
					if prop.identifier in 'is_active show_expanded'.split():
						header.addWidget(widget)
					elif mod.show_expanded:
						self.layout.addWidget(widget)

		self.layout.addItem(QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding) )

	def set_mod(self, mod, name, widget):
		setattr(mod, name, widget.isChecked())
		self.update_selected_ui(self.selected)

def clearLayout(layout):
	while layout.count():
		child = layout.takeAt(0)
		if child.widget():
			child.widget().deleteLater()

def getsel():
	return maya.cmds.ls(sl=True,long=True) or []

def mayabpy_start():
	#win = MayaBpyWindow()
	#win.show()
	mbpy_dock_start()

def notworking():
	hack=0
	for obj in MWIN.children():
		top=obj.objectName()
		if top.strip():
			print('maya toplevel widget:', top)
			if top=='workspacePanel1':
				for child in obj.children():
					if child.objectName()=='mayaLayoutInternalWidget':
						for sub in child.children():
							print(sub, sub.objectName())
							if isinstance(sub, QtWidgets.QTabWidget):
								print('FOUND QTab')
								hack += 1
								if hack==3:
									print('trying to add a tab...')
									sub.addTab(QtWidgets.QPushButton('hello'), 'world')
									print('OK?')
									break


mayabpy_start()
#show_test()
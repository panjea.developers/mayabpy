/*
* SPDX-FileCopyrightText: 1999 Matthias Elter <me@kde.org>
* SPDX-FileCopyrightText: 2002 Patrick Julien <freak@codepimps.org>
* SPDX-FileCopyrightText: 2015 Boudewijn Rempt <boud@valdyas.org>
*
*  SPDX-License-Identifier: GPL-2.0-or-later
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program; if not, write to the Free Software
*  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include <stdlib.h>

#include <QString>
#include <QPixmap>
#include <kis_debug.h>
#include <QProcess>
#include <QProcessEnvironment>
#include <QStandardPaths>
#include <QDir>
#include <QDate>
#include <QLocale>
#include <QSettings>
#include <QByteArray>
#include <QMessageBox>
#include <QThread>
#include <QLibraryInfo>
#include <QTranslator>

#include <QOperatingSystemVersion>

#include <time.h>

#include <KisApplication.h>
#include <KoConfig.h>
#include <KoResourcePaths.h>
#include <kis_config.h>

#include "KisDocument.h"
#include "kis_splash_screen.h"
#include "KisPart.h"
#include "KisApplicationArguments.h"
#include <opengl/kis_opengl.h>
#include "input/KisQtWidgetsTweaker.h"
#include <KisUsageLogger.h>
#include <kis_image_config.h>
#include "KisUiFont.h"
#include <KisMainWindow.h>

#include <KisSupportedArchitectures.h>

#include <KLocalizedTranslator>

#if defined Q_OS_WIN
#include "config_use_qt_tablet_windows.h"
#include <windows.h>
#ifndef USE_QT_TABLET_WINDOWS
#include <kis_tablet_support_win.h>
#include <kis_tablet_support_win8.h>
#else
#include <dialogs/KisDlgCustomTabletResolution.h>
#endif
#include "config-high-dpi-scale-factor-rounding-policy.h"
#include "config-set-has-border-in-full-screen-default.h"
#ifdef HAVE_SET_HAS_BORDER_IN_FULL_SCREEN_DEFAULT
#include <QtPlatformHeaders/QWindowsWindowFunctions>
#endif
#include <QLibrary>
#endif

#include <iostream>

extern "C" __attribute__ ((visibility ("default"))) int krita_main(int mode) {
	std::cout << "hello libkrita" << std::endl;
	dbgKrita << "libkrita hello world";

	int argc = 1;
	char *argv[] = {"/usr/local/bin/krita"};

	// The global initialization of the random generator
	qsrand(time(0));
	std::cout << "libkrita reset random seed OK" << std::endl;

	bool runningInKDE = !qgetenv("KDE_FULL_SESSION").isEmpty();

	#if defined HAVE_X11
		qputenv("QT_QPA_PLATFORM", "xcb");
	#endif

	// Workaround a bug in QNetworkManager
	qputenv("QT_BEARER_POLL_TIMEOUT", QByteArray::number(-1));
	std::cout << "libkrita poll workaround OK" << std::endl;

	// A per-user unique string, without /, because QLocalServer cannot use names with a / in it
	QString key = "Krita5" + QStandardPaths::writableLocation(QStandardPaths::HomeLocation).replace("/", "_");
	key = key.replace(":", "_").replace("\\","_");

	std::cout << "libkrita prepare opengl..." << std::endl;
	QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts, true);
	QCoreApplication::setAttribute(Qt::AA_DontCreateNativeWidgetSiblings, true);
	QCoreApplication::setAttribute(Qt::AA_UseHighDpiPixmaps, true);
	QCoreApplication::setAttribute(Qt::AA_DisableShaderDiskCache, true);
	std::cout << "libkrita prepare opengl OK" << std::endl;

	const QString configPath = QStandardPaths::writableLocation(QStandardPaths::GenericConfigLocation);
	QSettings kritarc(configPath + QStringLiteral("/kritadisplayrc"), QSettings::IniFormat);
	std::cout << "libkrita config rc" << std::endl;

	bool enableOpenGLDebug = false;
	bool openGLDebugSynchronous = false;
	bool logUsage = true;
	{
		KisConfig::RootSurfaceFormat rootSurfaceFormat = KisConfig::rootSurfaceFormat(&kritarc);
		KisOpenGL::OpenGLRenderer preferredRenderer = KisOpenGL::RendererAuto;

		#ifdef Q_OS_WIN
		const QString preferredRendererString = kritarc.value("OpenGLRenderer", "angle").toString();
		#else
		const QString preferredRendererString = kritarc.value("OpenGLRenderer", "auto").toString();
		#endif
		preferredRenderer = KisOpenGL::convertConfigToOpenGLRenderer(preferredRendererString);

		const KisOpenGL::RendererConfig config =
			KisOpenGL::selectSurfaceConfig(preferredRenderer, rootSurfaceFormat, enableOpenGLDebug);

		KisOpenGL::setDefaultSurfaceConfig(config);
		KisOpenGL::setDebugSynchronous(openGLDebugSynchronous);
	}
	std::cout << "libkrita opengl init..." << std::endl;

	QString root = "/tmp";
	QString language = "en_US";


	//bool rightToLeft = false;
		// And if there isn't one, check the one set by the system.
		//QLocale locale = QLocale::system();

		//if (locale.name() != QStringLiteral("en")) {

	//KisUsageLogger::writeLocaleSysInfo();

	// first create the application so we can create a pixmap
	KisApplication app(key, argc, argv);
	std::cout << "libkrita app OK" << std::endl;


	if (app.platformName() == "wayland") {
		QMessageBox::critical(0, i18nc("@title:window", "Fatal Error"), i18n("Krita does not support the Wayland platform. Use XWayland to run Krita on Wayland. Krita will close now."));
		return -1;
	}

	KisUsageLogger::writeHeader();
	KisOpenGL::initialize();


	KLocalizedString::setApplicationDomain("krita");
	//dbgLocale << "Available translations" << KLocalizedString::availableApplicationTranslations();
	//dbgLocale << "Available domain translations" << KLocalizedString::availableDomainTranslations("krita");


	#ifdef Q_OS_WIN
	QDir appdir(KoResourcePaths::getApplicationRoot());
	QString path = qgetenv("PATH");
	qputenv("PATH", QFile::encodeName(appdir.absolutePath() + "/bin" + ";"
									  + appdir.absolutePath() + "/lib" + ";"
									  + appdir.absolutePath() + "/Frameworks" + ";"
									  + appdir.absolutePath() + ";"
									  + path));

	dbgKrita << "PATH" << qgetenv("PATH");
	#endif

	if (qApp->applicationDirPath().contains(KRITA_BUILD_DIR)) {
		qFatal("FATAL: You're trying to run krita from the build location. You can only run Krita from the installation location.");
	}

	std::cout << "libkrita kis args..." << std::endl;

	KisApplicationArguments args(app);

	if (app.isRunning()) {
		// only pass arguments to main instance if they are not for batch processing
		// any batch processing would be done in this separate instance
		const bool batchRun = args.exportAs() || args.exportSequence();

		if (!batchRun) {
			QByteArray ba = args.serialize();
			if (app.sendMessage(ba)) {
				return 0;
			}
		}
	}

	if (!runningInKDE) {
		// Icons in menus are ugly and distracting
		app.setAttribute(Qt::AA_DontShowIconsInMenus);
	}
	#if (QT_VERSION >= QT_VERSION_CHECK(5, 10, 0))
		app.setAttribute(Qt::AA_DisableWindowContextHelpButton);
	#endif

	app.installEventFilter(KisQtWidgetsTweaker::instance());

	if (!args.noSplash()) {
		QWidget *splash = new KisSplashScreen();
		app.setSplashScreen(splash);
	}

	app.setAttribute(Qt::AA_CompressHighFrequencyEvents, false);

	// Set up remote arguments.
	/*
	QObject::connect(&app, SIGNAL(messageReceived(QByteArray,QObject*)),
					 &app, SLOT(remoteArguments(QByteArray,QObject*)));

	QObject::connect(&app, SIGNAL(fileOpenRequest(QString)),
					 &app, SLOT(fileOpenRequested(QString)));
	*/

	// Hardware information
	KisUsageLogger::writeSysInfo("\nHardware Information\n");
	KisUsageLogger::writeSysInfo(QString("  GPU Acceleration: %1").arg(kritarc.value("OpenGLRenderer", "auto").toString()));
	KisUsageLogger::writeSysInfo(QString("  Memory: %1 Mb").arg(KisImageConfig(true).totalRAM()));
	KisUsageLogger::writeSysInfo(QString("  Number of Cores: %1").arg(QThread::idealThreadCount()));
	KisUsageLogger::writeSysInfo(QString("  Swap Location: %1").arg(KisImageConfig(true).swapDir()));
	KisUsageLogger::writeSysInfo(
		QString("  Built for: %1")
			.arg(KisSupportedArchitectures::baseArchName()));
	KisUsageLogger::writeSysInfo(
		QString("  Base instruction set: %1")
			.arg(KisSupportedArchitectures::bestArchName()));
	KisUsageLogger::writeSysInfo(
		QString("  Supported instruction sets: %1")
			.arg(KisSupportedArchitectures::supportedInstructionSets()));

	KisUsageLogger::writeSysInfo("");

	KisConfig(true).logImportantSettings();

	app.setFont(KisUiFont::normalFont());

	std::cout << "libkrita ready..." << std::endl;

	if (!app.start(args)) {
		KisUsageLogger::log("Could not start Krita Application");
		return 1;
	}
	std::cout << "libkrita run exec..." << std::endl;
	int state = app.exec();
	std::cout << "libkrita exec exit" << std::endl;

	return state;
}

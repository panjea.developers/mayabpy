#!/usr/bin/env python3
# Run from Blender's root DIR
# https://developer.blender.org/P614
# Authored by Campbell Barton (campbellbarton) on Feb 15 2018
import os, sys

SOURCE_DIRS = ['intern']

USE_MULTIPROCESS = '--mp' in sys.argv

replace_all = (
	# source/blender/blenlib/BLI_threads.h
	("BLI_init_threads",            "BLI_threadpool_init"),
	("BLI_available_thread_index",  "BLI_threadpool_available_thread_index"),
	("BLI_insert_thread",           "BLI_threadpool_insert"),
	("BLI_remove_thread",           "BLI_threadpool_remove"),
	("BLI_remove_thread_index",     "BLI_threadpool_remove_index"),
	("BLI_remove_threads",          "BLI_threadpool_clear"),
	("BLI_end_threads",             "BLI_threadpool_end"),

	("BLI_begin_threaded_malloc",  "BLI_threaded_malloc_begin"),
	("BLI_end_threaded_malloc",    "BLI_threaded_malloc_end"),
	("BLI_lock_thread",            "BLI_thread_lock"),
	("BLI_unlock_thread",          "BLI_thread_unlock"),
	)

replace_tables = (
	replace_all,
)


replace_tables_re = [
		[((r'\b' + src + r'\b'), dst) for src, dst in table]
		for table in replace_tables
		]


def replace_all(fn, data_src):
	import re

	data_dst = data_src
	for table in replace_tables_re:
		for src_re, dst in table:
			data_dst = re.sub(src_re, dst, data_dst)
	return data_dst

operation = replace_all

def source_files(path):
	for dirpath, dirnames, filenames in os.walk(path):
		dirnames[:] = [d for d in dirnames if not d.startswith(".")]
		for filename in filenames:
			if filename.startswith("."):
				continue
			ext = os.path.splitext(filename)[1]
			if ext.lower() in {".c", ".cc", ".cxx", ".cpp", ".h", ".hxx", ".hpp"}:
				yield os.path.join(dirpath, filename)

def operation_wrap(fn):
	if os.path.isfile(fn+'.bak'):
		raise RuntimeError('bli thread api already patched')

	with open(fn, "r", encoding="utf-8") as f:
		data_src = f.read()
		data_dst = operation(fn, data_src)

	if data_dst is None or (data_src == data_dst):
		return

	with open(fn, "w", encoding="utf-8") as f:
		print('patching:', fn)
		open(fn+'.bak', 'wb').write(data_src.encode('utf-8'))
		f.write(data_dst)


def main():
	if USE_MULTIPROCESS:
		args = [fn for DIR in SOURCE_DIRS for fn in source_files(DIR)]
		import multiprocessing
		job_total = multiprocessing.cpu_count()
		pool = multiprocessing.Pool(processes=job_total * 2)
		pool.map(operation_wrap, args)
	else:
		for dpath in SOURCE_DIRS:
			for fn in source_files(dpath):
				operation_wrap(fn)


if __name__ == "__main__":
	main()
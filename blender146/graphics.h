/**
 * $Id:$
 * ***** BEGIN GPL/BL DUAL LICENSE BLOCK *****
 *
 * The contents of this file may be used under the terms of either the GNU
 * General Public License Version 2 or later (the "GPL", see
 * http://www.gnu.org/licenses/gpl.html ), or the Blender License 1.0 or
 * later (the "BL", see http://www.blender.org/BL/ ) which has to be
 * bought from the Blender Foundation to become active, in which case the
 * above mentioned GPL option does not apply.
 *
 * The Original Code is Copyright (C) 2002 by NaN Holding BV.
 * All rights reserved.
 *
 * The Original Code is: all of this file.
 *
 * Contributor(s): none yet.
 *
 * ***** END GPL/BL DUAL LICENSE BLOCK *****
 */

/* graphics.h    dec 93 */
// updated by rapter - nov 2022


#ifndef GRAPHICS_H
#define GRAPHICS_H

// https://stackoverflow.com/questions/18550265/compiling-a-c-program-that-uses-opengl-in-mac-os-x
//#include <gl/mygl.h>		/* in verband met typedef Object */
//#include <gl/device.h>
#include <OpenGL/gl.h>
#include <GLUT/glut.h>

// rapter note: what should these be, what is mmode()
#define MPROJECTION 0
#define MVIEWING 0

#include "objfnt.h"

// https://nixdoc.net/man-pages/IRIX/man3W/fminit.3W.html
// https://www.linuxjournal.com/article/4421
#include "fmclient.h"

#include "Button.h"
#include "screen.h"


#define LINE2S(v1, v2)	{bgnline(); v2s((short *)(v1)); v2s((short *)(v2)); endline();}
#define LINE3S(v1, v2)	{bgnline(); v3s((short *)(v1)); v3s((short *)(v2)); endline();}
#define LINE2I(v1, v2)	{bgnline(); v2i((int *)(v1)); v2i((int *)(v2)); endline();}
#define LINE3I(v1, v2)	{bgnline(); v3i((int *)(v1)); v3i((int *)(v2)); endline();}
#define LINE2F(v1, v2)	{bgnline(); v2f((float *)(v1)); v2f((float *)(v2)); endline();}
#define LINE3F(v1, v2)	{bgnline(); v3f((float *)(v1)); v3f((float *)(v2)); endline();}

#define IRIS 1
#define ELAN 2
#define ENTRY 3
#undef VIDEO
#define VIDEO 4
#define M_O2 5

#define drawmode mydrawmode

extern short getcursorN();	/* usiblender.c */

// from blender 1.72
/* ****************** OGL ******************************* */
/* voor OpenGl compatibility */

typedef short Device;
#define NORMALDRAW      0
#define PUPDRAW         1
#define OVERDRAW        2


#define CURSOR_VPAINT       GLUT_CURSOR_LEFT_ARROW
#define CURSOR_FACESEL      GLUT_CURSOR_RIGHT_ARROW
#define CURSOR_WAIT         GLUT_CURSOR_WAIT
#define CURSOR_EDIT         GLUT_CURSOR_CROSSHAIR
#define CURSOR_STD          GLUT_CURSOR_INHERIT

/* Mij afspraak is cpack een getal dat als 0xFFaa66 of zo kan worden uitgedrukt. Is dus gevoelig voor endian. 
 * Met deze define wordt het altijd goed afgebeeld
 */
#define cpack(x)    glColor3ub( ((x)&0xFF), (((x)>>8)&0xFF), (((x)>>16)&0xFF) )

#define glMultMatrixf(x)        glMultMatrixf( (float *)(x))
#define glLoadMatrixf(x)        glLoadMatrixf( (float *)(x))

#endif /* GRAPHICS_H */


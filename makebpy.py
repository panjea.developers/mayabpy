#!/usr/bin/env python3
import os, sys, subprocess, time, ctypes
import struct

## if on a 32bit host, then just for running makesdna and makesrna to capture their output
BITS32 = struct.calcsize('P') * 8 == 32
BLENDER_SRC = 'blender'
USE_CYCLES = '--cycles' in sys.argv

def ensure_blender_wasm_dna():
	for name in 'dna.c dna_verify.c dna_type_offsets.h'.split():
		print(name)
		data = open('./blendwasm/' + name, 'rb').read()
		open( './buildwbpy/source/blender/makesdna/intern/' + name, 'wb').write(data)

def ensure_blender_wasm_rna():
	name = 'RNA_prototypes.h'
	print(name)
	data = open('./blendwasm/' + name, 'rb').read()
	open( './buildwbpy/source/blender/makesrna/' + name, 'wb').write(data)

	for name in os.listdir('./blendwasm/rna'):
		data = open('./blendwasm/rna/' + name, 'rb').read()
		print(name, len(data))
		open( './buildwbpy/source/blender/makesrna/intern/' + name, 'wb').write(data)

BF_RNA_DIR_HACK = '''
	@echo "rna build step workaround"
	cp -v %s source/blender/makesrna/intern/.

source/blender/makesrna/intern/rna_action_gen.c: source/blender/makesrna/intern/rna_ID_gen.c
''' % os.path.abspath('./blendwasm/rna/rna_ID_gen.c')

def patch_bf_rna_dir():
	fname = './buildwbpy/source/blender/makesrna/intern/CMakeFiles/bf_rna.dir/build.make'
	assert os.path.isfile(fname)
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	key = 'source/blender/makesrna/intern/rna_action_gen.c: source/blender/makesrna/intern/rna_ID_gen.c'
	assert data.count(key)==1
	data = data.replace(key, BF_RNA_DIR_HACK)
		
	open(fname,'wb').write(data.encode('utf-8'))


if '--wasm-dna' in sys.argv:
	ensure_blender_wasm_dna()
	subprocess.check_call(['make'], cwd='./buildwbpy/source/blender/makesdna/intern')
	print('now run `./makebpy.py --wasm --continue`')
	sys.exit()

if '--wasm-rna' in sys.argv:
	ensure_blender_wasm_rna()
	patch_bf_rna_dir()
	subprocess.check_call(['make'], cwd='./buildwbpy/source/blender/makesrna/intern')
	print('now run `./makebpy.py --wasm --continue`')
	sys.exit()

if '--wasm-main' in sys.argv:
	data = open('./weblender.cpp','rb').read()
	fname = os.path.join(BLENDER_SRC, 'source/creator/weblender.cpp')
	open(fname, 'wb').write(data)
	subprocess.check_call(['make'], cwd='./buildwbpy/source/creator')
	sys.exit()


#https://knowledge.autodesk.com/support/maya/learn-explore/caas/simplecontent/content/installing-maya-2022-ubuntu.html
if sys.platform=='linux':
	MAYA_PATH = '/usr/autodesk/maya2023/bin'
	MAYA_INC = '/usr/autodesk/maya2023/include'
else:
	MAYA_PATH = '/Applications/Autodesk/maya2023/Maya.app/Contents/MacOS/'
	MAYA_INC = '/Applications/Autodesk/maya2023/include'

MAYA_LIBS = 'OpenMaya OpenMayaFX OpenMayaUI OpenMayaAnim OpenMayaRender'.split()
QT_LIBS = 'Qt5Core Qt5Gui Qt5Widgets Qt5OpenGL'.split()  # Qt53DCore 


## for testing mender qt standalone need to: `brew install qt@5`
if sys.platform=='darwin':
	QT_INC = '/usr/local/opt/qt@5/include'
else:
	if BITS32:
		QT_INC = '/usr/include/i386-linux-gnu/qt5'
	else:
		QT_INC = '/usr/include/x86_64-linux-gnu/qt5'

if not os.path.isdir(QT_INC) and os.path.isdir('./qt5-include'):
	QT_INC  = os.path.abspath('./qt5-include')
	QT_TGZ  = '/Applications/Autodesk/maya2023/include/qt_5.15.2-include.tar.gz'
else:
	QT_TGZ=None
	if sys.platform=='darwin':
		QT_LIB_DIR = '/usr/local/opt/qt@5/lib'
	else:
		if BITS32:
			QT_LIB_DIR = '/usr/lib/i386-linux-gnu'
		else:
			QT_LIB_DIR = '/usr/lib/x86_64-linux-gnu'

if sys.platform=='darwin':
	ext = 'dylib'
	if not os.path.isdir(QT_INC) and '--qt' in sys.argv:
		if QT_TGZ:
			os.mkdir(QT_INC)
			open(os.path.join(QT_INC, 'qt5-include.tar.gz'), 'wb').write(
				open(QT_TGZ,'rb').read()
			)
			subprocess.check_call(['tar', '-xvf', 'qt5-include.tar.gz'], cwd=QT_INC)
else:
	ext = 'so'

MAYA_INSTALLED = False
LIBOPENMAYA = None
MLIBS = [os.path.join(MAYA_PATH, 'lib%s.%s' ) %(lib,ext) for lib in MAYA_LIBS]
for lib in MLIBS:
	print('checking:', lib)
	if os.path.isfile(lib):
		MAYA_INSTALLED=True

if MAYA_INSTALLED:
	LIBOPENMAYA = MLIBS[0]


OPENTOONZ_PATCHED = './libtoonz'

## gets inserted into blenders cmake main build
def get_toonz_libs():
	print(OPENTOONZ_PATCHED)
	libs = []
	for lib in os.listdir(OPENTOONZ_PATCHED):
		if lib.endswith(('.dylib', '.so', '.dll')):
			print('found patched opentoonz lib:', lib)
			apath = os.path.abspath(
					os.path.join(OPENTOONZ_PATCHED, lib)
			)
			print(apath)
			libs.append(apath)
	#raise RuntimeError(libs)
	return '\n'.join(libs)

def get_qt_libs():
	if QT_TGZ:
		libs = [os.path.join(MAYA_PATH, 'lib%s.%s' ) %(lib,ext) for lib in QT_LIBS]
		for lib in libs:
			print('checking:', lib)
			assert os.path.isfile(lib)
	elif sys.platform=='darwin':
		libs = []
		for lib in QT_LIBS:
			print('finding:', lib)
			osxname = lib.replace('5','')
			lpath = os.path.join(QT_LIB_DIR, '%s.framework/%s' %(osxname,osxname))
			print(lpath)
			assert os.path.isfile(lpath)
			libs.append(lpath)
	else:
		#sudo apt-get install qtbase5-dev qtchooser qt5-qmake qtbase5-dev-tools
		#qmake -query QT_INSTALL_HEADERS
		libs = []
		for lib in QT_LIBS:
			print('finding:', lib)
			lpath = os.path.join(QT_LIB_DIR, 'lib'+lib+'.so')
			if os.path.isfile(lpath):
				print('found:', lpath)
				libs.append(lpath)
			elif BITS32:
				pass
			else:
				raise RuntimeError('you need to install qt5')

	if os.path.isfile(QTMYPAINT_LIB):
		libs.append( os.path.abspath(QTMYPAINT_LIB) )

	return '\n'.join(libs)


if MAYA_INSTALLED:
	PYVERSION = (3, 9, 3, 1)
	PYTHON_INC = '/usr/local/opt/python@3.9/Frameworks/Python.framework/Versions/3.9/Headers/'
	if not os.path.isdir(PYTHON_INC):
		subprocess.check_call(['brew', 'install', 'python@3.9'])
else:
	PYTHON_INC = None
	PYVERSION  = None

#'/usr/local/Frameworks/Python.framework/Headers'
if MAYA_INSTALLED:
	MAYAPY = os.path.join(MAYA_PATH, 'mayapy')
	assert os.path.isfile(MAYAPY)

# https://wiki.blender.org/wiki/Building_Blender/Mac
if sys.platform=='darwin':
	if not os.path.isfile('/usr/local/bin/svn'):
		subprocess.check_call(['brew', 'install', 'svn', 'cmake'])

if not os.path.isdir(BLENDER_SRC):
	subprocess.check_call(['git', 'clone', '--depth=1', 'https://git.blender.org/blender.git'])
	subprocess.check_call(['make', 'update'],cwd='blender')
	if sys.platform == 'linux':
		## minimal stuff required headless build
		os.system('sudo apt-get install libbrotli-dev libharfbuzz-dev libbz2-dev libepoxy-dev')

if not os.path.isdir('./blender/source/blender/blender27render'):
	subprocess.check_call([
		'ln', '-s', 
		os.path.abspath('./blender27render'),
		os.path.abspath('./blender/source/blender/blender27render'),
	])


#if sys.platform=='darwin':
#	if not os.path.isdir('lib/darwin'):
#		subprocess.check_call(['make', 'update'],cwd='blender')

QTMYPAINT_SRC = './qtmypaint'
if sys.platform=='darwin':
	QTMYPAINT_LIB = './qtmypaint/demo/libqtmypaint.dylib'
else:
	QTMYPAINT_LIB = './qtmypaint/demo/libqtmypaint.so'

def build_qtmypaint():
	if not os.path.isdir(QTMYPAINT_SRC):
		subprocess.check_call(['git', 'clone', '--depth=1', 'https://gitlab.com/panjea.developers/qtmypaint.git'])
	else:
		subprocess.check_call(['git', 'pull'], cwd=QTMYPAINT_SRC)

	qmake = 'qmake'
	if os.path.isfile('/usr/local/opt/qt@5/bin/qmake'):
		qmake = '/usr/local/opt/qt@5/bin/qmake'

	subprocess.check_call([qmake], cwd=QTMYPAINT_SRC)
	subprocess.check_call(['make'], cwd=QTMYPAINT_SRC)
	print(QTMYPAINT_LIB)
	assert os.path.isfile(QTMYPAINT_LIB)

if '--mypaint' in sys.argv:
	build_qtmypaint()
	sys.exit()

KRITA_SRC = './krita'
KRITA_BUILD = './buildkrita'
#LIBKRITA = os.path.join(KRITA_BUILD, 'krita/libkrita.' + ext)  ## SHARED is .dylib on osx
LIBKRITA = os.path.join(KRITA_BUILD, 'krita/krita.so')       ## MODULE is .so on osx

## https://invent.kde.org/packaging/homebrew-kde
KDE_BREW = './homebrew-kde'
QUAZIP_SRC = './quazip'
QUAZIP_BUILD = './buildquazip'

def patch_krita_krita_cmake():
	fname = os.path.join(KRITA_SRC, 'krita/CMakeLists.txt')
	assert os.path.isfile(fname)
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	key = 'add_executable(krita ${krita_SRCS})'
	if sys.platform=='darwin':
		## TODO libmypaint from brew has some issue with krita cmake
		## for now not using that plugin.
		#lnk = 'target_link_libraries(krita PUBLIC /usr/local/opt/libmypaint/lib/libmypaint.dylib)'
		#rep = 'add_library(krita MODULE ${krita_SRCS})' + '\n\t' + lnk
		#rep = 'add_library(krita SHARED ${krita_SRCS})' + '\n\t' + lnk
		rep = 'add_library(krita MODULE ${krita_SRCS})'
	else:
		rep = 'add_library(krita SHARED ${krita_SRCS})'

	assert data.count(key)==1
	data = data.replace(key, rep)

	key = 'add_subdirectory( integration )'
	assert data.count(key)==1
	data = data.replace(key, '')
		
	open(fname,'wb').write(data.encode('utf-8'))


def patch_krita_cmake():
	fname = os.path.join(KRITA_SRC, 'CMakeLists.txt')
	assert os.path.isfile(fname)
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	key = 'set( CMAKE_CXX_STANDARD 14 )'
	#rep = 'set( CMAKE_CXX_STANDARD 17 )'
	rep = 'set( CMAKE_CXX_STANDARD 14 )'  ## in osx brew some deps are not c++17 ready.
	rep += '\n'
	rep += 'add_definitions(-DCMS_NO_REGISTER_KEYWORD=1)'
	assert data.count(key)==1
	data = data.replace(key, rep)
		
	open(fname,'wb').write(data.encode('utf-8'))

def patch_krita_main():
	fname = os.path.join(KRITA_SRC, 'krita/main.cc')
	assert os.path.isfile(fname)
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	data = open('./libkrita.cpp','rb').read().decode('utf-8')
	open(fname,'wb').write( data.encode('utf-8') )

KRITA_PAINTOPS_CMAKE = '''
include_directories(libpaintop)

add_subdirectory( libpaintop )
add_subdirectory( defaultpaintops )
add_subdirectory( hairy )
add_subdirectory( deform )
add_subdirectory( curvebrush )
add_subdirectory( spray )
add_subdirectory( filterop )
add_subdirectory( experiment )
add_subdirectory( particle )
add_subdirectory( gridbrush )
add_subdirectory( hatching)
add_subdirectory( sketch )
add_subdirectory( colorsmudge )
add_subdirectory( roundmarker )
add_subdirectory( tangentnormal )

#if(LIBMYPAINT_FOUND)
#    add_subdirectory( mypaint )
#endif()

'''

def patch_krita_plugins():
	fname = os.path.join(KRITA_SRC, 'plugins/paintops/CMakeLists.txt')
	assert os.path.isfile(fname)
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	assert 'mypaint' in data
	data = KRITA_PAINTOPS_CMAKE
	open(fname,'wb').write(data.encode('utf-8'))


def build_krita():
	print("WARN: krita integration is not yet working with blender")
	patch_krita_cmake()
	patch_krita_krita_cmake()
	patch_krita_main()
	patch_krita_plugins()

	if not os.path.isdir(QUAZIP_SRC):
		subprocess.check_call(['git', 'clone', '--depth=1', 'https://github.com/stachenov/quazip.git'])

	if not os.path.isdir(QUAZIP_BUILD):
		os.mkdir(QUAZIP_BUILD)
		srcdir = os.path.abspath(QUAZIP_SRC)
		assert os.path.isdir(srcdir)
		cmd = [
			'cmake', srcdir,
			#'-DCMAKE_POSITION_INDEPENDENT_CODE=ON',  ## should not be required
			'-DQUAZIP_QT_MAJOR_VERSION=5',
			'-DQt5_DIR=/usr/local/opt/qt@5/lib/cmake/Qt5',

		]
		print(cmd)
		subprocess.check_call(cmd, cwd=QUAZIP_BUILD)
		subprocess.check_call(['make'], cwd=QUAZIP_BUILD)
		subprocess.check_call(['sudo', 'make', 'install'], cwd=QUAZIP_BUILD)


	if sys.platform=='darwin' and '--install-deps' in sys.argv:
		# kde-extra-cmake-modules (old old name)
		#os.system('brew install extra-cmake-modules')  ## this conflicts with the one from kde
		cmd = 'brew install kde-extra-cmake-modules'.split()
		print(cmd)
		subprocess.check_call(cmd)
		#cmd = 'brew install kde-mac/kde/libkexiv2'.split()
		cmd = 'brew install exiv2 gsl'.split()  ## quazip
		print(cmd)
		subprocess.check_call(cmd)

		if not os.path.isdir(KDE_BREW):
			subprocess.check_call(['git', 'clone', '--depth=1', 'https://invent.kde.org/packaging/homebrew-kde.git'])
			cmd = 'brew untap kde-mac/kde'.split()
			print(cmd)
			try:
				subprocess.check_call(cmd)
				print('previous kde removed OK')
			except:
				print('previous kde not installed')

			cmd = 'brew tap kde-mac/kde https://invent.kde.org/packaging/homebrew-kde.git --force-auto-update'.split()
			print(cmd)
			subprocess.check_call(cmd)

			cmd = 'brew install kde-mac/kde/kf5-kguiaddons'.split()
			print(cmd)
			subprocess.check_call(cmd)

			cmd = 'brew install kde-mac/kde/kf5-kwindowsystem'.split()
			print(cmd)
			subprocess.check_call(cmd)


			cmd = 'brew install kde-mac/kde/kf5-kcoreaddons'.split()
			print(cmd)
			subprocess.check_call(cmd)

			cmd = 'brew install kde-mac/kde/kf5-kwidgetsaddons'.split()
			print(cmd)
			subprocess.check_call(cmd)

			for dep in 'kf5-kitemviews kf5-kitemmodels kf5-kconfig kf5-kcompletion'.split():
				cmd = 'brew install kde-mac/kde/' + dep
				print(cmd)
				subprocess.check_call(cmd.split())

	if not os.path.isdir(KRITA_SRC):
		subprocess.check_call(['git', 'clone', '--depth=1', 'https://github.com/KDE/krita.git'])
		#if sys.platform=='linux':
		#	os.system('sudo apt-get install libbrotli-dev')

	if not os.path.isdir(KRITA_BUILD):
		os.mkdir(KRITA_BUILD)

	srcdir = os.path.abspath(KRITA_SRC)
	assert os.path.isdir(srcdir)
	# lcms2.h problems on osx with c++17 - not really required but good to be Qt6 compatible for later
	#define CMS_NO_REGISTER_KEYWORD 1
	cmd = [
		'cmake', srcdir,
		#'-B', 'build',
		#'-DCMAKE_CXX_FLAGS=-std=c++17',
		#'-DCMAKE_CXX_STANDARD=17',  ## cmake overrides this?
		#'-DCMAKE_CXX_STANDARD_REQUIRED=ON',
		#'-DCMS_NO_REGISTER_KEYWORD=1',
		#'-DLIBMYPAINT_LIBRARIES=/usr/local/opt/libmypaint/lib',

		'-DCMAKE_BUILD_TYPE=Release',  ## no debug syms
		'-DCMAKE_POSITION_INDEPENDENT_CODE=ON',
		#'-DBUILD_SHARED_LIBS=TRUE',
		'-DCMAKE_INSTALL_PREFIX=/usr/local',
		'-DKRITA_DEVS=ON',
		'-DQt5_DIR=/usr/local/opt/qt@5/lib/cmake/Qt5',
		'-DQt5Xml_DIR=/usr/local/opt/qt@5/lib/cmake/Qt5Xml',
		'-DQt5Widgets_DIR=/usr/local/opt/qt@5/lib/cmake/Qt5Widgets',
		'-DQt5Core_DIR=/usr/local/opt/qt@5/lib/cmake/Qt5Core',
		'-DQt5Gui_DIR=/usr/local/opt/qt@5/lib/cmake/Qt5Gui',
		'-DQt5DBus_DIR=/usr/local/opt/qt@5/lib/cmake/Qt5DBus',
	]
	print(cmd)
	subprocess.check_call(cmd, cwd=KRITA_BUILD)
	subprocess.check_call(['make'], cwd=KRITA_BUILD)
	subprocess.check_call(['sudo','make','install'], cwd=KRITA_BUILD)
	sys.exit()

def test_libkrita():
	print(LIBKRITA)
	os.system('nm ' + LIBKRITA)
	os.system('otool -L ' + LIBKRITA)

	print('libkrita ctypes test:')
	os.environ['KIS_TEST_PREFIX_PATH']='/usr/local/'
	import ctypes
	dll = ctypes.CDLL(LIBKRITA)
	print(dll)
	dll.krita_main()

if '--krita' in sys.argv:
	if os.path.isfile(LIBKRITA):
		print('libkrita shared library already built')
	else:
		build_krita()

	test_libkrita()
	sys.exit()


OPENTOONZ_SRC = 'opentoonz'
OPENTOONZ_BUILD = 'buildtoonz'
if sys.platform=='darwin':
	LIBOPENTOONZ = os.path.join(OPENTOONZ_BUILD, 'toonz/libOpenToonz.dylib')
else:
	LIBOPENTOONZ = os.path.join(OPENTOONZ_BUILD, 'lib/opentoonz/libOpenToonz.so')

FREETYPE_SRC = './freetype'
FREETYPE_BUILD = './buildfreetype'
FREETYPE_LIB = os.path.abspath(
	os.path.join(FREETYPE_BUILD,'libfreetype.a')
)
FREETYPE_INCLUDE_DIRS = os.path.abspath(
	os.path.join(FREETYPE_BUILD,'include')
)

def build_freetype():
	if not os.path.isdir(FREETYPE_SRC):
		subprocess.check_call(['git', 'clone', '--depth=1', 'https://gitlab.freedesktop.org/freetype/freetype.git'])
		if sys.platform=='linux':
			os.system('sudo apt-get install libbrotli-dev libharfbuzz-dev libbz2-dev')

	if not os.path.isdir(FREETYPE_BUILD):
		os.mkdir(FREETYPE_BUILD)

	srcdir = os.path.abspath(FREETYPE_SRC)
	assert os.path.isdir(srcdir)
	cmd = [
		'cmake', srcdir,
		#'-B', 'build',
		'-DCMAKE_BUILD_TYPE=Release',  ## no debug syms
		'-DCMAKE_POSITION_INDEPENDENT_CODE=ON',
		#'-DBUILD_SHARED_LIBS=TRUE',
		'-DFT_REQUIRE_ZLIB=TRUE',
		'-DFT_REQUIRE_BZIP2=TRUE',
		'-DFT_REQUIRE_PNG=TRUE',
		'-DFT_REQUIRE_HARFBUZZ=TRUE',
		'-DFT_REQUIRE_BROTLI=TRUE',
		#'--config', 'Release',
		#'--target', 'package'
	]
	print(cmd)
	subprocess.check_call(cmd, cwd=FREETYPE_BUILD)
	subprocess.check_call(['make'], cwd=FREETYPE_BUILD)
	assert os.path.isfile(FREETYPE_LIB)
	#subprocess.check_call(['sudo', 'cp', '-v', FREETYPE_LIB, '/usr/local/lib'])
	subprocess.check_call(['sudo', 'make', 'install'], cwd=FREETYPE_BUILD)

if sys.platform=='linux':
	if not os.path.isfile(FREETYPE_LIB):
		build_freetype()

JPEG_SRC = './libjpeg-turbo'
JPEG_BUILD = './buildjpeg'

def build_jpeg_turbo():
	if not os.path.isdir(JPEG_SRC):
		subprocess.check_call(['git', 'clone', '--depth=1', 'https://github.com/libjpeg-turbo/libjpeg-turbo.git'])

	if not os.path.isdir(JPEG_BUILD):
		os.mkdir(JPEG_BUILD)

		srcdir = os.path.abspath(JPEG_SRC)
		assert os.path.isdir(srcdir)
		cmd = [
			'cmake', srcdir,
			'-DCMAKE_POSITION_INDEPENDENT_CODE=ON',
			#'-DBUILD_SHARED_LIBS=ON',
		]
		print(cmd)
		subprocess.check_call(cmd, cwd=JPEG_BUILD)
		subprocess.check_call(['make'], cwd=JPEG_BUILD)
		subprocess.check_call(['sudo', 'make', 'install'], cwd=JPEG_BUILD)
		sys.exit()
		
	if not os.path.isfile('/usr/local/include/turbojpeg.h'):
		assert os.path.isdir('/opt/libjpeg-turbo/include/')
		os.system('sudo cp -Rv /opt/libjpeg-turbo/include/*.* /usr/local/include/.')


if '--toonz' in sys.argv:
	if sys.platform=='linux':
		build_jpeg_turbo()

	# note can not use libturbojpeg0-dev from ubuntu because:
	# x86_64-linux-gnu/libturbojpeg.a(jcapimin.c.o): relocation R_X86_64_PC32 
	# against symbol `jpeg_natural_order' can not be used when making a 
	# shared object; recompile with -fPIC

	if not os.path.isdir(OPENTOONZ_SRC):
		subprocess.check_call(['git', 'clone', '--depth=1', 'https://github.com/opentoonz/opentoonz.git'])
		if sys.platform=='darwin':
			subprocess.check_call('brew install superlu opencv glew lz4 libjpeg libpng lzo pkg-config libusb cmake git-lfs libmypaint qt@5 boost jpeg-turbo'.split())
		else:
			subprocess.check_call('sudo apt-get install qt5-default qttools5-dev qttools5-dev-tools libsuperlu-dev libglew-dev liblz4-dev libjpeg-dev libpng-dev liblzo2-dev pkg-config libusb-dev cmake git-lfs libmypaint-dev libboost-dev libqt5serialport5-dev libopencv-dev libusb-1.0-0-dev libjpeg-turbo8 freeglut3 freeglut3-dev patchelf'.split())
		## do not use: libturbojpeg0-dev
		try:
			subprocess.check_call('git lfs install'.split())
		except:
			raise RuntimeError('try running `git lfs install` then `git lfs pull`')

		subprocess.check_call('git lfs pull'.split(), cwd=OPENTOONZ_SRC)

	LIBTIFF_SRC = 'libtiff'
	LIBTIFF_BUILD = 'buildtiff'
	if not os.path.isdir(LIBTIFF_SRC):
		subprocess.check_call(['git', 'clone', '--depth=1', 'https://gitlab.com/libtiff/libtiff.git'])

	rebuild_tiff = '--tiff' in sys.argv
	if not os.path.isdir(LIBTIFF_BUILD):
		os.mkdir(LIBTIFF_BUILD)
		rebuild_tiff = True

	if rebuild_tiff:
		srcdir = os.path.abspath('./libtiff')
		assert os.path.isdir(srcdir)
		cmd = [
			'cmake', srcdir,
			'-Dtiff-tools=OFF',
			'-Dtiff-tests=OFF',
			'-Dtiff-contrib=OFF',
			'-Dtiff-docs=OFF',
			'-Dtiff-deprecated=OFF',
			'-Dzstd=OFF',
			'-Dwebp=OFF',
			'-DCMAKE_POSITION_INDEPENDENT_CODE=ON',  ## required on linux, not on osx
			#'-DBUILD_SHARED_LIBS=ON',
		]
		subprocess.check_call(cmd, cwd=LIBTIFF_BUILD)
		subprocess.check_call(['make'], cwd=LIBTIFF_BUILD)
		subprocess.check_call(['sudo', 'make', 'install'], cwd=LIBTIFF_BUILD)  ## not required
		sys.exit()

	## cmake will ignore this? fixed by using sudo make install
	TIFF_LIB = os.path.abspath(os.path.join(LIBTIFF_BUILD, 'libtiff/libtiff.a'))
	assert os.path.isfile(TIFF_LIB)

	TIFF_INC_DIR = os.path.abspath(os.path.join(LIBTIFF_SRC, 'libtiff'))
	assert os.path.isfile(os.path.join(TIFF_INC_DIR,'tiffiop.h'))

	TIFF_CONFIG_H = os.path.join(TIFF_INC_DIR,'tif_config.h')
	TIFF_CONF_H = os.path.join(TIFF_INC_DIR,'tiffconf.h')

	if not os.path.isfile(TIFF_CONFIG_H):
		print('saving tif_config.h', TIFF_CONFIG_H)
		open(TIFF_CONFIG_H,'wb').write(
			open(os.path.join(LIBTIFF_BUILD,'libtiff/tif_config.h'), 'rb').read()
		)
		open(TIFF_CONF_H,'wb').write(
			open(os.path.join(LIBTIFF_BUILD,'libtiff/tiffconf.h'), 'rb').read()
		)

def patch_toonz_tzp():
	fname = os.path.join(OPENTOONZ_SRC, 'toonz/sources/image/tzp/tiio_tzp.cpp')
	assert os.path.isfile(fname)
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))
	key = '#include "tiffiop.h"'
	assert data.count(key)==1
	data = data.replace(key, '#include <tiffiop.h>')
	open(fname,'wb').write(data.encode('utf-8'))


def patch_toonz_tif():
	fname = os.path.join(OPENTOONZ_SRC, 'toonz/sources/image/tif/tiio_tif.cpp')
	assert os.path.isfile(fname)
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	key = 'TIFFReadRGBATile_64'
	assert data.count(key)==1
	data = data.replace(key, 'TIFFReadRGBATile')
	key = 'int ret = TIFFReadRGBATile(m_tiff, x, y, tile.get());'
	pat = 'int ret = TIFFReadRGBATile(m_tiff, x, y, (uint32_t*)tile.get());'
	print(data.count(key))
	assert data.count(key)==2
	data = data.replace(key, pat)

	key = 'TIFFReadRGBAStrip_64'
	assert data.count(key)
	data = data.replace(key, 'TIFFReadRGBAStrip')
	key = 'TIFFReadRGBAStrip(m_tiff, y, (uint64 *)m_stripBuffer);'
	pat = 'TIFFReadRGBAStrip(m_tiff, y, (uint32_t *)m_stripBuffer);'
	assert data.count(key)==1
	data = data.replace(key, pat)

	open(fname,'wb').write(data.encode('utf-8'))

TOONZ_CMAKE_BAD = '''
add_subdirectory(tcleanupper)
add_subdirectory(tcomposer)
add_subdirectory(tconverter)
add_subdirectory(toonzfarm)
'''.strip().splitlines()

## not required on osx
def patch_toonz_toplevel_cmake():
	fname = os.path.join(OPENTOONZ_SRC, 'toonz/sources/CMakeLists.txt')
	assert os.path.isfile(fname)
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	for bad in TOONZ_CMAKE_BAD:
		assert data.count(bad) ==1
		data = data.replace(bad,'')

	open(fname,'wb').write(data.encode('utf-8'))


def patch_toonz_cmake():
	fname = os.path.join(OPENTOONZ_SRC, 'toonz/sources/toonz/CMakeLists.txt')
	assert os.path.isfile(fname)
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	key = 'add_executable(OpenToonz MACOSX_BUNDLE OpenToonz.icns ${HEADERS} ${SOURCES} ${OBJCSOURCES} ${RESOURCES})'
	pat = 'add_library(OpenToonz SHARED ${HEADERS} ${SOURCES} ${OBJCSOURCES} ${RESOURCES})'
	assert data.count(key)==1
	data = data.replace(key, pat)

	key = 'add_executable(OpenToonz WIN32 ${HEADERS} ${SOURCES} ${OBJCSOURCES} ${RESOURCES} ${RC_FILE})'
	assert data.count(key)==1
	data = data.replace(key, pat)

	## not required on osx
	for tag in 'tcleanup tcomposer tconverter tfarmcontroller tfarmserver'.split():
		bad = '"$<TARGET_FILE:%s>"' % tag
		#print(bad)
		#print(data.count(bad))
		assert data.count(bad)==2
		data = data.replace(bad,'')

		bad = 'add_custom_command(TARGET OpenToonz POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:%s> ${CMAKE_RUNTIME_OUTPUT_DIRECTORY} DEPENDS %s)' %(tag,tag)
		assert data.count(bad)==1
		data = data.replace(bad,'')
		
	open(fname,'wb').write(data.encode('utf-8'))


LIBTOONZ_HEADER = '''
#include <QDebug>

'''

def patch_toonz_main():
	## note this is just appended to the source ##
	libtoonz = open('./libtoonz.cpp','rb').read().decode('utf-8')

	fname = os.path.join(OPENTOONZ_SRC, 'toonz/sources/toonz/main.cpp')
	assert os.path.isfile(fname)
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	data = LIBTOONZ_HEADER + data + libtoonz
	open(fname,'wb').write(data.encode('utf-8'))

def patch_toonz_tenv():
	## note this is just appended to the source ##
	libtoonz_tenv = open('./libtoonz_tenv.cpp','rb').read().decode('utf-8')

	fname = os.path.join(OPENTOONZ_SRC, 'toonz/sources/common/tapptools/tenv.cpp')
	assert os.path.isfile(fname)
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	#data = data + libtoonz
	data = libtoonz_tenv
	open(fname,'wb').write(data.encode('utf-8'))

def patch_toonz_mainwin():
	## note this is just appended to the source ##
	patch = open('./libtoonz_mainwindow.cpp','rb').read().decode('utf-8')

	fname = os.path.join(OPENTOONZ_SRC, 'toonz/sources/toonz/mainwindow.cpp')
	assert os.path.isfile(fname)
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	data = patch
	open(fname,'wb').write(data.encode('utf-8'))

TOONZ_VBRUSH_HEADER = '''
//extern "C" {
	typedef void(*TNZ_CALLBACK)(std::vector<float>);
	static TNZ_CALLBACK s_stroke_cb=nullptr;
	void opentoonz_set_stroke_callback(TNZ_CALLBACK ptr){
		std::cout<<"set_stroke_callback"<< ptr << std::endl;
		//std::vector<float> test;
		//test.push_back(1.8);
		//ptr(test);
		s_stroke_cb=ptr;
	}
//}


static void blender_notify(TStroke* stroke){
	std::cout<<"blender_notify - st:"<< stroke << std::endl;

	if (s_stroke_cb==nullptr) return;

	std::cout<<"blender_notify - cb:"<< s_stroke_cb << std::endl;

	std::vector<float> vec;
	auto n = stroke->getControlPointCount();
	for (int i=0; i<n; i++) {
		TPointD pnt = stroke->getControlPoint(i);
		vec.push_back(pnt.x);
		vec.push_back(pnt.y);
	}
	s_stroke_cb( vec );
}

'''

def patch_toonz_vbrush():
	## note this is just appended to the source ##
	#patch = open('./libtoonz_mainwindow.cpp','rb').read().decode('utf-8')

	fname = os.path.join(OPENTOONZ_SRC, 'toonz/sources/tnztools/toonzvectorbrushtool.cpp')
	assert os.path.isfile(fname)
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	##key = 'int points = stroke->getControlPointCount();'  ## not here, this is motion paths section
	key = 'stroke->setStyle(m_styleId);'
	patch = 'blender_notify(stroke);'
	assert data.count(key)==1
	data = data.replace(key, key+patch)

	key = 'using namespace ToolUtils;'
	data = data.replace(key, key+TOONZ_VBRUSH_HEADER)

	open(fname,'wb').write(data.encode('utf-8'))


def build_opentoonz():
	if not os.path.isdir(OPENTOONZ_BUILD):
		os.mkdir(OPENTOONZ_BUILD)
	srcdir = os.path.abspath('./opentoonz/toonz/sources')
	assert os.path.isdir(srcdir)

	patch_toonz_tif()
	patch_toonz_tzp()
	patch_toonz_cmake()
	patch_toonz_toplevel_cmake()
	patch_toonz_main()
	patch_toonz_tenv()
	patch_toonz_mainwin()
	patch_toonz_vbrush()

	cmd = [
		'cmake', srcdir,
		'-DWITH_SYSTEM_LZO=ON',
		'-DWITH_SYSTEM_SUPERLU=ON',
		'-DWITH_CANON=OFF',
		'-DWITH_TRANSLATION=OFF',
		'-DWITH_WINTAB=OFF',
	]
	if sys.platform=='darwin':
		cmd.extend([
		'-DQT_PATH='+QT_LIB_DIR,

		## brew was missing tiffiop.h
		#'-DTIFF_INCLUDE_DIR=/usr/local/opt/libtiff/include',
		#'-DTIFF_LIB=/usr/local/opt/libtiff/lib',
		#'-DTIFF_LIBRARY=/usr/local/opt/libtiff/lib/libtiff.dylib',

		'-DSUPERLU_INCLUDE_DIR=/usr/local/opt/superlu/include',
		'-DSUPERLU_LIB=/usr/local/opt/superlu/lib',
		'-DSUPERLU_LIBRARY=/usr/local/opt/superlu/lib/libsuperlu.dylib',
		])
	else:
		cmd.extend([
		#'-DTURBOJPEG_LIB=/usr/lib/x86_64-linux-gnu/libjpeg.so',
		## sudo apt-get install libturbojpeg0-dev
		#'-DTURBOJPEG_LIB=/usr/lib/x86_64-linux-gnu/libturbojpeg.a',

		'-DTURBOJPEG_LIB=/opt/libjpeg-turbo/lib64/libturbojpeg.a',
		#'-DTURBOJPEG_LIB=/opt/libjpeg-turbo/lib64/libjpeg.a',

		#'-DTURBOJPEG_INCLUDE_DIR=/opt/libjpeg-turbo/include',  ## not defined
		#/opt/libjpeg-turbo/lib64/libturbojpeg.so',

		'-DGLUT_LIB=/usr/lib/x86_64-linux-gnu/libglut.so.3',

		'-DTIFF_INCLUDE_DIR='+TIFF_INC_DIR,
		##'-DTIFF_LIB='+TIFF_LIB_DIR,
		'-DTIFF_LIBRARY='+TIFF_LIB,

		'-DSUPERLU_INCLUDE_DIR=/usr/include/superlu',
		'-DSUPERLU_LIB=/usr/lib/x86_64-linux-gnu',
		'-DSUPERLU_LIBRARY=/usr/lib/x86_64-linux-gnu/libsuperlu.so',
		])

	print(cmd)
	subprocess.check_call(cmd, cwd=OPENTOONZ_BUILD)
	subprocess.check_call(['make'], cwd=OPENTOONZ_BUILD)

	OPENTOONZ_STUFF = '/Applications/OpenToonz'
	if sys.platform=='darwin':
		if not os.path.isdir(OPENTOONZ_STUFF):
			print('installing opentoonz stuff...')
			os.system('sudo mkdir /Applications/OpenToonz')
			os.system('sudo cp -rv opentoonz/stuff /Applications/OpenToonz/OpenToonz_stuff')
			os.system('sudo chmod -R 777 /Applications/OpenToonz')

	## note above seems to be missing some stuff
	## workaround is to manually install https://github.com/opentoonz/opentoonz/releases/download/v1.6.0/OpenToonz.pkg
	#if '--test' in sys.argv:
	#	os.system('./buildtoonz/toonz/OpenToonz.app/Contents/MacOS/OpenToonz')
	assert os.path.isfile(LIBOPENTOONZ)
	os.system('ls -lh ' + LIBOPENTOONZ)


if not os.path.isdir(OPENTOONZ_PATCHED):
	os.mkdir(OPENTOONZ_PATCHED)

def findlibs(path):
	if not os.path.isdir(path):
		print('WARN: failed to find library: did you first run `./makebpy.py --toonz`')
		return []

	libs = []
	for name in os.listdir(path):
		if name.endswith(ext):
			assert name not in libs
			libs.append(name)

	return libs

TOONZ_PATHS = [
	'colorfx',
	'image',
	'mousedragfilter',
	'sound',
	'stdfx',
	'tcleanupper',
	'tconverter',
	'tnzbase',
	'tnzext',
	'tnztools',
	'toonz',
	'toonzfarm',
	'toonzlib',
	'toonzqt',
]

TOONZ_SRC_LIBS = {}
def find_toonz_libs():
	global TOONZ_SRC_LIBS
	TOONZ_SRC_LIBS = {}
	if sys.platform=='darwin':
		for path in TOONZ_PATHS:
			libs = findlibs(os.path.join(OPENTOONZ_BUILD, path))
			if len(libs):
				TOONZ_SRC_LIBS[path] = libs
	else:
		linux_libs = os.path.join(OPENTOONZ_BUILD,'lib/opentoonz')
		if os.path.isdir(linux_libs):
			libs = []
			for name in os.listdir(linux_libs):
				if name.endswith('.so'):
					libs.append( os.path.join(linux_libs,name) )
			TOONZ_SRC_LIBS['ALL'] = libs

find_toonz_libs()
print('libtoonz prebuilt libs:', TOONZ_SRC_LIBS)

def patch_blender_exe( bexe ):
	if not sys.platform=='darwin': return
	src = os.path.abspath(bexe)
	assert os.path.isfile(src)
	deps = subprocess.run(['otool', '-L', src], capture_output=True)

	dofix = False
	for ln in deps.stdout.splitlines():
		ln = ln.decode('utf-8')
		if ' ' not in ln: continue
		ln = ln.strip().split()[0]
		if ln=='libqtmypaint.1.dylib':
			print(ln)
			dofix=True
			break

	if dofix:
		cmd = [
			'install_name_tool', '-change', 'libqtmypaint.1.dylib',
			os.path.abspath(QTMYPAINT_LIB),
			src
		]
		print(cmd)
		subprocess.check_call(cmd)

	if '--verbose' in sys.argv:
		print('checking patched:')
		subprocess.check_call(['otool', '-L', src])



def patchlib(libpath, libname):
	lib = os.path.join(libpath, libname)
	src = os.path.join(OPENTOONZ_BUILD, lib)
	print(src)
	assert os.path.isfile(src)
	deps = subprocess.run(['otool', '-L', src], capture_output=True)

	others = []
	for ln in deps.stdout.splitlines():
		ln = ln.decode('utf-8')
		if ' ' not in ln: continue
		ln = ln.strip().split()[0]
		if ln.startswith('@executable_path/'):
			print(ln)
			other_name = ln.split('/')[-1]
			if other_name not in others:
				if libname != other_name:
					others.append( other_name )

	path = os.path.join(OPENTOONZ_PATCHED, libname)
	open(path, 'wb').write(
		open(src,'rb').read()
	)
	#subprocess.check_call(['install_name_tool', '-id', libname, path])
	subprocess.check_call(['install_name_tool', '-id', os.path.abspath(path), path])

	if others:
		print('library: %s - deps: %s' %(libname, others))
		for other in others:
			cmd = [
				'install_name_tool', '-change', '@executable_path/'+other,
				#'@loader_path/' + other
				#other,   ## just load from same folder for now - breaks on Blender from Blender.org
				os.path.abspath(os.path.join(OPENTOONZ_PATCHED,other)),   ## testing TODO
				path
			]
			print(cmd)
			subprocess.check_call(cmd)

		print('checking patched:')
		subprocess.check_call(['otool', '-L', path])

	return path

def loadlib(lib):
	return ctypes.CDLL(lib.split('/')[-1])

def patch_toonz_libs():
	if os.path.isdir('/Applications/OpenToonz/OpenToonz_stuff'):
		os.system(
			'cp -R /Applications/OpenToonz/OpenToonz_stuff/* /tmp/portablestuff/.'
		)
	patched = []
	#libs = [
	#	'tnzcore/tnzcore',
	#	'image/image',
	#	'toonz/OpenToonz',
	#]
	#for name in libs:
	#	p = patchlib(*name.split('/'))
	#	patched.append(p)
	if not len(TOONZ_SRC_LIBS):
		find_toonz_libs()

	if sys.platform=='linux':
		for src in TOONZ_SRC_LIBS['ALL']:
			print('copy:', src)
			libname = src.split('/')[-1]
			path = os.path.join(OPENTOONZ_PATCHED, libname)
			print('to:', path)
			open(path, 'wb').write(
				open(src,'rb').read()
			)
			patched.append(path)

	else:
		for path in TOONZ_SRC_LIBS:
			for lib in TOONZ_SRC_LIBS[path]:
				patched.append(
					patchlib(path, lib)
				)

	if '--test-ctypes' in sys.argv:
		start_toonz()
		loaded = []
		for p in patched:
			lib = loadlib(p)
			print(lib)
			loaded.append(lib)
			print('')
			print('')
		sys.exit()

	if not os.path.isdir(os.path.join(OPENTOONZ_PATCHED, 'portablestuff')):
		os.mkdir(os.path.join(OPENTOONZ_PATCHED, 'portablestuff'))
	#if not os.path.isdir(os.path.join('/tmp', 'portablestuff')):
	#	os.mkdir(os.path.join('/tmp', 'portablestuff'))


if '--toonz' in sys.argv:
	build_opentoonz()
	patch_toonz_libs()
	sys.exit()
	time.sleep(3)

def setup_bl_ui():
	path = os.path.join(BLENDER_SRC, 'release/scripts/startup/bl_ui')
	assert os.path.isdir(path)
	bl = './bl_ui'
	idir = './buildblenderqt/bin/Blender.app/Contents/Resources/3.4/scripts/startup/bl_ui'

	for name in os.listdir(bl):
		print('saving:', name)
		data = open(os.path.join(bl,name),'rb').read()
		dst = os.path.join(path, name)
		print('to:',dst)
		open(dst, 'wb').write(data)
		if os.path.isdir(idir):
			dst = os.path.join(idir, name)
			print('to:',dst)
			open(dst, 'wb').write(data)
			idir = './buildblenderqt/bin/Blender.app/Contents/Resources/3.4/scripts/startup/bl_ui'

if MAYA_INSTALLED:
	setup_bl_ui()

def setup_ghost_qt():
	ghost = os.path.join(BLENDER_SRC, 'intern/ghost/intern')
	assert os.path.isdir(ghost)
	gqt = './ghostQT'
	for name in os.listdir(gqt):
		if 'QT' not in name:
			continue
		print('saving:', name)
		data = open(os.path.join(gqt,name),'rb').read()
		open(os.path.join(ghost, name), 'wb').write(data)

	patch_ghost_cmake()
	patch_creator()
	patch_creator_cmake()
	patch_blender_cmake()

	patch_ghost_isystem()
	patch_ghost_isystem_paths()
	patch_ghost_context()
	patch_ghost_context_egl()


GHOST_QT_CMAKE = '''
if(WITH_QT)
	add_definitions(-DWITH_QT)
	list(APPEND SRC
		intern/GHOST_SystemPathsQT.cpp
		intern/GHOST_DisplayManagerQT.cpp
		intern/GHOST_ContextQT.cpp
		intern/GHOST_SystemQT.cpp
		intern/GHOST_WindowQT.cpp
		intern/GHOST_SystemPathsQT.h
		intern/GHOST_DisplayManagerQT.h
		intern/GHOST_ContextQT.h
		intern/GHOST_SystemQT.h
		intern/GHOST_WindowQT.h
	)
	list(APPEND LIB
	%s
	)
	list(APPEND INC_SYS
	%s
	)
endif()
blender_add_lib(bf_intern_ghost "${SRC}" "${INC}" "${INC_SYS}" "${LIB}")
''' % (get_qt_libs(), QT_INC)

CMAKE_HACK_APPLE = '''
if(WITH_QT)

elseif(APPLE)
'''

def patch_ghost_cmake():
	fname = os.path.join(BLENDER_SRC, 'intern/ghost/CMakeLists.txt')
	assert os.path.isfile(fname)
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	bad = 'elseif(APPLE AND NOT WITH_GHOST_X11)'
	assert data.count(bad)==1
	data = data.replace(bad, 'elseif(0)')

	bad = 'if(APPLE)'
	assert data.count(bad)==1
	data = data.replace(bad, CMAKE_HACK_APPLE)

	key = 'blender_add_lib(bf_intern_ghost "${SRC}" "${INC}" "${INC_SYS}" "${LIB}")'
	assert data.count(key)==1
	print('patching ghost CMakeLists.txt')
	data = data.replace(key, GHOST_QT_CMAKE)
	open(fname,'wb').write(data.encode('utf-8'))

def patch_creator():
	data = open('./mender.cpp','rb').read()
	fname = os.path.join(BLENDER_SRC, 'source/creator/mender.cpp')
	open(fname, 'wb').write(data)

	data = open('./mender.h','rb').read()
	fname = os.path.join(BLENDER_SRC, 'source/creator/mender.h')
	open(fname, 'wb').write(data)

	data = open('./weblender.cpp','rb').read()
	fname = os.path.join(BLENDER_SRC, 'source/creator/weblender.cpp')
	open(fname, 'wb').write(data)



MENDER_CMAKE_INC = '''
set(INC
%s
''' % QT_INC

MENDER_CMAKE_LIB = '''
set(LIB
%s
%s
''' % (get_qt_libs(), get_toonz_libs())

EM_CMAKE_BAD = '''set(LIB
  bf_windowmanager
)'''


BLENDER_WASM_LIB_TAGS = '''
bf_font
bf_intern_memutil
bf_gpencil
bf_blenloader
extern_curve_fit_nd
extern_rangetree
extern_clew
bf_geometry
bf_intern_iksolver
bf_intern_opencolorio
bf_blenkernel
bf_nodes_texture
#bf_rna
bf_dna
bf_dna_blenlib
extern_lzma
bf_ikplugin
bf_blenlib
#bf_wavefront_obj
bf_intern_guardedalloc
bf_modifiers
bf_bmesh
bf_nodes
#bf_intern_opensubdiv
bf_gpencil_modifiers
bf_intern_eigen
#bf_nodes_shader
extern_hipew
bf_intern_libc_compat
#bf_nodes_composite
#bf_intern_ghost
#bf_intern_libmv
extern_cuew
extern_minilzo
bf_stl
bf_windowmanager
bf_intern_clog
bf_intern_sky
bf_depsgraph
bf_functions
bf_io_common
bf_nodes_function
extern_wcwidth
bf_nodes_geometry
bf_imbuf
'''.strip()

BLENDER_WASM_LIBS = []
for blib in BLENDER_WASM_LIB_TAGS.splitlines():
	if blib.startswith('#'): continue
	BLENDER_WASM_LIBS.append(blib)

EM_CMAKE_LIBS = '''
set(LIB
%s
)
''' % '\n'.join(BLENDER_WASM_LIBS)

nbytes = 0
def list_static_libs(path):
	global nbytes
	for name in os.listdir(path):
		p = os.path.join(path, name)
		if p.endswith('.a'):
			#print(p)
			os.system('ls -lh ' + p)
			print(name[3:-2])
			nbytes += len(open(p,'rb').read())
		elif os.path.isdir(p):
			list_static_libs(p)


if '--list-blender-libs' in sys.argv:
	list_static_libs('./buildwbpy/')
	print('static libs total bytes:', nbytes)
	kb = nbytes / 1024
	print('static libs total KB:', kb)
	mb = nbytes / (1024*1024)
	print('static libs total MB:', mb)

	sys.exit()

def patch_creator_cmake():
	fname = os.path.join(BLENDER_SRC, 'source/creator/CMakeLists.txt')
	assert os.path.isfile(fname)
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	key = 'creator.c'
	assert data.count(key)==1

	if '--wasm' in sys.argv or '--headless' in sys.argv:
		data = data.replace(key, 'weblender.cpp')

		if '--use-window-manager' in sys.argv:
			pass
		else:
			assert data.count(EM_CMAKE_BAD)==1
			data = data.replace(EM_CMAKE_BAD, EM_CMAKE_LIBS)
	else:
		data = data.replace(key, 'mender.cpp')

		key = 'set(INC'
		assert data.count(key)==1
		data = data.replace(key, MENDER_CMAKE_INC)
		key = '\nset(LIB'  ## because of `unset(LIB)
		assert data.count(key)==1
		data = data.replace(key, MENDER_CMAKE_LIB)

	open(fname,'wb').write(data.encode('utf-8'))

def patch_blender_cmake():
	fname = os.path.join(BLENDER_SRC, 'source/blender/CMakeLists.txt')
	assert os.path.isfile(fname)
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))


	if '--wasm' in sys.argv or '--headless' in sys.argv:
		data = BLENDER_WASM_CMAKE

	open(fname,'wb').write(data.encode('utf-8'))


BLENDER_WASM_CMAKE = '''
# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright 2006 Blender Foundation. All rights reserved.

set(SRC_DNA_INC
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_ID.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_ID_enums.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_action_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_anim_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_armature_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_asset_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_boid_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_brush_enums.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_brush_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_cachefile_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_camera_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_cloth_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_collection_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_color_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_constraint_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_curve_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_curveprofile_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_curves_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_customdata_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_defs.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_documentation.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_dynamicpaint_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_effect_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_fileglobal_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_fluid_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_freestyle_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_genfile.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_gpencil_modifier_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_gpencil_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_gpu_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_image_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_ipo_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_key_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_lattice_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_layer_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_light_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_lightprobe_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_lineart_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_linestyle_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_listBase.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_mask_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_material_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_mesh_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_meshdata_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_meta_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_modifier_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_movieclip_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_nla_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_node_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_object_enums.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_object_fluidsim_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_object_force_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_object_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_outliner_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_packedFile_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_particle_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_pointcache_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_pointcloud_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_rigidbody_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_scene_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_screen_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_sdna_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_sequence_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_session_uuid_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_shader_fx_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_simulation_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_sound_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_space_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_speaker_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_text_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_texture_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_tracking_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_userdef_enums.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_userdef_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_uuid_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_vec_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_vfont_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_view2d_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_view3d_enums.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_view3d_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_viewer_path_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_volume_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_windowmanager_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_workspace_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_world_types.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_xr_types.h
)

set(SRC_DNA_DEFAULTS_INC
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_armature_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_asset_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_brush_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_cachefile_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_camera_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_collection_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_curve_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_curves_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_fluid_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_gpencil_modifier_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_image_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_lattice_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_light_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_lightprobe_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_linestyle_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_material_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_mesh_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_meta_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_modifier_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_movieclip_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_object_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_particle_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_pointcloud_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_scene_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_simulation_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_space_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_speaker_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_texture_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_vec_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_view3d_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_volume_defaults.h
	${CMAKE_CURRENT_SOURCE_DIR}/makesdna/DNA_world_defaults.h
)

#add_subdirectory(datatoc)
#add_subdirectory(editors)
#add_subdirectory(windowmanager)
add_subdirectory(blenkernel)
add_subdirectory(blenlib)
add_subdirectory(bmesh)
#add_subdirectory(draw)
#add_subdirectory(render)
add_subdirectory(blender27render)
add_subdirectory(blenfont)
#add_subdirectory(blentranslation)
add_subdirectory(blenloader)
add_subdirectory(depsgraph)
add_subdirectory(ikplugin)
#add_subdirectory(simulation)
add_subdirectory(geometry)
#add_subdirectory(gpu)
add_subdirectory(imbuf)
add_subdirectory(nodes)
add_subdirectory(modifiers)
add_subdirectory(gpencil_modifiers)
#add_subdirectory(sequencer)
#add_subdirectory(shader_fx)
add_subdirectory(io)
add_subdirectory(functions)
add_subdirectory(makesdna)
add_subdirectory(makesrna)
#add_subdirectory(compositor)
'''

def patch_blf_font():
	fname = os.path.join(BLENDER_SRC, 'source/blender/blenfont/intern/blf_font.c')
	assert os.path.isfile(fname)
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	bad = 'FT_Done_MM_Var(font->ft_lib, font->variations);'
	assert data.count(bad)==1
	data = data.replace(bad, '/*TODO free font mem on platform EMSCRIPTEN*/')
	open(fname,'wb').write(data.encode('utf-8'))

patch_blf_font()

def patch_blf_glyph():
	fname = os.path.join(BLENDER_SRC, 'source/blender/blenfont/intern/blf_glyph.c')
	assert os.path.isfile(fname)
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	bad = 'FT_Get_Var_Design_Coordinates(glyph_font->face, BLF_VARIATIONS_MAX, &coords[0]);'
	assert data.count(bad)==1
	data = data.replace(bad, '/*TODO ft get var design coords on platform EMSCRIPTEN*/')
	open(fname,'wb').write(data.encode('utf-8'))

patch_blf_glyph()

def patch_creator_old():
	fname = os.path.join(BLENDER_SRC, 'source/creator/creator.c')
	assert os.path.isfile(fname)
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	bad = '#if defined(__APPLE__) && !defined(WITH_PYTHON_MODULE) && !defined(WITH_HEADLESS)'
	assert data.count(bad)==1
	data = data.replace(bad, '#if 0')
	open(fname,'wb').write(data.encode('utf-8'))

GHOST_ISYSTEM_HACK_HEAD_OSX = '''
#ifdef WITH_QT
	#include "GHOST_SystemQT.h"
#elif defined(WITH_GHOST_X11) && defined(WITH_GHOST_WAYLAND)
'''

GHOST_ISYSTEM_HACK_HEAD_LINUX = '''
#ifdef WITH_QT
	#warning "using GhostQT - very experimental"
#elif defined(WITH_GHOST_X11) && defined(WITH_GHOST_WAYLAND)
'''


GHOST_ISYSTEM_HACK = '''
#elif defined(WITH_QT)
	m_system = new GHOST_SystemQT();
#elif defined(WITH_GHOST_X11) && defined(WITH_GHOST_WAYLAND)
'''

GHOST_ISYSTEM_LINUX_PRE = '''
#ifdef WITH_QT
	#include "GHOST_SystemQT.h"
#endif
'''

def patch_ghost_isystem():
	fname = os.path.join(BLENDER_SRC, 'intern/ghost/intern/GHOST_ISystem.cpp')
	assert os.path.isfile(fname)
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	bad = '#elif defined(WITH_GHOST_X11) && defined(WITH_GHOST_WAYLAND)'
	assert data.count(bad)==1
	data = data.replace(bad, GHOST_ISYSTEM_HACK)

	bad = '#if defined(WITH_GHOST_X11) && defined(WITH_GHOST_WAYLAND)'
	assert data.count(bad)==1
	if sys.platform=='darwin':
		data = data.replace(bad, GHOST_ISYSTEM_HACK_HEAD_OSX)
	else:
		data = data.replace(bad, GHOST_ISYSTEM_HACK_HEAD_LINUX)
		data = GHOST_ISYSTEM_LINUX_PRE + data

	open(fname,'wb').write(data.encode('utf-8'))


def patch_ghost_isystem_paths():
	fname = os.path.join(BLENDER_SRC, 'intern/ghost/intern/GHOST_ISystemPaths.cpp')
	assert os.path.isfile(fname)
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	bad = 'WIN32'
	assert data.count(bad)==2
	data = data.replace(bad, 'WITH_QT')

	bad = 'Win32'
	assert data.count(bad)==2
	data = data.replace(bad, 'QT')

	open(fname,'wb').write(data.encode('utf-8'))


GHOST_HACK_CTX = '''
#ifdef EMSCRIPTEN
	#include <GLES2/gl2.h>
	#include <GLES2/gl2ext.h>
#else
	#include <epoxy/gl.h>
#endif
'''

def patch_ghost_context():
	fname = os.path.join(BLENDER_SRC, 'intern/ghost/intern/GHOST_Context.h')
	assert os.path.isfile(fname)
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	bad = '#include <epoxy/gl.h>'
	assert data.count(bad)==1
	data = data.replace(bad, GHOST_HACK_CTX)

	open(fname,'wb').write(data.encode('utf-8'))

GHOST_HACK_CTX_EGL = '''
#ifdef EMSCRIPTEN
	#include <EGL/egl.h>
#else
	#include <epoxy/egl.h>
#endif
'''

def patch_ghost_context_egl():
	fname = os.path.join(BLENDER_SRC, 'intern/ghost/intern/GHOST_ContextEGL.h')
	assert os.path.isfile(fname)
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	bad = '#include <epoxy/egl.h>'
	assert data.count(bad)==1
	data = data.replace(bad, GHOST_HACK_CTX_EGL)

	bad = '#include <epoxy/gl.h>'
	assert data.count(bad)==1
	data = data.replace(bad, GHOST_HACK_CTX)

	open(fname,'wb').write(data.encode('utf-8'))


	fname = os.path.join(BLENDER_SRC, 'intern/ghost/intern/GHOST_ContextEGL.cpp')
	assert os.path.isfile(fname)
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	if '--wasm' in sys.argv or '--headless' in sys.argv:
		data = open('./ghostQT/GHOST_ContextEGL.cpp', 'rb').read().decode('utf-8')

	open(fname,'wb').write(data.encode('utf-8'))


setup_ghost_qt()

PLATFORM_HEADLESS = '''


'''

def patch_platform_unix():
	fname = os.path.join(BLENDER_SRC, 'build_files/cmake/platform/platform_unix.cmake')
	assert os.path.isfile(fname)
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	if '--wasm' in sys.argv:
		## note to fix jpeg include error, `embuilder build libjpeg` forces download and cache to sysroot
		data = open('./blender_platform_wasm.cmake','rb').read().decode('utf-8')
	elif '--headless' in sys.argv:
		data = PLATFORM_HEADLESS
	elif sys.platform=='linux':
		## this fix is just needed on ubuntu20?
		bad = 'check_freetype_for_brotli()'
		assert data.count(bad)==2
		data = data.replace(bad, '#'+bad)

	open(fname,'wb').write(data.encode('utf-8'))

patch_platform_unix()


def ensure_emcmake_will_work():
	## this is an issue on debian using `apt-get install emscripten`
	## because it puts the symlinks in /usr/bin
	## when it should also be in both for cmake builds to work
	if not os.path.isfile('/usr/share/emscripten/emcc'):
		if os.path.isfile('/usr/bin/emcc'):
			for name in 'em++ embuilder emcmake emconfigure emranlib emscons emar emcc em-config emmake emrun emsize'.split():
				cm = 'sudo ln -s /usr/bin/%s /usr/share/emscripten/%s' % (name,name)
				print(cm)
				subprocess.check_call(cm.split())


cmd = []
if '--wasm' in sys.argv:
	#ensure_blender_wasm_dna()  ## will not work here
	if os.path.isfile('/usr/bin/emcmake'):
		cmd = [os.path.expanduser('/usr/bin/emcmake')]
		if sys.platform=='linux':
			ensure_emcmake_will_work()
	else:
		cmd = [os.path.expanduser('~/emsdk/upstream/emscripten/emcmake')]

cmd.extend([
	'cmake', os.path.abspath('./blender'),
])

if '--qt' in sys.argv:

	cmd.extend([
	'-DWITH_QT=ON',
	'-DWITH_GHOST_X11=OFF',
	'-DWITH_GHOST_WAYLAND=OFF',
	])

elif '--wasm' in sys.argv or '--32bits' in sys.argv or '--headless' in sys.argv:
	#os.environ['CMAKE_TOOLCHAIN_PATH']=os.path.expanduser('~/emsdk/upstream/emscripten/cmake/Modules/Platform/Emscripten.cmake')

	if '--32bits' in sys.argv:
		## this fails on osx default clang and brew gcc12
		cmd.extend([
			'-DCMAKE_C_FLAGS=-m32',
			'-DCMAKE_CXX_FLAGS=-m32',
			#https://stackoverflow.com/questions/52830484/nasm-cant-link-object-file-with-ld-on-macos-mojave
			#'-DCMAKE_EXE_LINKER_FLAGS=/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/usr/lib -lSystem',

			'-DCMAKE_C_COMPILER=/usr/local/bin/gcc-12',
			'-DCMAKE_CXX_COMPILER=/usr/local/bin/g++-12',
		])

	cmd.extend([
	#'-DCMAKE_TOOLCHAIN_FILE=%s' % os.path.expanduser('~/emsdk/upstream/emscripten/cmake/Modules/Platform/Emscripten.cmake'),
	#'-DCMAKE_CROSSCOMPILING_EMULATOR=%s' % os.path.expanduser('~/emsdk/node/14.18.2_64bit/bin/node'),  ## this is required?
	'-DWITH_PYTHON_MODULE=OFF',
	'-DWITH_QT=OFF',
	'-DWITH_PYTHON=OFF',
	'-DWITH_HEADLESS=ON',
	'-DWITH_CODEC_AVI=OFF',
	'-DWITH_IMAGE_OPENJPEG=OFF',
	'-DWITH_IMAGE_TIFF=OFF',
	'-DWITH_JEMALLOC=OFF',
	'-DWITH_GMP=OFF',
	'-DWITH_HARU=OFF',
	'-DWITH_IMAGE_WEBP=OFF',

	'-DWITH_ALEMBIC=OFF',

	## needs datatoc
	'-DWITH_BLENDER_THUMBNAILER=OFF',
	'-DWITH_OPENSUBDIV=OFF',

	])

else:

	cmd.extend([
	'-DWITH_PYTHON_MODULE=ON',
	'-DWITH_QT=OFF',
	])

cmd.extend([
	'-DWITH_PYTHON_INSTALL=OFF',
	#'-DTEST_PYTHON_EXE='+ MAYAPY,
])

if MAYA_INSTALLED:
	cmd.extend([
	'-DPYTHON_VERSION=%s.%s' %(PYVERSION[0], PYVERSION[1]),
	'-DPYTHON_INCLUDE_DIR='+PYTHON_INC,
	#-DCMAKE_INSTALL_PREFIX=TODO some maya path
	])

cmd.extend([
	'-DWITH_BUILDINFO=OFF',
	'-DWITH_NUMPY=OFF',
	'-DWITH_MOD_FLUID=OFF',
	'-DWITH_MOD_REMESH=OFF',
	'-DWITH_SDL=OFF',
	'-DWITH_OPENAL=OFF',
	'-DWITH_COREAUDIO=OFF',
	'-DWITH_CODEC_SNDFILE=OFF',
	'-DWITH_IMAGE_CINEON=OFF',
	'-DWITH_IMAGE_DDS=OFF',
	'-DWITH_IMAGE_HDR=OFF',
	'-DWITH_IMAGE_OPENEXR=OFF',

	'-DWITH_OPENMP=OFF',
	'-DWITH_IMAGE_OPENEXR=OFF',
	'-DWITH_CODEC_FFMPEG=OFF',
	'-DWITH_TBB=OFF',
	'-DWITH_JACK=OFF',
	'-DWITH_XR_OPENXR=OFF',
	'-DWITH_OPENIMAGEIO=OFF',

	'-DWITH_DRACO=OFF',
	'-DWITH_BULLET=OFF',
	'-DWITH_FFTW3=OFF', '-DWITH_MOD_OCEANSIM=OFF',
	'-DWITH_FREESTYLE=OFF',
	'-DWITH_IK_ITASC=OFF',
	'-DWITH_INPUT_NDOF=OFF',
	'-DWITH_INPUT_IME=OFF',
	'-DWITH_OPENCOLLADA=OFF',
	'-DWITH_OPENCOLORIO=OFF',
	'-DWITH_OPENIMAGEDENOISE=OFF',
	'-DWITH_OPENVDB=OFF',
	'-DWITH_POTRACE=OFF',
	'-DWITH_PUGIXML=OFF',
	'-DWITH_USD=OFF',

	'-DWITH_LIBMV=OFF',
	'-DWITH_CERES=OFF',
	'-DWITH_AUDASPACE=OFF',

])

if USE_CYCLES:
	cmd.extend([
		'-DWITH_CYCLES=ON',
		'-DWITH_BOOST=ON',
		'-DWITH_LLVM=ON',
	])
else:
	cmd.extend([
		'-DWITH_CYCLES=OFF',
		'-DWITH_BOOST=OFF',
		'-DWITH_LLVM=OFF',
	])


if sys.platform=='linux':
	cmd.extend([
	#'-DWITH_LEGACY_OPENGL=ON',  ## probably not used anymore

	'-DWITH_SYSTEM_FREETYPE=TRUE',
	#'-DFREETYPE_INCLUDE_DIRS=' + FREETYPE_INCLUDE_DIRS,
	'-DFREETYPE_INCLUDE_DIRS=/usr/local/include/freetype2',

	#'-DFT_CONFIG_OPTION_USE_BROTLI=OFF',
	'-DWITH_OPENSUBDIV=OFF',
	'-DWITH_POTRACE=OFF',
	'-DWITH_GHOST_WAYLAND=OFF',
	])
	#raise RuntimeError(cmd)

CMAKE_MAIN_HEAD = '''
if(WITH_QT)
	add_definitions(-DWITH_QT)
endif()
'''
if os.path.isfile('/usr/bin/node'):
	NODEJS = '/usr/bin/node'
else:
	NODEJS = os.path.expanduser('~/emsdk/node/14.18.2_64bit/bin/node')

def patch_dna_cmake():
	fname = 'blender/source/blender/makesdna/intern/CMakeLists.txt'
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	key = '"$<TARGET_FILE:makesdna>"'
	assert data.count(key)==1

	if '--wasm' in sys.argv:
		#data = data.replace(key, '%s "$<TARGET_FILE:makesdna>"' % NODEJS)
		data = data.replace(key, 'echo "$<TARGET_FILE:makesdna>"')

	open(fname,'wb').write(data.encode('utf-8'))

patch_dna_cmake()

def patch_rna_cmake():
	fname = 'blender/source/blender/makesrna/intern/CMakeLists.txt'
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	key = '"$<TARGET_FILE:makesrna>"'
	assert data.count(key)==1

	if '--wasm' in sys.argv:
		#data = data.replace(key, '%s "$<TARGET_FILE:makesdna>"' % NODEJS)
		data = data.replace(key, 'echo "$<TARGET_FILE:makesrna>"')

	open(fname,'wb').write(data.encode('utf-8'))

patch_rna_cmake()


def patch_cmake():
	fname = 'blender/CMakeLists.txt'
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	bad = 'if(DEFINED PYTHON_VERSION AND "${PYTHON_VERSION}" VERSION_LESS "3.10")'
	assert data.count(bad)==1
	print('patching CMakeLists.txt')
	data = data.replace(bad, 'if(0)')
	data = CMAKE_MAIN_HEAD + data
	open(fname,'wb').write(data.encode('utf-8'))

patch_cmake()

blend_bug_material_lineart = '''  prop = RNA_def_property(srna, "mat_occlusion", PROP_INT, PROP_NONE);
  RNA_def_property_int_default(prop, 1);
  RNA_def_property_ui_range(prop, 0.0f, 5.0f, 1.0f, 1);'''

blend_bug_material_lineart_fix = '''
  prop = RNA_def_property(srna, "mat_occlusion", PROP_INT, PROP_NONE);
  RNA_def_property_int_default(prop, 1);
  RNA_def_property_range(prop, 0, 255);
'''


def patch_lineart_bug():
	fname = 'blender/source/blender/makesrna/intern/rna_material.c'
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	if blend_bug_material_lineart in data:
		print("WARN: your blender source code is old - and has lineart material bug - patching...")
		data = data.replace(blend_bug_material_lineart, blend_bug_material_lineart_fix)

	open(fname,'wb').write(data.encode('utf-8'))

patch_lineart_bug()

blend_bug_userdef_types = '''  /** Generic gizmo size. */
  char gizmo_size;
  /** Navigate gizmo size. */
  char gizmo_size_navigate_v3d;
'''

blend_bug_userdef_types2 = '''  /** Drag pixels (scaled by DPI). */
  char drag_threshold_mouse;
  char drag_threshold_tablet;
  char drag_threshold;
  char move_threshold;
'''

blend_bug_userdef_types_fix = '''  /** Generic gizmo size. */
  unsigned char gizmo_size;
  /** Navigate gizmo size. */
  unsigned char gizmo_size_navigate_v3d;
'''

blend_bug_userdef_types2_fix = '''  /** Drag pixels (scaled by DPI). */
  unsigned char drag_threshold_mouse;
  unsigned char drag_threshold_tablet;
  unsigned char drag_threshold;
  unsigned char move_threshold;
'''

def patch_userdef_types_bug():
	fname = 'blender/source/blender/makesdna/DNA_userdef_types.h'
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	if blend_bug_userdef_types in data:
		print("WARN: your blender source code is old - and has userdef types bug - patching...")
		data = data.replace(blend_bug_userdef_types, blend_bug_userdef_types_fix)
		assert data.count(blend_bug_userdef_types2)==1
		data = data.replace(blend_bug_userdef_types2, blend_bug_userdef_types2_fix)
	else:
		raise RuntimeError('NOT YET TESTED')

	open(fname,'wb').write(data.encode('utf-8'))

patch_userdef_types_bug()

#define _LARGEFILE64_SOURCE
EM_FILE_FIX = '''
#ifdef EMSCRIPTEN
	typedef int64_t off64_t;
#endif
'''

def patch_bli_filereader():
	fname = 'blender/source/blender/blenlib/BLI_filereader.h'
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	key = '#pragma once'
	assert data.count(key)==1
	data = data.replace(key, key+EM_FILE_FIX)
	open(fname,'wb').write(data.encode('utf-8'))

patch_bli_filereader()

EM_PYLIKE_FIX = '''
#ifdef EMSCRIPTEN
	#define FE_DIVBYZERO 0
	#define FE_INVALID 0
#endif
'''

def patch_pylike_eval():
	fname = 'blender/source/blender/blenlib/intern/expr_pylike_eval.c'
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	data = EM_PYLIKE_FIX + data
	open(fname,'wb').write(data.encode('utf-8'))

patch_pylike_eval()

def patch_fileops():
	fname = 'blender/source/blender/blenlib/intern/fileops.c'
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	if '--wasm' in sys.argv:
		data = open('./blendwasm/fileops.c','rb').read().decode('utf-8')

	open(fname,'wb').write(data.encode('utf-8'))

patch_fileops()

EM_FILE_ZSTD_FIX = '''
#ifdef EMSCRIPTEN
	#include <string.h>
	#include "BLI_blenlib.h"
	#include "BLI_endian_switch.h"
	#include "BLI_filereader.h"

	FileReader *BLI_filereader_new_zstd(FileReader *base){
		return NULL;
	}

#else
'''

def patch_filereader_zstd():
	fname = 'blender/source/blender/blenlib/intern/filereader_zstd.c'
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	data = EM_FILE_ZSTD_FIX + data + '\n#endif'

	open(fname,'wb').write(data.encode('utf-8'))

patch_filereader_zstd()

def patch_writefile():
	fname = 'blender/source/blender/blenloader/intern/writefile.cc'
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	if '--wasm' in sys.argv:
		data = open('./blendwasm/writefile.cc', 'rb').read().decode('utf-8')

	open(fname,'wb').write(data.encode('utf-8'))

patch_writefile()


EM_STORAGE_FIX = '''
#ifdef EMSCRIPTEN
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

#include <sys/stat.h>

#if defined(__NetBSD__) || defined(__DragonFly__) || defined(__HAIKU__)
/* Other modern unix OS's should probably use this also. */
#  include <sys/statvfs.h>
#  define USE_STATFS_STATVFS
#endif

#if defined(__linux__) || defined(__hpux) || defined(__GNU__) || defined(__GLIBC__)
#  include <sys/vfs.h>
#endif

#include <fcntl.h>
#include <string.h> /* `strcpy` etc. */


/* lib includes */
#include "MEM_guardedalloc.h"

#include "BLI_fileops.h"
#include "BLI_linklist.h"
#include "BLI_path_util.h"
#include "BLI_string.h"
#include "BLI_utildefines.h"

char *BLI_current_working_dir(char *dir, const size_t maxncpy)
{
	return "/tmp";
}

double BLI_dir_free_space(const char *dir)
{
	return 0;
}

int64_t BLI_ftell(FILE *stream)
{
	return 0;
}

int BLI_fseek(FILE *stream, int64_t offset, int whence)
{
	return 0;
}

int64_t BLI_lseek(int fd, int64_t offset, int whence)
{
	return 0;
}

size_t BLI_file_descriptor_size(int file)
{
	return 0;
}

size_t BLI_file_size(const char *path)
{
	return 0;
}

/* Return file attributes. Apple version of this function is defined in storage_apple.mm */
eFileAttributes BLI_file_attributes(const char *path)
{
	return 0;
}

/* Return alias/shortcut file target. Apple version is defined in storage_apple.mm */
bool BLI_file_alias_target(const char *filepath,
													 /* This parameter can only be `const` on Linux since
														* redirection is not supported there.
														* NOLINTNEXTLINE: readability-non-const-parameter. */
													 char r_targetpath[/*FILE_MAXDIR*/])
{
	return false;
}


int BLI_exists(const char *path)
{
	return 0;
}


int BLI_stat(const char *path, BLI_stat_t *buffer)
{
	return 0;
}

int BLI_fstat(int fd, struct stat *buffer)
{
//  return fstat(fd, buffer);
	return 0;
}

bool BLI_is_dir(const char *file)
{
//  return S_ISDIR(BLI_exists(file));
	return false;
}

bool BLI_is_file(const char *path)
{
//  const int mode = BLI_exists(path);
//  return (mode && !S_ISDIR(mode));
	return false;
}

/**
 * Use for both text and binary file reading.
 */
static void *file_read_data_as_mem_impl(FILE *fp,
																				bool read_size_exact,
																				size_t pad_bytes,
																				size_t *r_size)
{
	return NULL;
}

void *BLI_file_read_text_as_mem(const char *filepath, size_t pad_bytes, size_t *r_size)
{
	return NULL;
}

void *BLI_file_read_binary_as_mem(const char *filepath, size_t pad_bytes, size_t *r_size)
{
	return NULL;
}

void *BLI_file_read_text_as_mem_with_newline_as_nil(const char *filepath,
													bool trim_trailing_space,
													size_t pad_bytes,
													size_t *r_size)
{
	return NULL;
}

LinkNode *BLI_file_read_as_lines(const char *filepath) { return NULL; }

void BLI_file_free_lines(LinkNode *lines){
	BLI_linklist_freeN(lines);
}

bool BLI_file_older(const char *file1, const char *file2){ return false; }

#else

'''
def patch_storage():
	fname = 'blender/source/blender/blenlib/intern/storage.c'
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	data = EM_STORAGE_FIX + data + '\n#endif'

	open(fname,'wb').write(data.encode('utf-8'))

patch_storage()

def patch_string_utf8():
	fname = 'blender/source/blender/blenlib/intern/string_utf8.c'
	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak','wb').write(data.encode('utf-8'))

	bad = '(Chars)[0] & (Mask);'
	good = '( (unsigned int)((Chars)[0]) & (Mask) );'
	assert data.count(bad)==1
	data = data.replace(bad, good)

	bad = '(c & 0x3f) | 0x80;'
	good = '(char)( (c & 0x3f) | 0x80 );'
	assert data.count(bad)==1
	data = data.replace(bad, good)

	open(fname,'wb').write(data.encode('utf-8'))

patch_string_utf8()


def patch_bpy_capi_utils():
	fname = 'blender/source/blender/python/intern/bpy_capi_utils.h'
	data = open(fname,'rb').read().decode('utf-8')
	bad = '''#  error "Python 3.10 or greater is required, you'll need to update your Python."'''
	if bad in data:
		print('patching bpy_capi_utils.h')
		open(fname+'.bak','wb').write(data.encode('utf-8'))
		data = data.replace(bad, '')
		open(fname,'wb').write(data.encode('utf-8'))
	else:
		print(fname + ' up to date')

patch_bpy_capi_utils()

def patch_bpy_driver():
	fname = 'blender/source/blender/python/intern/bpy_driver.c'
	data = open(fname,'rb').read().decode('utf-8')
	bads = ['OK_OP(GET_LEN),','OK_OP(ROT_N),']
	if bads[0] in data:
		print('patching', fname)
		open(fname+'.bak','wb').write(data.encode('utf-8'))
		for bad in bads:
			data = data.replace(bad, '')
		open(fname,'wb').write(data.encode('utf-8'))
	else:
		print(fname + ' up to date')

patch_bpy_driver()

def patch_mathutils():
	fname = 'blender/source/blender/python/mathutils/mathutils.c'
	data = open(fname,'rb').read().decode('utf-8')
	bad = '_Py_HashDouble(NULL,'
	if bad in data:
		print('patching', fname)
		open(fname+'.bak','wb').write(data.encode('utf-8'))
		data = data.replace(bad, '_Py_HashDouble(')
		open(fname,'wb').write(data.encode('utf-8'))
	else:
		print(fname + ' up to date')

if MAYA_INSTALLED:
	patch_mathutils()

## note: maya c++ api can not directly dirty a node
## https://around-the-corner.typepad.com/adn/2012/07/dirtying-a-maya-mplug-for-array-attribute.html

MAYA_PLUGIN_HEAD='''
#include "intern/depsgraph.h"


//#define SWIG
#define PLUGIN_EXPORT extern "C"
#include <maya/MFnPlugin.h>
//#undef SWIG

#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MGlobal.h>
#include <maya/MItGeometry.h>
#include <maya/MMatrix.h>
#include <maya/MPointArray.h>
#include <maya/MStatus.h>
#include <maya/MFnMesh.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MPxDeformerNode.h>
#include <maya/MAnimControl.h>
#include <maya/MEventMessage.h>
#include <iostream>
#include <vector>
#include <array>

static const MTypeId k_%s_ID = %s;
//extern "C"{
//	MStatus initializePlugin(MObject obj);
//	MStatus uninitializePlugin(MObject obj);
//}
'''#.replace(
#	'<maya/',
#	'</Applications/Autodesk/maya2023/include/maya/'
#)

## maya_verts needs to be: float (*vertexCos)[3]
MAYA2BLENDER_WAVE = '''
auto md = new WaveModifierData();
md->height=env;
md->width=1.0;
md->falloff=0.0;
md->timeoffs=0.0;
md->lifetime=1000.0;
md->startx=0.0;
md->starty=0.0;
md->speed=1.0;
md->narrow=0.5;
md->flag=MOD_WAVE_X;
md->texture=NULL;

auto ctx = new ModifierEvalContext();
//auto dep = new blender::deg::Depsgraph();  // no default constructor
blender::deg::Depsgraph *dep = (blender::deg::Depsgraph*)malloc(sizeof(blender::deg::Depsgraph));
dep->frame = 0;
dep->ctime = MAnimControl::currentTime().value() * 0.1;
std::cout<<"ctime:"<<dep->ctime <<std::endl;
ctx->depsgraph = (Depsgraph*)dep;
//auto ob = new Object();
//auto mesh = new Mesh();
Object *ob=NULL;
Mesh *mesh=NULL;
waveModifier_do(md,ctx,ob,mesh, mender_pos, mender_num_verts);

// TODO free md
'''

MAYA_PLUGIN = '''
static const char *k_%s_Name = "%sDeformer";

struct %sDeformer : MPxDeformerNode {
	//static void *creator();
	//static MStatus initialize();
	//enum PostEvaluationEnum=MPxNode::kLeaveDirty;
	MStatus deform(MDataBlock &block,MItGeometry &iterator,const MMatrix &matrix,unsigned int multiIndex){
		std::cout<<"myplugin calling deform"<<std::endl;
		float(*mender_pos)[3] = NULL;
		MStatus result;
		MDataHandle envh = block.inputValue(envelope, &result);
		CHECK_MSTATUS_AND_RETURN_IT(result);
		float env = envh.asFloat();
		//std::vector<std::array<float,3>> maya_verts = std::vector<std::array<float,3>>();
		mender_pos = ( float(*)[3] )MEM_malloc_arrayN(iterator.count(), sizeof(*mender_pos), "mender pos");
		int mender_num_verts = 0;
		for (; !iterator.isDone(); iterator.next()){
			MPoint ip = iterator.position();
			//iterator.setPosition(MPoint(ip.x*env,ip.y*env,ip.z*env,1));
			//maya_verts.push_back({(float)ip.x,(float)ip.y,(float)ip.z});
			mender_pos[mender_num_verts][0]=ip.x;
			mender_pos[mender_num_verts][1]=ip.y;
			mender_pos[mender_num_verts][2]=ip.z;
			mender_num_verts++;
		}
		// call blender func here
		%s
		// set data from blender calc
		iterator.reset();
		auto idx=0;
		for (int idx=0; !iterator.isDone(); iterator.next()){
			MPoint ip = iterator.position();
			float x=mender_pos[idx][0];
			float y=mender_pos[idx][1];
			float z=mender_pos[idx][2];
			iterator.setPosition(MPoint(x,y,z,1));
			idx++;
		}
		MEM_SAFE_FREE(mender_pos);
		return result;
	};
};
'''

# https://download.autodesk.com/us/maya/2011help/API/class_m_global.html#bcf49310dbf61ff6a26be05b5e214137
## TODO why is MGlobal::executeCommand `dgdirty` not working here?
MAYA_PLUGIN_CAPI = '''
extern "C"{
	static void mender_time_changed(void *ptr){
		//std::cout<<"mender time changed"<<std::endl;
		%sDeformer* mod=(%sDeformer*)ptr;
		//mod->dgdirty();
		//MGlobal::executeCommand("dgdirty mender_%s1", true,false);
	}
	static void* %s_creator(){
		void* ptr=(void*)(new %sDeformer());
		MCallbackId callbackId = MEventMessage::addEventCallback("timeChanged", (MMessage::MBasicFunction)mender_time_changed, ptr );
		return ptr;

	}
	static MStatus %s_initialize(){return MStatus();}//%sDefomer.initialize();}
}

'''

MAYA_PLUGIN_IMP = '''
extern "C"{
MStatus initializePlugin(MObject obj) {
	std::cout<<"init of blender maya plugin..."<<std::endl;
	MStatus status;
	MFnPlugin plugin(obj, "rapter", "0.0.1", "Any");
	// Make a deformer node
	status = plugin.registerNode(
		"mender_%s", 
		k_%s_ID, 
		%s_creator,
		%s_initialize, 

		MPxNode::kDeformerNode
	);
	CHECK_MSTATUS_AND_RETURN_IT(status);
	return status;
}
MStatus uninitializePlugin(MObject obj) {
	MFnPlugin plugin(obj);
	MStatus status;
	status =  plugin.deregisterNode(k_%s_ID);
	CHECK_MSTATUS_AND_RETURN_IT(status);
	return status;
}
}
'''

MAYA_PLUGIN_MODS = 0

def gen_maya_plugin(name, glue_code='', ext='.c'):
	global MAYA_PLUGIN_MODS
	MAYA_PLUGIN_MODS += 1

	fname = 'blender/source/blender/modifiers/CMakeLists.txt'
	if os.path.isfile(fname+'.bak'):
		cmake = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		cmake = open(fname,'rb').read().decode('utf-8')
		print('saving back up of modifier CMakeLists')
		open(fname+'.bak','wb').write(cmake.encode('utf-8'))

	mod_c = 'intern/MOD_%s%s' % (name, ext)

	assert cmake.count(mod_c)==1
	print('patching cmake for modifier:',name)

	cmake = cmake.replace(mod_c, 'intern/MOD_mayaplug_%s.cpp' % name)
	inckey = '${ZLIB_INCLUDE_DIRS}'
	assert cmake.count(inckey)==1
	cmake = cmake.replace(inckey, inckey+'\n'+MAYA_INC)

	if '--maya-plugin' in sys.argv:
		libkey = 'bf_blenkernel'
		assert cmake.count(libkey)==1
		cmake = cmake.replace(libkey, libkey+'\n'+LIBOPENMAYA)

	open(fname,'wb').write(cmake.encode('utf-8'))

	plugin = '#ifdef WITH_PYTHON_MODULE'  ## TODO should be MAYA_PLUGIN_XXX
	plugin += MAYA_PLUGIN_HEAD % (name, MAYA_PLUGIN_MODS+10000)
	plugin += MAYA_PLUGIN % tuple( ([name]*3)+[glue_code] )
	plugin += MAYA_PLUGIN_CAPI % tuple([name]*7)
	plugin += MAYA_PLUGIN_IMP % tuple([name]*5)
	plugin += '#endif'
	return plugin

def patch_mod(name, inject='', cb=None):
	print('patching blender modifier:', name)
	ext = '.c'
	fname = 'blender/source/blender/modifiers/intern/MOD_%s.c' % name
	if os.path.isfile(fname):
		pass
	else:
		fname = 'blender/source/blender/modifiers/intern/MOD_%s.cc' % name
		ext = '.cc'

	if os.path.isfile(fname+'.bak'):
		data = open(fname+'.bak','rb').read().decode('utf-8')
	else:
		data = open(fname,'rb').read().decode('utf-8')
		open(fname+'.bak', 'wb').write(data.encode('utf-8'))

	tail = gen_maya_plugin(name, glue_code=inject, ext=ext)
	print(tail)
	assert tail not in data
	print('patching', fname)

	if cb:
		data = cb(data)
	data += tail
	path = fname.replace(ext,'.cpp').replace('/MOD_','/MOD_mayaplug_')
	print('saving:',path)
	open(path,'wb').write(data.encode('utf-8'))


def wave_patch(data):
	bad = 'tex_co = MEM_malloc_arrayN(verts_num, sizeof(*tex_co), "waveModifier_do tex_co");'
	if bad in data:
		good='tex_co = ( float(*)[3] )MEM_malloc_arrayN(verts_num, sizeof(*tex_co), "waveModifier_do tex_co");'
		data=data.replace(bad,good)
	return data

if MAYA_INSTALLED:
	patch_mod('wave', inject=MAYA2BLENDER_WAVE, cb=wave_patch)


build = 'buildbpy'
if '--qt' in sys.argv:
	build = 'buildblenderqt'
elif '--maya-plugin' in sys.argv:
	build = 'buildmaya'
elif '--wasm' in sys.argv:
	build = 'buildwbpy'
elif '--32bits' in sys.argv:
	build = 'buildbpy32'
elif '--headless' in sys.argv:
	build = 'buildheadless'

if not os.path.isdir(build): os.mkdir(build)
print(cmd)
subprocess.check_call(cmd, cwd=build)

if '--wasm' in sys.argv:
	#ensure_blender_wasm_dna()
	#ensure_blender_wasm_rna(). ## make deletes these each run
	try:
		subprocess.check_call(['make'], cwd=build)
	except:
		print('when build fails at 9% then run `./makebpy --wasm-dna`')
		print('when build fails at 20% then run `./makebpy --wasm-rna`')
		sys.exit()
else:
	subprocess.check_call(['make'], cwd=build)

#if '--qt' not in sys.argv:
if '--wasm' in sys.argv or '--32bits' in sys.argv or '--headless' in sys.argv:
	pass
else:
	subprocess.check_call(['make', 'install'], cwd=build)

#bpy = os.path.join(build, 'bin/bpy/__init__.so')
#assert os.path.isfile(bpy)
#open('bpy.so', 'wb').write(open(bpy,'rb').read())
if '--qt' in sys.argv:
	print("build blenderQT OK")
	if sys.platform=='linux':
		bexe = 'blender'
		bdir = './buildblenderqt/bin'
		os.system('cp -v qtmypaint/demo/libqtmypaint.1.so ./buildblenderqt/bin/.')
	else:
		bexe = 'Blender.app/Contents/MacOS/Blender'
		#bexe = './Blender'
		bdir = './buildblenderqt/bin/Blender.app/Contents/MacOS'
		os.system('cp -v qtmypaint/demo/libqtmypaint.1.dylib ./buildblenderqt/bin/Blender.app/Contents/MacOS/.')

	bpath = os.path.join('./buildblenderqt/bin', bexe)
	if sys.platform=='linux':
		#os.system('cp -Rv %s/*.* ./buildblenderqt/bin/.' % OPENTOONZ_PATCHED)
		os.system('ldd %s | grep "not found"' %bpath)
		for missing in 'libimage.so libtnztools.so libOpenToonz.so'.split():
			rep = os.path.abspath( os.path.join(OPENTOONZ_PATCHED, missing) )
			assert os.path.isfile(rep)
			cmd = [
				'patchelf', 
				#'--debug', 
				'--replace-needed', missing, rep, 
				bpath
			]
			print(cmd)
			subprocess.check_call(cmd)
			
		assert os.path.isfile(QTMYPAINT_LIB)
		cmd = ['patchelf']
		if '--debug' in sys.argv:
			cmd.append('--debug')

		cmd.extend([
			'--replace-needed', 
			'libqtmypaint.so.1', 
			os.path.abspath(QTMYPAINT_LIB),
			bpath,
		])

		print(cmd)
		subprocess.check_call(cmd)

		os.system('ldd %s | grep "not found"' %bpath)
	else:
		patch_blender_exe( bpath )

	test_file_simple = os.path.expanduser('~/Desktop/test-blenderQT.blend')
	test_file = ''
	if '--test-load-blend' in sys.argv:
		## TODO this crashes blender because something is getting loaded from a thread
		## TODO disable multi threaded rendering from material preview
		if os.path.isfile(test_file_simple):
			test_file = test_file_simple

	if '--test' in sys.argv:
		cmd = []
		if '--lldb' in sys.argv:
			cmd.append('lldb')
			cmd.append('--')
		#	os.system('lldb ./buildblenderqt/bin/%s %s' % (bexe, test_file))
		#else:
		#	os.system('./buildblenderqt/bin/%s %s' %(bexe, test_file))

		#cmd.append('./buildblenderqt/bin/'+bexe)
		#cmd.append(bexe)
		cmd.append(bpath)

		if test_file:
			cmd.append(test_file)

		print(cmd)
		## note: for now run from the build install folder
		## because libqtmypaint.1.so/dylib is also there
		#subprocess.check_call(cmd, cwd= os.path.abspath(bdir) )
		## but this will not fully work either, because then bpy.so is not found
		## on osx the exe needs to be patched to find libqtmypaint..
		subprocess.check_call(cmd)

elif '--maya-plugin' in sys.argv:
	bpy = './lib/darwin/python/lib/python3.10/site-packages/bpy'
	assert os.path.isdir(bpy)
	print('build bpy.so maya plugin OK')
	print(bpy)
	os.system('cp -Rv %s ./bpy_maya_plugin' % bpy)
elif '--wasm' in sys.argv or '--headless' in sys.argv:
	pass
else:
	bpy = './lib/darwin/python/lib/python3.10/site-packages/bpy'
	assert os.path.isdir(bpy)
	print('build bpy.so OK')
	print(bpy)
	os.system('cp -Rv %s .' % bpy)



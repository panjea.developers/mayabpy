/* SPDX-License-Identifier: GPL-2.0-or-later
 * Copyright 2001-2002 NaN Holding BV. All rights reserved. */

/** \file
 * \ingroup creator
 */
#include <thread>
#include <stdlib.h>
#include <string.h>

#ifdef WIN32
#  include "utfconv.h"
#  include <windows.h>
#endif

#if defined(WITH_TBB_MALLOC) && defined(_MSC_VER) && defined(NDEBUG)
#  pragma comment(lib, "tbbmalloc_proxy.lib")
#  pragma comment(linker, "/include:__TBB_malloc_proxy")
#endif

#include "MEM_guardedalloc.h"

#include "CLG_log.h"

#include "DNA_genfile.h"

#include "BLI_string.h"
#include "BLI_system.h"
#include "BLI_task.h"
#include "BLI_threads.h"
#include "BLI_utildefines.h"

/* Mostly initialization functions. */
#include "BKE_appdir.h"
#include "BKE_blender.h"
#include "BKE_brush.h"
#include "BKE_cachefile.h"
#include "BKE_callbacks.h"
#include "BKE_context.h"
#include "BKE_global.h"
#include "BKE_gpencil_modifier.h"
#include "BKE_idtype.h"
#include "BKE_main.h"
#include "BKE_material.h"
#include "BKE_modifier.h"
#include "BKE_node.h"
#include "BKE_particle.h"
#include "BKE_shader_fx.h"
#include "BKE_sound.h"
#include "BKE_vfont.h"
#include "BKE_volume.h"

#ifndef WITH_PYTHON_MODULE
#  include "BLI_args.h"
#endif

#include "DEG_depsgraph.h"

#include "IMB_imbuf.h" /* For #IMB_init. */

#include "RE_engine.h"
#include "RE_texture.h"

#include "ED_datafiles.h"

#include "WM_api.h"
#include "WM_toolsystem.h"

//#include "WM_message.h"
//#include "WM_types.h"
//#include "wm.h"
#include "wm_draw.h"
#include "wm_event_system.h"
#include "wm_window.h"
//#ifdef WITH_XR_OPENXR
//#  include "wm_xr.h"
//#endif

#include "RNA_define.h"

#ifdef WITH_FREESTYLE
#  include "FRS_freestyle.h"
#endif

#include <signal.h>

#ifdef __FreeBSD__
#  include <floatingpoint.h>
#endif

#ifdef WITH_BINRELOC
#  include "binreloc.h"
#endif

#ifdef WITH_LIBMV
#  include "libmv-capi.h"
#endif

#ifdef WITH_CYCLES_LOGGING
#  include "CCL_api.h"
#endif

#ifdef WITH_SDL_DYNLOAD
#  include "sdlew.h"
#endif

#include "creator_intern.h" /* Own include. */


#ifdef WITH_QT
#include <QtGui/QSurfaceFormat>
//#include <QtWidgets/QApplication>
#include <QtGui/QWindow>
//#include <QtGui/QGuiApplication>
#include <QtWidgets/QApplication>
#include <QtGui/QOpenGLPaintDevice>
#include <QtGui/QPainter>

//#include "../../intern/ghost/intern/GHOST_QT.h"  breaks epoxy include

//QGuiApplication* MenderApplication=nullptr;
QApplication* MenderApplication=nullptr;  // opentoonz compatible - extern linked
extern QApplication* ToonzApplication;
extern "C" void opentoonz_update();
extern "C" int opentoonz_main(int mode);
extern "C" int qtmypaint_main(int mode);
extern "C" void qtmypaint_update();

typedef void(*TNZ_CALLBACK)(std::vector<float>);
extern void opentoonz_set_stroke_callback(TNZ_CALLBACK ptr);

typedef void(*MYPAINT_CALLBACK)(std::string);
extern void qtmypaint_set_callback(MYPAINT_CALLBACK ptr);


#include "BPY_extern_run.h"
#include "mender.h"
static bContext* MENDER_CTX = nullptr;

void tnz_stroke_cb(std::vector<float> stroke) {
	std::cout<<"stroke-cb" << std::endl;
	for( auto val : stroke ) {
		std::cout<<"stroke-val="<<val << std::endl;
	}
	if (MENDER_CTX==nullptr)
		bool ok = BPY_run_string_exec(nullptr, nullptr, "print('hey python')");
	else
		bool ok = BPY_run_string_exec(MENDER_CTX, nullptr, test_gen_py_update(stroke).c_str() );

}

static std::string run_in_blender_thread = "";

void mypaint_stroke_cb(std::string info) {
	std::cout<<"mypaint stroke-cb:" << info << std::endl;
	// because this creates an offscreen gl context from the mypaint thread,
	// delay and call later
	//if (MENDER_CTX)
	//	bool ok = BPY_run_string_exec(MENDER_CTX, nullptr, test_gen_py_update_mypaint(info).c_str() );
	run_in_blender_thread=test_gen_py_update_mypaint(info);

}

// note: blender's glcontext must be made current before calling this
// TODO: remove temp workaround in window get state (this forces the glcontext to be current)
static void mender_update(bContext* C){
	MENDER_CTX=C;
	#ifdef WITH_QT_DEBUG
		std::cout<<"main wm_window_process_events" << std::endl;
	#endif
	/* Get events from ghost, handle window events, add to window queues. */
	wm_window_process_events(C);

	#ifdef WITH_QT_DEBUG
		std::cout<<"main wm_event_do_handlers" << std::endl;
	#endif

		/* Per window, all events to the window, screen, area and region handlers. */
		wm_event_do_handlers(C);

	#ifdef WITH_QT_DEBUG
		std::cout<<"main wm_event_do_notifiers" << std::endl;
	#endif

	/* Events have left notes about changes, we handle and cache it. */
	wm_event_do_notifiers(C);

	#ifdef WITH_QT_DEBUG
		std::cout<<"main wm_draw_update" << std::endl;
	#endif

	/* Execute cached changes draw. */
	wm_draw_update(C);

}


struct OpenGLWindow : QWindow {
	//Q_OBJECT
	//public:
	OpenGLWindow(QWindow *parent=nullptr) : QWindow(parent){
		std::cout<<"qt opengl window"<<std::endl;
		setSurfaceType(QWindow::OpenGLSurface);
	}
	~OpenGLWindow() {
		std::cout<<"qt opengl window"<<std::endl;
	}

	void render(QPainter *painter){
		std::cout<<"qt opengl render QPainter"<<std::endl;
	}
	void render(){
		std::cout<<"qt opengl render"<<std::endl;
		if (!m_device) m_device = new QOpenGLPaintDevice;
	//    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		m_device->setSize(size() * devicePixelRatio());
		m_device->setDevicePixelRatio(devicePixelRatio());
		QPainter painter(m_device);
		render(&painter);

	}
	void initialize() {}
	void setAnimating(bool animating){
		m_animating = animating;
		if (animating) renderLater();
	}
	//public slots:
	void renderLater(){
		requestUpdate();
	}
	void renderNow(){
		if (!isExposed())
		    return;
		std::cout<<"qt opengl renderNow"<<std::endl;

		bool needsInitialize = false;

		if (!m_context) {
			std::cout<<"qt opengl renderNow new render context"<<std::endl;
		    m_context = new QOpenGLContext(this);
		    m_context->setFormat(requestedFormat());
		    m_context->create();
			std::cout<<"qt opengl renderNow"<< m_context <<std::endl;
		    needsInitialize = true;
		}

		m_context->makeCurrent(this);

		if (needsInitialize) {
		    //initializeOpenGLFunctions();
		    initialize();
		}

		render();

		m_context->swapBuffers(this);

		if (m_animating)
		    renderLater();

	}

	//protected:
	bool event(QEvent *event) override {
		switch (event->type()) {
			case QEvent::UpdateRequest:
				renderNow();
				return true;
			default:
				return QWindow::event(event);
			}
	}

	void exposeEvent(QExposeEvent *event) override{
		if (isExposed()) renderNow();
	}

	//private:
	bool m_animating = false;

	QOpenGLContext *m_context = nullptr;
	QOpenGLPaintDevice *m_device = nullptr;
};
/*
	OpenGLWindow::OpenGLWindow(QWindow *parent) : QWindow(parent){
		setSurfaceType(QWindow::OpenGLSurface);
	}
*/



static void test_qt_opengl(QApplication *app){
	OpenGLWindow* win= new OpenGLWindow();
	win->resize(640,480);
	win->show();
	win->setAnimating(true);
	opentoonz_main(-1);
	app->exec();
}

#endif

/* -------------------------------------------------------------------- */
/** \name Local Defines
 * \{ */

/* When building as a Python module, don't use special argument handling
 * so the module loading logic can control the `argv` & `argc`. */
#if defined(WIN32) && !defined(WITH_PYTHON_MODULE)
#  define USE_WIN32_UNICODE_ARGS
#endif

/** \} */

/* -------------------------------------------------------------------- */
/** \name Local Application State
 * \{ */

/* written to by 'creator_args.c' */
struct ApplicationState app_state = {
		.signal =
				{
						.use_crash_handler = true,
						.use_abort_handler = true,
				},
		.exit_code_on_error =
				{
						.python = 0,
				},
};

/** \} */

/* -------------------------------------------------------------------- */
/** \name Application Level Callbacks
 *
 * Initialize callbacks for the modules that need them.
 * \{ */

static void callback_mem_error(const char *errorStr)
{
	fputs(errorStr, stderr);
	fflush(stderr);
}

static void main_callback_setup(void)
{
	/* Error output from the guarded allocation routines. */
	MEM_set_error_callback(callback_mem_error);
}

/* free data on early exit (if Python calls 'sys.exit()' while parsing args for eg). */
struct CreatorAtExitData {
#ifndef WITH_PYTHON_MODULE
	bArgs *ba;
#endif

#ifdef USE_WIN32_UNICODE_ARGS
	const char **argv;
	int argv_num;
#endif

#if defined(WITH_PYTHON_MODULE) && !defined(USE_WIN32_UNICODE_ARGS)
	void *_empty; /* Prevent empty struct error with MSVC. */
#endif
};

static void callback_main_atexit(void *user_data)
{
	struct CreatorAtExitData *app_init_data = (CreatorAtExitData*)user_data;

#ifndef WITH_PYTHON_MODULE
	if (app_init_data->ba) {
		BLI_args_destroy(app_init_data->ba);
		app_init_data->ba = NULL;
	}
#else
	UNUSED_VARS(app_init_data); /* May be unused. */
#endif

#ifdef USE_WIN32_UNICODE_ARGS
	if (app_init_data->argv) {
		while (app_init_data->argv_num) {
			free((void *)app_init_data->argv[--app_init_data->argv_num]);
		}
		free((void *)app_init_data->argv);
		app_init_data->argv = NULL;
	}
#else
	UNUSED_VARS(app_init_data); /* May be unused. */
#endif
}

static void callback_clg_fatal(void *fp)
{
	BLI_system_backtrace((FILE*)fp);
}

/** \} */

/* -------------------------------------------------------------------- */
/** \name Blender as a Stand-Alone Python Module (bpy)
 *
 * While not officially supported, this can be useful for Python developers.
 * See: https://wiki.blender.org/wiki/Building_Blender/Other/BlenderAsPyModule
 * \{ */

#ifdef WITH_PYTHON_MODULE

/* Called in `bpy_interface.c` when building as a Python module. */
extern "C" int main_python_enter(int argc, const char **argv);
extern "C" void main_python_exit(void);

/* Rename the 'main' function, allowing Python initialization to call it. */
#  define main main_python_enter
static void *evil_C = NULL;

#  ifdef __APPLE__
/* Environment is not available in macOS shared libraries. */
#    include <crt_externs.h>
char **environ = NULL;
#  endif /* __APPLE__ */

#endif /* WITH_PYTHON_MODULE */

/** \} */

/* -------------------------------------------------------------------- */
/** \name GMP Allocator Workaround
 * \{ */

#if (defined(WITH_TBB_MALLOC) && defined(_MSC_VER) && defined(NDEBUG) && defined(WITH_GMP)) || \
		defined(DOXYGEN)
#  include "gmp.h"
#  include "tbb/scalable_allocator.h"

void *gmp_alloc(size_t size)
{
	return scalable_malloc(size);
}
void *gmp_realloc(void *ptr, size_t old_size, size_t new_size)
{
	return scalable_realloc(ptr, new_size);
}

void gmp_free(void *ptr, size_t size)
{
	scalable_free(ptr);
}
/**
 * Use TBB's scalable_allocator on Windows.
 * `TBBmalloc` correctly captures all allocations already,
 * however, GMP is built with MINGW since it doesn't build with MSVC,
 * which TBB has issues hooking into automatically.
 */
void gmp_blender_init_allocator()
{
	mp_set_memory_functions(gmp_alloc, gmp_realloc, gmp_free);
}
#endif

/** \} */

/* -------------------------------------------------------------------- */
/** \name Main Function
 * \{ */

/**
 * Blender's main function responsibilities are:
 * - setup subsystems.
 * - handle arguments.
 * - run #WM_main() event loop,
 *   or exit immediately when running in background-mode.
 */
int main(int argc,
#ifdef USE_WIN32_UNICODE_ARGS
				 const char **UNUSED(argv_c)
#else
				 const char **argv
#endif
)
{
	bContext *C;

#ifndef WITH_PYTHON_MODULE
	bArgs *ba;
#endif

#ifdef USE_WIN32_UNICODE_ARGS
	char **argv;
	int argv_num;
#endif

	/* --- end declarations --- */
// https://doc.qt.io/qt-6/qopenglwidget.html
#ifdef WITH_PYTHON_MODULE
	std::cout<<"enter main - init bpy..." << std::endl;

#elif defined(WITH_QT)
	QSurfaceFormat format;
	format.setProfile(QSurfaceFormat::CoreProfile);
	format.setVersion(3, 3);
	//format.setVersion(3, 2);
	//format.setProfile(QSurfaceFormat::CompatibilityProfile);
	format.setDepthBufferSize(24);
	format.setStencilBufferSize(8);
	format.setRedBufferSize(8);
	format.setGreenBufferSize(8);
	format.setBlueBufferSize(8);
	QSurfaceFormat::setDefaultFormat(format);
	//QGuiApplication* qtapp = new QGuiApplication(argc, (char**)argv);
	QApplication* qtapp = new QApplication(argc, (char**)argv);
	#ifdef WITH_QT_TESTS
		test_qt_opengl(qtapp);
	#endif
		std::cout<<"QApplication init OK" << std::endl;

	//OpenGLWindow* win= new OpenGLWindow();
	//QtBlenderWindow* win= new QtBlenderWindow();
	//win->resize(640,480);
	//win->show();
	//win->setAnimating(true);
	//qtapp.processEvents();
	//MenderGlobals->window=win;
	// set extern global MenderApplication so GHOST_QT can use it
	MenderApplication = qtapp;

// too soon?
//	std::cout<<"call extern opentoonz_main..." << std::endl;
//	opentoonz_main(-1);
//	std::cout<<"call extern opentoonz_main OK" << std::endl;


#endif

	/* Ensure we free data on early-exit. */
	struct CreatorAtExitData app_init_data = {NULL};
	BKE_blender_atexit_register(callback_main_atexit, &app_init_data);

	/* Un-buffered `stdout` makes `stdout` and `stderr` better synchronized, and helps
	 * when stepping through code in a debugger (prints are immediately
	 * visible). However disabling buffering causes lock contention on windows
	 * see T76767 for details, since this is a debugging aid, we do not enable
	 * the un-buffered behavior for release builds. */
#ifndef NDEBUG
	setvbuf(stdout, NULL, _IONBF, 0);
#endif

#ifdef WIN32
	/* We delay loading of OPENMP so we can set the policy here. */
#  if defined(_MSC_VER)
	_putenv_s("OMP_WAIT_POLICY", "PASSIVE");
#  endif

#  ifdef USE_WIN32_UNICODE_ARGS
	/* Win32 Unicode Arguments. */
	{
		/* NOTE: Can't use `guardedalloc` allocation here, as it's not yet initialized
		 * (it depends on the arguments passed in, which is what we're getting here!) */
		wchar_t **argv_16 = CommandLineToArgvW(GetCommandLineW(), &argc);
		argv = malloc(argc * sizeof(char *));
		for (argv_num = 0; argv_num < argc; argv_num++) {
			argv[argv_num] = alloc_utf_8_from_16(argv_16[argv_num], 0);
		}
		LocalFree(argv_16);

		/* free on early-exit */
		app_init_data.argv = argv;
		app_init_data.argv_num = argv_num;
	}
#  endif /* USE_WIN32_UNICODE_ARGS */
#endif   /* WIN32 */

	/* NOTE: Special exception for guarded allocator type switch:
	 *       we need to perform switch from lock-free to fully
	 *       guarded allocator before any allocation happened.
	 */
	{
		int i;
		for (i = 0; i < argc; i++) {
			if (STR_ELEM(argv[i], "-d", "--debug", "--debug-memory", "--debug-all")) {
				printf("Switching to fully guarded memory allocator.\n");
				MEM_use_guarded_allocator();
				break;
			}
			if (STREQ(argv[i], "--")) {
				break;
			}
		}
		MEM_init_memleak_detection();
	}

#ifdef BUILD_DATE
	{
		time_t temp_time = build_commit_timestamp;
		struct tm *tm = gmtime(&temp_time);
		if (LIKELY(tm)) {
			strftime(build_commit_date, sizeof(build_commit_date), "%Y-%m-%d", tm);
			strftime(build_commit_time, sizeof(build_commit_time), "%H:%M", tm);
		}
		else {
			const char *unknown = "date-unknown";
			BLI_strncpy(build_commit_date, unknown, sizeof(build_commit_date));
			BLI_strncpy(build_commit_time, unknown, sizeof(build_commit_time));
		}
	}
#endif

#ifdef WITH_SDL_DYNLOAD
	sdlewInit();
#endif

	/* Initialize logging. */
	CLG_init();
	CLG_fatal_fn_set(callback_clg_fatal);

	C = CTX_create();

#ifdef WITH_PYTHON_MODULE
#  ifdef __APPLE__
	environ = *_NSGetEnviron();
#  endif

#  undef main
	evil_C = C;
#endif

#ifdef WITH_BINRELOC
	br_init(NULL);
#endif

#ifdef WITH_LIBMV
	libmv_initLogging(argv[0]);
#elif defined(WITH_CYCLES_LOGGING)
	CCL_init_logging(argv[0]);
#endif

#if defined(WITH_TBB_MALLOC) && defined(_MSC_VER) && defined(NDEBUG) && defined(WITH_GMP)
	gmp_blender_init_allocator();
#endif

	std::cout<<"blender: main_callback_setup ..." << std::endl;
	main_callback_setup();
	std::cout<<"blender: main_callback_setup OK" << std::endl;

//#if defined(__APPLE__) && !defined(WITH_PYTHON_MODULE) && !defined(WITH_HEADLESS)
#if 0
	/* Patch to ignore argument finder gives us (PID?) */
	if (argc == 2 && STRPREFIX(argv[1], "-psn_")) {
		extern int GHOST_HACK_getFirstFile(char buf[]);
		static char firstfilebuf[512];

		argc = 1;

		if (GHOST_HACK_getFirstFile(firstfilebuf)) {
			argc = 2;
			argv[1] = firstfilebuf;
		}
	}
#endif

#ifdef __FreeBSD__
	fpsetmask(0);
#endif

	/* Initialize path to executable. */
	std::cout<<"blender: BKE_appdir_program_path_init..." << std::endl;
	BKE_appdir_program_path_init(argv[0]);
	std::cout<<"blender: BLI_threadapi_init ..." << std::endl;
	BLI_threadapi_init();
	std::cout<<"blender: DNA_sdna_current_init ..." << std::endl;
	DNA_sdna_current_init();
	std::cout<<"blender: BKE_blender_globals_init ..." << std::endl;
	BKE_blender_globals_init(); /* blender.c */

	BKE_idtype_init();
	BKE_cachefiles_init();
	BKE_modifier_init();
	BKE_gpencil_modifier_init();
	BKE_shaderfx_init();
	BKE_volumes_init();
	DEG_register_node_types();

	BKE_brush_system_init();
	RE_texture_rng_init();

	BKE_callback_global_init();

	/* First test for background-mode (#Global.background) */
#ifndef WITH_PYTHON_MODULE
	ba = BLI_args_create(argc, (const char **)argv); /* skip binary path */

	/* Ensure we free on early exit. */
	app_init_data.ba = ba;

	main_args_setup(C, ba);

	/* Begin argument parsing, ignore leaks so arguments that call #exit
	 * (such as '--version' & '--help') don't report leaks. */
	MEM_use_memleak_detection(false);
	std::cout<<"blender: BLI_args_parse ..." << std::endl;

	/* Parse environment handling arguments. */
	BLI_args_parse(ba, ARG_PASS_ENVIRONMENT, NULL, NULL);
	std::cout<<"blender: BLI_args_parse OK" << std::endl;

#else
	/* Using preferences or user startup makes no sense for #WITH_PYTHON_MODULE. */
	G.factory_startup = true;
#endif

	/* After parsing #ARG_PASS_ENVIRONMENT such as `--env-*`,
	 * since they impact `BKE_appdir` behavior. */
	BKE_appdir_init();

	/* After parsing number of threads argument. */
	BLI_task_scheduler_init();

	/* Initialize sub-systems that use `BKE_appdir.h`. */
	std::cout<<"blender: sub systems init ..." << std::endl;
	IMB_init();

#ifndef WITH_PYTHON_MODULE
	/* First test for background-mode (#Global.background) */
	BLI_args_parse(ba, ARG_PASS_SETTINGS, NULL, NULL);

	main_signal_setup();
#endif

#ifdef WITH_FFMPEG
	/* Keep after #ARG_PASS_SETTINGS since debug flags are checked. */
	IMB_ffmpeg_init();
#endif

	/* After #ARG_PASS_SETTINGS arguments, this is so #WM_main_playanim skips #RNA_init. */
	std::cout<<"blender: RNA_init ..." << std::endl;

	RNA_init();

	RE_engines_init();
	BKE_node_system_init();
	BKE_particle_init_rng();
	/* End second initialization. */

#if defined(WITH_PYTHON_MODULE) || defined(WITH_HEADLESS)
	/* Python module mode ALWAYS runs in background-mode (for now). */
	G.background = true;
#else
	if (G.background) {
		main_signal_setup_background();
	}
#endif

	/* Background render uses this font too. */
	BKE_vfont_builtin_register(datatoc_bfont_pfb, datatoc_bfont_pfb_size);

	/* Initialize FFMPEG if built in, also needed for background-mode if videos are
	 * rendered via FFMPEG. */
	BKE_sound_init_once();
	std::cout<<"blender: BKE_materials_init ..." << std::endl;

	BKE_materials_init();

#ifndef WITH_PYTHON_MODULE
	if (G.background == 0) {
		BLI_args_parse(ba, ARG_PASS_SETTINGS_GUI, NULL, NULL);
	}
	BLI_args_parse(ba, ARG_PASS_SETTINGS_FORCE, NULL, NULL);
#endif

#ifdef WITH_QT_WM
	std::cout<<"mender: WM_init TODO" << std::endl;
#else
	std::cout<<"blender: WM_init ..." << std::endl;
	WM_init(C, argc, (const char **)argv);
	std::cout<<"blender: WM_init OK" << std::endl;

	/* Need to be after WM init so that userpref are loaded. */
	RE_engines_init_experimental();
	std::cout<<"blender: RE_engines_init_experimental OK" << std::endl;
#endif

#ifndef WITH_PYTHON
	printf(
			"\n* WARNING * - Blender compiled without Python!\n"
			"this is not intended for typical usage\n\n");
#endif
	std::cout<<"blender: CTX_py_init_set ..." << std::endl;

	CTX_py_init_set(C, true);
	WM_keyconfig_init(C);

#ifdef WITH_FREESTYLE
	/* Initialize Freestyle. */
	FRS_init();
	FRS_set_context(C);
#endif

	/* OK we are ready for it */
#ifndef WITH_PYTHON_MODULE
	/* Handles #ARG_PASS_FINAL. */
	std::cout<<"blender: main_args_setup_post ..." << std::endl;
	main_args_setup_post(C, ba);
	std::cout<<"blender: main_args_setup_post OK" << std::endl;

#endif

	/* Explicitly free data allocated for argument parsing:
	 * - 'ba'
	 * - 'argv' on WIN32.
	 */
	callback_main_atexit(&app_init_data);
	BKE_blender_atexit_unregister(callback_main_atexit, &app_init_data);

	/* End argument parsing, allow memory leaks to be printed. */
	MEM_use_memleak_detection(true);

	/* Paranoid, avoid accidental re-use. */
#ifndef WITH_PYTHON_MODULE
	ba = NULL;
	(void)ba;
#endif

#ifdef USE_WIN32_UNICODE_ARGS
	argv = NULL;
	(void)argv;
#endif

	std::cout<<"blender: main loop ..." << std::endl;

#ifdef WITH_PYTHON_MODULE
	std::cout<<"bpy module is ready" << std::endl;

#elif defined(WITH_QT)

	std::cout<<"pre main..." << std::endl;

	wm_event_do_refresh_wm_and_depsgraph(C);
	std::cout<<"main..." << std::endl;
	mender_update(C);

	//std::thread toonzthread([]{
		std::cout<<"call extern opentoonz_main..." << std::endl;
		opentoonz_main(-1);
		std::cout<<"call extern opentoonz_main OK" << std::endl;
	//});
		std::cout<<"ToonzApplication pointer="<< ToonzApplication << std::endl;

	std::cout<<"main... post opentoonz update..." << std::endl;
	wm_event_do_refresh_wm_and_depsgraph(C);
	mender_update(C);
	std::cout<<"main... post opentoonz update OK" << std::endl;

	opentoonz_set_stroke_callback( tnz_stroke_cb );
	std::cout<<"main... post opentoonz set stroke callback OK" << std::endl;


	std::cout<<"main... starting mypaint" << std::endl;
	qtmypaint_main(-1);
	std::cout<<"main... set mypaint callback" << std::endl;
	qtmypaint_set_callback( mypaint_stroke_cb );

	std::cout<<"enter main loop..." << std::endl;

	while (true){
		#ifdef WITH_QT_DEBUG_MAIN_LOOP
			std::cout<<"main... update" << std::endl;
		#endif
		mender_update(C);
		if (run_in_blender_thread != std::string("")){
			bool ok = BPY_run_string_exec(C, nullptr, run_in_blender_thread.c_str() );
			run_in_blender_thread = std::string("");
			mender_update(C);
		}

		//qtapp->processEvents();
		opentoonz_update();
		qtmypaint_update();
	}

	//return qtapp->exec();
	//WM_main(C);


#else
	if (G.background) {
		/* Using window-manager API in background-mode is a bit odd, but works fine. */
		WM_exit(C);
	}
	else {
		/* When no file is loaded, show the splash screen. */
		const char *blendfile_path = BKE_main_blendfile_path_from_global();
		if (blendfile_path[0] == '\0') {
			WM_init_splash(C);
		}
		WM_main(C);
	}
#endif /* WITH_PYTHON_MODULE */

	return 0;
} /* End of `int main(...)` function. */

#ifdef WITH_PYTHON_MODULE
void main_python_exit(void)
{
	WM_exit_ex((bContext *)evil_C, true);
	evil_C = NULL;
}
#endif

/** \} */

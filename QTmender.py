import maya
import sys
__maya_term__ = sys.stdout 
__terminal__ = sys.__stdout__  # maya replaces sys.stdout
def print(*args):
	for a in args:
		__terminal__.write('%s\n'%a)
def maya_print(*args):
	for a in args:
		__maya_term__.write('%s\n'%a)

import maya.OpenMayaUI as omui
from PySide2 import QtCore, QtWidgets
from shiboken2 import wrapInstance

MWIN=None
def mayaWindow():
	global MWIN
	main_window_ptr = omui.MQtUtil.mainWindow()  ## swig
	MWIN=wrapInstance(int(main_window_ptr),QtWidgets.QWidget)
	return MWIN

###############


'''
Template class for docking a Qt widget to maya 2017+.
Author: Lior ben horin
12-1-2017
'''
import weakref
from maya.app.general.mayaMixin import *
class BroMainWindow_Dockable(MayaQWidgetDockableMixin, QMainWindow):
	DOCK_LABEL_NAME = 'no name window' # Window display name
	CONTROL_NAME = 'no_name_window' # Window unique object name
	instances = list()

	def __init__(self):
		super(BroMainWindow_Dockable, self).__init__()
		self.delete_instances()
		self.__class__.instances.append(weakref.proxy(self))
		# Not sure, but I suppose that we better keep track of instances of our window and keep Maya environment clean.
		# So we'll remove all instances before creating a new one.
		if maya.cmds.window(self.CONTROL_NAME + "WorkspaceControl", ex=True):
			print("Removing", self.CONTROL_NAME + "WorkspaceControl")
			maya.cmds.deleteUI(self.CONTROL_NAME + "WorkspaceControl")
			print("Removed", self.CONTROL_NAME + "WorkspaceControl")

		self.setAttribute(Qt.WA_DeleteOnClose, True)
		# Set object name and window title
		self.setObjectName(self.CONTROL_NAME)
		self.setWindowTitle(self.DOCK_LABEL_NAME)
		self.central_widget = QWidget()
		self.setCentralWidget(self.central_widget)
		self.main_layout = QVBoxLayout()
		self.central_widget.setLayout(self.main_layout)
		self.build_ui()

	@staticmethod
	def delete_instances():
		for ins in BroMainWindow_Dockable.instances:
			try:
				print('Delete {}'.format(ins))
			except:
				print('Window reference seems to be removed already, ignore.')
			try:
				ins.setParent(None)
				ins.deleteLater()
			except:
				# ignore the fact that the actual parent has already been deleted by Maya...
				pass
			try:
				BroMainWindow_Dockable.instances.remove(ins)
				del ins
			except:
				# Supress error
				pass

	def build_ui(self):
		"""
		This function is called at the end of window initialization and creates your actual UI.
		Override it with your UI.
		"""
		pass

def dock_start():
	global MDOCK
	MDOCK = MenderDock().show(dockable=True)

class MenderDock(BroMainWindow_Dockable):
	DOCK_LABEL_NAME = 'mender'  # Window display name
	instances = list()
	CONTROL_NAME = 'mender_widget'  # Window unique object name
	def __init__(self):
		super(MenderDock, self).__init__()
		self.resize(240,180)
		self.timer = QtCore.QTimer(self)
		self.timer.timeout.connect(self.loop)
		self.timer.start(10)
		self.modifier = None

	def loop(self):
		if self.modifier:
			#print('force dirty')
			maya.cmds.dgdirty('mender_wave1')

	def build_ui(self):
		btn=QtWidgets.QPushButton('add wave deformer')
		self.main_layout.addWidget(btn)
		btn.clicked.connect(self.add_wave_mod)

		self.statusBar = QStatusBar()
		self.statusBar.showMessage("mender c++ plugin")
		self.setStatusBar(self.statusBar)
		self.statusBar.setObjectName("statusBar")
		self.setStyleSheet("#statusBar {background-color:#faa300;color:#fff}")

	def add_wave_mod(self):
		print('add_wave_mod: waveDeformer')
		self.modifier=True
		return maya.cmds.deformer(type='mender_wave')

###############

def clearLayout(layout):
	while layout.count():
		child = layout.takeAt(0)
		if child.widget():
			child.widget().deleteLater()

def getsel():
	return maya.cmds.ls(sl=True,long=True) or []

def mender_start():
	print('PLUGIN DEBUG')
	print(maya.cmds.pluginInfo(query=True, listPluginsPath=True))
	print('all plugins:',maya.cmds.pluginInfo(query=True, listPlugins=True))
	print('--MENDER--')
	print('vendor:', maya.cmds.pluginInfo('mender', query=True, vendor=True))
	print('version:', maya.cmds.pluginInfo('mender', query=True, version=True))
	print('data:', maya.cmds.pluginInfo('mender', query=True, data=True))
	print('nodes:', maya.cmds.pluginInfo('mender', query=True, dependNode=True))
	print('nodes ids:', maya.cmds.pluginInfo('mender', query=True, dependNodeId=True))
	print('starting mender dock')
	dock_start()

mender_start()

QApplication* ToonzApplication = nullptr;  // extern used by blender

static MainWindow* s_main_win = nullptr;

extern "C" void opentoonz_update() {
	ToonzApplication->processEvents();
}

// called by mender.cpp main
// can also be called from python ctypes
extern "C" int opentoonz_main(int mode) {
	// Install signal handlers to catch crashes
	//CrashHandler::install();
	int argc = 1;
	char *argv[]={"/tmp/OpenToonz"};

	QSurfaceFormat format;
	if (mode<=-2) {
		qDebug() << "libtoonz using opengl 3.3 - CoreProfile";
		format.setVersion(3, 3);
		format.setProfile(QSurfaceFormat::CoreProfile);  // breaks opentoonz gl rendering
	} else if (mode==-1) {
		qDebug() << "libtoonz using opengl 3.3 - CompatibilityProfile";
		format.setVersion(3, 3);
		format.setProfile(QSurfaceFormat::CompatibilityProfile);
	} else {
		qDebug() << "libtoonz using opengl - default version";
		//format.setVersion(2, 2);
	}
	format.setDepthBufferSize(24);
	format.setStencilBufferSize(8);
	format.setRedBufferSize(8);
	format.setGreenBufferSize(8);
	format.setBlueBufferSize(8);
	QSurfaceFormat::setDefaultFormat(format);


	// parsing arguments and qualifiers
	TFilePath loadFilePath;
	QString argumentLayoutFileName = "";
	QHash<QString, QString> argumentPathValues;
	if (argc > 1) {
		TCli::Usage usage(argv[0]);
		TCli::UsageLine usageLine;
		TCli::FilePathArgument loadFileArg(
				"filePath", "Source scene file to open or script file to run");
		TCli::StringQualifier layoutFileQual(
				"-layout filename",
				"Custom layout file to be used, it should be saved in "
				"$TOONZPROFILES\\layouts\\personal\\[CurrentLayoutName].[UserName]\\. "
				"layouts.txt is used by default.");
		usageLine = usageLine + layoutFileQual;

		// system path qualifiers
		std::map<QString, std::unique_ptr<TCli::QualifierT<TFilePath>>>
				systemPathQualMap;
		QString qualKey  = QString("%1ROOT").arg(systemVarPrefix);
		QString qualName = QString("-%1 folderpath").arg(qualKey);
		QString qualHelp =
				QString(
						"%1 path. It will automatically set other system paths to %1 "
						"unless individually specified with other qualifiers.")
						.arg(qualKey);
		systemPathQualMap[qualKey].reset(new TCli::QualifierT<TFilePath>(
				qualName.toStdString(), qualHelp.toStdString()));
		usageLine = usageLine + *systemPathQualMap[qualKey];

		const std::map<std::string, std::string> &spm = TEnv::getSystemPathMap();
		for (auto itr = spm.begin(); itr != spm.end(); ++itr) {
			qualKey = QString("%1%2")
										.arg(systemVarPrefix)
										.arg(QString::fromStdString((*itr).first));
			qualName = QString("-%1 folderpath").arg(qualKey);
			qualHelp = QString("%1 path.").arg(qualKey);
			systemPathQualMap[qualKey].reset(new TCli::QualifierT<TFilePath>(
					qualName.toStdString(), qualHelp.toStdString()));
			usageLine = usageLine + *systemPathQualMap[qualKey];
		}
		usage.add(usageLine);
		usage.add(usageLine + loadFileArg);

		if (!usage.parse(argc, argv)) exit(1);

		loadFilePath = loadFileArg.getValue();
		if (layoutFileQual.isSelected())
			argumentLayoutFileName =
					QString::fromStdString(layoutFileQual.getValue());
		for (auto q_itr = systemPathQualMap.begin();
				 q_itr != systemPathQualMap.end(); ++q_itr) {
			if (q_itr->second->isSelected())
				argumentPathValues.insert(q_itr->first,
																	q_itr->second->getValue().getQString());
		}

		argc = 1;
	}


	if (mode == 5){
		//qDebug() << "using qt app pointer:" << qtapp_pointer;
		//app = (QGuiApplication*)qtapp_pointer;
		ToonzApplication = (QApplication*)QCoreApplication::instance();
		qDebug() << "qt app instance reuse:"<< ToonzApplication;
	} else {
		qDebug() << "making new qt application";
		// Enables high-DPI scaling. This attribute must be set before QApplication is
		// constructed. Available from Qt 5.6.
		//QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
		ToonzApplication= new QApplication(argc, argv);

		// Set the app's locale for numeric stuff to standard C. This is important for
		// atof() and similar
		// calls that are locale-dependent.
		if (mode==0) setlocale(LC_NUMERIC, "C");

	}
	qDebug() << "toonz qt application extern pointer=" << ToonzApplication;

	auto app = ToonzApplication;


#ifdef Q_OS_WIN
	//	Since currently OpenToonz does not work with OpenGL of software or
	// angle,	force Qt to use desktop OpenGL
	// FIXME: This options should be called before constructing the application.
	// Thus, ANGLE seems to be enabled as of now.
	app->setAttribute(Qt::AA_UseDesktopOpenGL, true);
#endif

	// Some Qt objects are destroyed badly without a living qApp. So, we must
	// enforce a way to either
	// postpone the application destruction until the very end, OR ensure that
	// sensible objects are
	// destroyed before.

	// Using a static QApplication only worked on Windows, and in any case C++
	// respects the statics destruction
	// order ONLY within the same library. On MAC, it made the app crash on exit
	// o_o. So, nope.

	std::unique_ptr<QObject> mainScope(new QObject(app));  // A QObject destroyed before the qApp is therefore explicitly
	mainScope->setObjectName("mainScope");  // provided. It can be accessed by
																					// looking in the qApp's children.

#ifdef _WIN32
#ifndef x64
	// Store the floating point control word. It will be re-set before Toonz
	// initialization
	// has ended.
	unsigned int fpWord = 0;
	_controlfp_s(&fpWord, 0, 0);
#endif
#endif

#ifdef _WIN32
	// At least on windows, Qt's 4.5.2 native windows feature tend to create
	// weird flickering effects when dragging panel separators.
	app->setAttribute(Qt::AA_DontCreateNativeWidgetSiblings);
#endif

	// Enable to render smooth icons on high dpi monitors
	app->setAttribute(Qt::AA_UseHighDpiPixmaps);
#if defined(_WIN32) && QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
	// Compress tablet events with application attributes instead of implementing
	// the delay-timer by ourselves
	app->setAttribute(Qt::AA_CompressHighFrequencyEvents);
	app->setAttribute(Qt::AA_CompressTabletEvents);
#endif

#ifdef _WIN32
	// This attribute is set to make menubar icon to be always (16 x devPixRatio).
	// Without this attribute the menu bar icon size becomes the same as tool bar
	// when Windows scale is in 125%. Currently hiding the menu bar icon is done
	// by setting transparent pixmap only in menu bar icon size. So the size must
	// be different between for menu bar and for tool bar.
	app->setAttribute(Qt::AA_Use96Dpi);
#endif


// Set current directory to the bundle/application path - this is needed to have
// correct relative paths
#ifdef MACOSX
	{
		QDir appDir(QApplication::applicationDirPath());
		appDir.cdUp(), appDir.cdUp(), appDir.cdUp();

		bool ret = QDir::setCurrent(appDir.absolutePath());
		assert(ret);
	}
#endif

	// Set icon theme search paths
	QStringList themeSearchPathsList = {":/icons"};
	QIcon::setThemeSearchPaths(themeSearchPathsList);
	qDebug() << "All icon theme search paths:" << QIcon::themeSearchPaths();

	// Set show icons in menus flag (use iconVisibleInMenu to disable selectively)
	QApplication::instance()->setAttribute(Qt::AA_DontShowIconsInMenus, false);

	TEnv::setApplicationFileName(argv[0]);

	// splash screen
	qDebug() << "load splash screen";
	QPixmap splashPixmap = QIcon(":Resources/splash.svg").pixmap(QSize(610, 344));

#ifdef _WIN32
	QFont font("Segoe UI", -1);
#else
	QFont font("Helvetica", -1);
#endif
	font.setPixelSize(13);
	font.setWeight(50);
	app->setFont(font);
	qDebug() << "font set OK";

	QString offsetStr("\n\n\n\n\n\n\n\n");

	TSystem::hasMainLoop(true);

	TMessageRepository::instance();

	bool isRunScript = (loadFilePath.getType() == "toonzscript");

	qDebug() << "show splash screen";

	QSplashScreen splash(splashPixmap);
	if (!isRunScript) splash.show();
	app->processEvents();

	splash.showMessage(offsetStr + "Initializing QGLFormat...", Qt::AlignCenter,
										 Qt::white);
	app->processEvents();

	// OpenGL
	QGLFormat fmt;
	fmt.setAlpha(true);
	fmt.setStencil(true);
	QGLFormat::setDefaultFormat(fmt);

	if(mode==0){
		glutInit(&argc, argv);
		qDebug() << "GL INIT OK";
	}

	splash.showMessage(offsetStr + "Initializing Toonz environment ...",
										 Qt::AlignCenter, Qt::white);
	app->processEvents();

	// Install run out of contiguous memory callback
	//TBigMemoryManager::instance()->setRunOutOfContiguousMemoryHandler(
	//		&toonzRunOutOfContMemHandler);

	// Toonz environment
	qDebug() << "initToonzEnv...";
	initToonzEnv(argumentPathValues);
	qDebug() << "initToonzEnv OK";

	// prepare for 30bit display
	if (Preferences::instance()->is30bitDisplayEnabled()) {
		QSurfaceFormat sFmt = QSurfaceFormat::defaultFormat();
		sFmt.setRedBufferSize(10);
		sFmt.setGreenBufferSize(10);
		sFmt.setBlueBufferSize(10);
		sFmt.setAlphaBufferSize(2);
		QSurfaceFormat::setDefaultFormat(sFmt);
	}

	// Initialize thread components
	TThread::init();

	qDebug() << "threads init OK";

	TProjectManager *projectManager = TProjectManager::instance();
	if (Preferences::instance()->isSVNEnabled()) {
		// Read Version Control repositories and add it to project manager as
		// "special" svn project root
		VersionControl::instance()->init();
		QList<SVNRepository> repositories =
				VersionControl::instance()->getRepositories();
		int count = repositories.size();
		for (int i = 0; i < count; i++) {
			SVNRepository r = repositories.at(i);

			TFilePath localPath(r.m_localPath.toStdWString());
			if (!TFileStatus(localPath).doesExist()) {
				try {
					TSystem::mkDir(localPath);
				} catch (TException &e) {
					fatalError(QString::fromStdWString(e.getMessage()));
				}
			}
			projectManager->addSVNProjectsRoot(localPath);
		}
	}

#if defined(MACOSX) && defined(__LP64__)

/*
	qDebug() << "setup shared memory";

	// Load the shared memory settings
	int shmmax = Preferences::instance()->getShmMax();
	int shmseg = Preferences::instance()->getShmSeg();
	int shmall = Preferences::instance()->getShmAll();
	int shmmni = Preferences::instance()->getShmMni();

	if (shmall <
			0)  // Make sure that at least 100 MB of shared memory are available
		shmall = (tipc::shm_maxSharedPages() < (100 << 8)) ? (100 << 8) : -1;

	tipc::shm_set(shmmax, shmseg, shmall, shmmni);
*/

#endif

	// DVDirModel must be instantiated after Version Control initialization...
	FolderListenerManager::instance()->addListener(DvDirModel::instance());

	splash.showMessage(offsetStr + "Loading Translator ...", Qt::AlignCenter,
										 Qt::white);
	app->processEvents();

	// Carico la traduzione contenuta in toonz.qm (se ï¿½ presente)
	QString languagePathString =
			QString::fromStdString(::to_string(TEnv::getConfigDir() + "loc"));
#ifndef WIN32
	// the merge of menu on osx can cause problems with different languages with
	// the Preferences menu
	// qt_mac_set_menubar_merge(false);
	languagePathString += "/" + Preferences::instance()->getCurrentLanguage();
#else
	languagePathString += "\\" + Preferences::instance()->getCurrentLanguage();
#endif
	QTranslator translator;
	translator.load("toonz", languagePathString);

	// La installo
	app->installTranslator(&translator);

	// Carico la traduzione contenuta in toonzqt.qm (se e' presente)
	QTranslator translator2;
	translator2.load("toonzqt", languagePathString);
	app->installTranslator(&translator2);

	// Carico la traduzione contenuta in tnzcore.qm (se e' presente)
	QTranslator tnzcoreTranslator;
	tnzcoreTranslator.load("tnzcore", languagePathString);
	qApp->installTranslator(&tnzcoreTranslator);

	// Carico la traduzione contenuta in toonzlib.qm (se e' presente)
	QTranslator toonzlibTranslator;
	toonzlibTranslator.load("toonzlib", languagePathString);
	qApp->installTranslator(&toonzlibTranslator);

	// Carico la traduzione contenuta in colorfx.qm (se e' presente)
	QTranslator colorfxTranslator;
	colorfxTranslator.load("colorfx", languagePathString);
	qApp->installTranslator(&colorfxTranslator);

	// Carico la traduzione contenuta in tools.qm
	QTranslator toolTranslator;
	toolTranslator.load("tnztools", languagePathString);
	qApp->installTranslator(&toolTranslator);

	// load translation for file writers properties
	QTranslator imageTranslator;
	imageTranslator.load("image", languagePathString);
	qApp->installTranslator(&imageTranslator);

	QTranslator qtTranslator;
	qtTranslator.load("qt_" + QLocale::system().name(),
										QLibraryInfo::location(QLibraryInfo::TranslationsPath));
	app->installTranslator(&qtTranslator);

	// Aggiorno la traduzione delle properties di tutti i tools
	TTool::updateToolsPropertiesTranslation();
	// Apply translation to file writers properties
	Tiio::updateFileWritersPropertiesTranslation();

	// Force to have left-to-right layout direction in any language environment.
	// This function has to be called after installTranslator().
	app->setLayoutDirection(Qt::LeftToRight);

	qDebug() << "translations loaded OK";

	splash.showMessage(offsetStr + "Loading styles ...", Qt::AlignCenter,
										 Qt::white);
	app->processEvents();

	// Set default start icon theme
	//QIcon::setThemeName(Preferences::instance()->getIconTheme() ? "dark" : "light");

	// qDebug() << "Icon theme name:" << QIcon::themeName();

	// stile
	//QApplication::setStyle("windows");

	IconGenerator::setFilmstripIconSize(Preferences::instance()->getIconSize());

	splash.showMessage(offsetStr + "Loading shaders ...", Qt::AlignCenter,
										 Qt::white);
	app->processEvents();
	loadShaderInterfaces(ToonzFolder::getLibraryFolder() + TFilePath("shaders"));
	qDebug() << "shaded loaded OK";


	splash.showMessage(offsetStr + "Initializing OpenToonz ...", Qt::AlignCenter,
										 Qt::white);
	app->processEvents();

	TTool::setApplication(TApp::instance());
	TApp::instance()->init();

	splash.showMessage(offsetStr + "Loading Plugins...", Qt::AlignCenter,
										 Qt::white);
	app->processEvents();
	/* poll the thread ends:
	 絶対に必要なわけではないが PluginLoader は中で setup
	 ハンドラが常に固有のスレッドで呼ばれるよう main thread queue の blocking
	 をしているので
	 processEvents を行う必要がある
*/
	while (!PluginLoader::load_entries("")) {
		app->processEvents();
	}

	splash.showMessage(offsetStr + "Creating main window ...", Qt::AlignCenter,
										 Qt::white);
	app->processEvents();

	/*-- Layoutファイル名をMainWindowのctorに渡す --*/
	//MainWindow w(argumentLayoutFileName);
	s_main_win = new MainWindow( argumentLayoutFileName );
	CrashHandler::attachParentWindow(s_main_win);
	CrashHandler::reportProjectInfo(true);

	if (isRunScript) {
		// load script
		if (TFileStatus(loadFilePath).doesExist()) {
			// find project for this script file
			TProjectManager *pm    = TProjectManager::instance();
			TProjectP sceneProject = pm->loadSceneProject(loadFilePath);
			TFilePath oldProjectPath;
			if (!sceneProject) {
				std::cerr << QObject::tr(
												 "It is not possible to load the scene %1 because it "
												 "does not "
												 "belong to any project.")
												 .arg(loadFilePath.getQString())
												 .toStdString()
									<< std::endl;
				return 1;
			}
			if (sceneProject && !sceneProject->isCurrent()) {
				oldProjectPath = pm->getCurrentProjectPath();
				pm->setCurrentProjectPath(sceneProject->getProjectPath());
			}
			ScriptEngine engine;
			QObject::connect(&engine, &ScriptEngine::output, script_output);
			QString s = QString::fromStdWString(loadFilePath.getWideString())
											.replace("\\", "\\\\")
											.replace("\"", "\\\"");
			QString cmd = QString("run(\"%1\")").arg(s);
			engine.evaluate(cmd);
			engine.wait();
			if (!oldProjectPath.isEmpty()) pm->setCurrentProjectPath(oldProjectPath);
			return 1;
		} else {
			std::cerr << QObject::tr("Script file %1 does not exists.")
											 .arg(loadFilePath.getQString())
											 .toStdString()
								<< std::endl;
			return 1;
		}
	}

#ifdef _WIN32
	// http://doc.qt.io/qt-5/windows-issues.html#fullscreen-opengl-based-windows
	if (s_main_win->windowHandle())
		QWindowsWindowFunctions::setHasBorderInFullScreen(s_main_win->windowHandle(), true);
#endif

		// Qt have started to support Windows Ink from 5.12.
		// Unlike WinTab API used in Qt 5.9 the tablet behaviors are different and
		// are (at least, for OT) problematic. The customized Qt5.15.2 are made with
		// cherry-picking the WinTab feature to be officially introduced from 6.0.
		// See https://github.com/shun-iwasawa/qt5/releases/tag/v5.15.2_wintab for
		// details. The following feature can only be used with the customized Qt,
		// with WITH_WINTAB build option, and in Windows-x64 build.

#ifdef WITH_WINTAB
	bool useQtNativeWinInk = Preferences::instance()->isQtNativeWinInkEnabled();
	QWindowsWindowFunctions::setWinTabEnabled(!useQtNativeWinInk);
#endif

	splash.showMessage(offsetStr + "Loading style sheet ...", Qt::AlignCenter,
										 Qt::white);
	app->processEvents();

	// Carico lo styleSheet
	QString currentStyle = Preferences::instance()->getCurrentStyleSheet();
	//app->setStyleSheet(currentStyle);  // not in QGuiApplication

	s_main_win->setWindowTitle(QString::fromStdString(TEnv::getApplicationFullName()));
	if (TEnv::getIsPortable()) {
		splash.showMessage(offsetStr + "Starting OpenToonz Portable ...",
											 Qt::AlignCenter, Qt::white);
	} else {
		splash.showMessage(offsetStr + "Starting main window ...", Qt::AlignCenter,
											 Qt::white);
	}
	app->processEvents();
	qDebug() << "style loaded OK";
	qDebug() << "loading mainwindow.ini";


	TFilePath fp = ToonzFolder::getModuleFile("mainwindow.ini");
	QSettings settings(toQString(fp), QSettings::IniFormat);
/*
	if (settings.contains("MainWindowGeometry"))
		s_main_win->restoreGeometry(settings.value("MainWindowGeometry").toByteArray());
	else  // maximize window on the first launch
		s_main_win->setWindowState(s_main_win->windowState() | Qt::WindowMaximized);
*/
	s_main_win->resize(640,480);
	ExpressionReferenceManager::instance()->init();

#ifndef MACOSX
	// Workaround for the maximized window case: Qt delivers two resize events,
	// one in the normal geometry, before
	// maximizing (why!?), the second afterwards - all inside the following show()
	// call. This makes troublesome for
	// the docking system to correctly restore the saved geometry. Fortunately,
	// MainWindow::showEvent(..) gets called
	// just between the two, so we can disable the currentRoom layout right before
	// showing and re-enable it after
	// the normal resize has happened.
	if (s_main_win->isMaximized()) s_main_win->getCurrentRoom()->layout()->setEnabled(false);
#endif
	qDebug() << "finish splash screen";

	QRect splashGeometry = splash.geometry();
	splash.finish(s_main_win);

	app->setQuitOnLastWindowClosed(false);
	// a.connect(&a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()));
	//if (Preferences::instance()->isLatestVersionCheckEnabled())
	//	s_main_win->checkForUpdates();

	qDebug() << "show main window...";
	s_main_win->show();
	qDebug() << "show main window OK";

	// Show floating panels only after the main window has been shown
	//w.startupFloatingPanels();
	//qDebug() << "startupFloatingPanels OK";

	CommandManager::instance()->execute(T_Hand);
	if (!loadFilePath.isEmpty()) {
		splash.showMessage(
				QString("Loading file '") + loadFilePath.getQString() + "'...",
				Qt::AlignCenter, Qt::white);
		loadFilePath = loadFilePath.withType("tnz");
		if (TFileStatus(loadFilePath).doesExist()) IoCmd::loadScene(loadFilePath);
	}

	QFont *myFont;
	QString fontName  = Preferences::instance()->getInterfaceFont();
	QString fontStyle = Preferences::instance()->getInterfaceFontStyle();

	TFontManager *fontMgr = TFontManager::instance();
	std::vector<std::wstring> typefaces;
	bool isBold = false, isItalic = false, hasKerning = false;
	try {
		fontMgr->loadFontNames();
		fontMgr->setFamily(fontName.toStdWString());
		fontMgr->getAllTypefaces(typefaces);
		isBold     = fontMgr->isBold(fontName, fontStyle);
		isItalic   = fontMgr->isItalic(fontName, fontStyle);
		hasKerning = fontMgr->hasKerning();
	} catch (TFontCreationError &) {
		// Do nothing. A default font should load
	}

	myFont = new QFont(fontName);
	myFont->setPixelSize(EnvSoftwareCurrentFontSize);
	myFont->setBold(isBold);
	myFont->setItalic(isItalic);
	myFont->setKerning(hasKerning);

	app->setFont(*myFont);
	qDebug() << "myFont set OK";


	QAction *action = CommandManager::instance()->getAction("MI_OpenTMessage");
	if (action)
		QObject::connect(TMessageRepository::instance(),
										 SIGNAL(openMessageCenter()), action, SLOT(trigger()));

	QObject::connect(TUndoManager::manager(), SIGNAL(somethingChanged()),
									 TApp::instance()->getCurrentScene(), SLOT(setDirtyFlag()));

#ifdef _WIN32
#ifndef x64
	// On 32-bit architecture, there could be cases in which initialization could
	// alter the
	// FPU floating point control word. I've seen this happen when loading some
	// AVI coded (VFAPI),
	// where 80-bit internal precision was used instead of the standard 64-bit
	// (much faster and
	// sufficient - especially considering that x86 truncates to 64-bit
	// representation anyway).
	// IN ANY CASE, revert to the original control word.
	// In the x64 case these precision changes simply should not take place up to
	// _controlfp_s
	// documentation.
	_controlfp_s(0, fpWord, -1);
#endif
#endif

#ifdef _WIN32
	if (Preferences::instance()->isWinInkEnabled()) {
		KisTabletSupportWin8 *penFilter = new KisTabletSupportWin8();
		if (penFilter->init()) {
			app->installNativeEventFilter(penFilter);
		} else {
			delete penFilter;
		}
	}
#endif
	qDebug() << "installEventFilter";
	int ret = 0;
	app->installEventFilter(TApp::instance());
	if (mode==0){
		qDebug() << "run application exec";
		ret = app->exec();
		TUndoManager::manager()->reset();
		PreviewFxManager::instance()->reset();
	}
	return ret;
}

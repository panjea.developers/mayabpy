/* SPDX-License-Identifier: GPL-2.0-or-later
 * Copyright 2014 Blender Foundation. All rights reserved. */

/** \file
 * \ingroup GHOST
 *
 * Definition of GHOST_ContextQT class.
 */

#include "GHOST_ContextQT.h"
#include <vector>
#include <cassert>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <QtOpenGL/QGL>
#include <QtWidgets/QOpenGLWidget>

int QT_GL_MakeCurrent(GHOST_ContextQT *gctx, QT_Window *window, QOpenGLContext* ctx){
	if (window) {
		window->set_context(ctx);
	} else {
		#ifdef WITH_QT_GHOST_DEBUG
			std::cout << "GHOST_ContextQT QT_GL_MakeCurrent offscreen"<<ctx << std::endl;
		#endif
		//gctx->m_context->makeCurrent(gctx->m_offscreen_surf);
		ctx->makeCurrent(gctx->m_offscreen_surf);
	}
	#ifdef WITH_QT_GHOST_DEBUG
		std::cout << "GHOST_ContextQT QT_GL_MakeCurrent"<<ctx << std::endl;
	#endif
	// TODO ensure actually current
	return 1;
}

int QT_GL_SetSwapInterval(int){return 0;}
int QT_GL_GetSwapInterval(){return 33;}

void QT_GL_SwapWindow(GHOST_ContextQT *gctx, QT_Window *window){
	if(window){
		#ifdef WITH_QT_GHOST_DEBUG
			std::cout << "GHOST_ContextQT QT_GL_SwapWindow:"<<window << std::endl;
		#endif

		//window->renderNow();
		window->swap_buffers();
	} else {
		#ifdef WITH_QT_GHOST_DEBUG
			std::cout << "TODO GHOST_ContextQT QT_GL_SwapWindow offscreen" << std::endl;		
		#endif
		gctx->m_context->swapBuffers(gctx->m_offscreen_surf);
	}
}


QOpenGLContext* QT_GL_CreateContext(GHOST_ContextQT *gctx, QT_Window *window){
	std::cout << "GHOST_ContextQT QT_GL_CreateContext - processEvents - window:"<<window << std::endl;
	//std::cout << "GHOST_ContextQT QT_GL_CreateContext opengl_widget:"<<window->opengl_widget << std::endl;
	//auto ctx = window->opengl_widget->context();
	MenderApplication->processEvents();
	QOpenGLContext* ctx=nullptr;
	if (window){
		if (window->isExposed()){
			window->make_context();
		} else {
			std::cout << "ERROR: GHOST_ContextQT QT_GL_CreateContext failed because window is not exposed:"<<window << std::endl;
		}
		ctx = window->m_context;
	} else {
		std::cout << "GHOST_ContextQT QT_GL_CreateContext offscreen surface:"<<gctx->m_offscreen_surf<< std::endl;
		ctx = gctx->m_context;
	}
	std::cout << "GHOST_ContextQT QT_GL_CreateContext context:"<<ctx << std::endl;
	return ctx;
	//return nullptr;
}

QT_GLContext GHOST_ContextQT::s_sharedContext = nullptr;
int GHOST_ContextQT::s_sharedCount = 0;

GHOST_ContextQT::GHOST_ContextQT(bool stereoVisual,
																	 QT_Window *window,
																	 int contextProfileMask,
																	 int contextMajorVersion,
																	 int contextMinorVersion,
																	 int contextFlags,
																	 int contextResetNotificationStrategy)
		: GHOST_Context(stereoVisual),
			m_window(window),
			m_hidden_window(nullptr),
			m_contextProfileMask(contextProfileMask),
			m_contextMajorVersion(contextMajorVersion),
			m_contextMinorVersion(contextMinorVersion),
			m_contextFlags(contextFlags),
			m_contextResetNotificationStrategy(contextResetNotificationStrategy),
			m_context(nullptr)
{

	std::cout << "GHOST_ContextQT construct" << std::endl;
	std::cout << "GHOST_ContextQT opengl version requested: "<< contextMajorVersion <<"."<<contextMinorVersion << std::endl;

	std::cout << "GHOST_ContextQT construct QWindow pointer="<<m_window << std::endl;
	if (m_window == nullptr){
		std::cout << "GHOST_ContextQT construct making m_offscreen_surf" << std::endl;
		if (s_sharedCount){
			std::cout << "GHOST_ContextQT construct using shared context" << std::endl;
			m_context=s_sharedContext;
		} else {
			std::cout << "GHOST_ContextQT construct making new context" << std::endl;
			m_context= new QOpenGLContext();
			m_context->create();			
		}

		if(!m_context->isValid()) throw std::runtime_error(std::string("failed to create offscreen context"));
		m_offscreen_surf = new QOffscreenSurface();
		m_offscreen_surf->create();
		m_context->makeCurrent(m_offscreen_surf);
	} else {
		if(s_sharedCount==0 && m_window->m_context){
			std::cout << "GHOST_ContextQT construct new shared context"<<m_window->m_context << std::endl;
			s_sharedCount++;
			s_sharedContext=m_window->m_context;
		}
	}
	//window->opengl_widget = new QOpenGLWidget((QWidget*)window);
	//m_window->opengl_widget = new QOpenGLWidget();
	//std::cout << "GHOST_ContextQT construct OK:"<< m_window->opengl_widget << std::endl;
}


GHOST_ContextQT::~GHOST_ContextQT()
{
	if (m_context == nullptr) {
		return;
	}
/*
	if (m_window != nullptr && m_context == QT_GL_GetCurrentContext()) {
		QT_GL_MakeCurrent(m_window, nullptr);
	}
	if (m_context != s_sharedContext || s_sharedCount == 1) {
		assert(s_sharedCount > 0);

		s_sharedCount--;

		if (s_sharedCount == 0) {
			s_sharedContext = nullptr;
		}
		QT_GL_DeleteContext(m_context);
	}

	if (m_hidden_window != nullptr) {
		QT_DestroyWindow(m_hidden_window);
	}
*/
}

GHOST_TSuccess GHOST_ContextQT::swapBuffers()
{
	QT_GL_SwapWindow(this,m_window);
	return GHOST_kSuccess;
}

GHOST_TSuccess GHOST_ContextQT::activateDrawingContext()
{
	#ifdef WITH_QT_GHOST_DEBUG
	std::cout << "GHOST_ContextQT activateDrawingContext:"<< this << std::endl;
	#endif
	if (m_context == nullptr) {
		return GHOST_kFailure;
	}
	return QT_GL_MakeCurrent(this, m_window, m_context) ? GHOST_kSuccess : GHOST_kFailure;
}

GHOST_TSuccess GHOST_ContextQT::releaseDrawingContext()
{
	std::cout << "GHOST_ContextQT releaseDrawingContext:"<< this << std::endl;

	if (m_context == nullptr) {
		return GHOST_kFailure;
	}
	/* Untested, may not work. */
	return QT_GL_MakeCurrent(this, nullptr, nullptr) ? GHOST_kSuccess : GHOST_kFailure;
}

GHOST_TSuccess GHOST_ContextQT::initializeDrawingContext()
{
	std::cout << "GHOST_WindowQT initializeDrawingContext:"<< this << std::endl;

#ifdef GHOST_OPENGL_ALPHA
	const bool needAlpha = true;
#else
	const bool needAlpha = false;
#endif
/*
	QT_GL_SetAttribute(QT_GL_CONTEXT_PROFILE_MASK, m_contextProfileMask);
	QT_GL_SetAttribute(QT_GL_CONTEXT_MAJOR_VERSION, m_contextMajorVersion);
	QT_GL_SetAttribute(QT_GL_CONTEXT_MINOR_VERSION, m_contextMinorVersion);
	QT_GL_SetAttribute(QT_GL_CONTEXT_FLAGS, m_contextFlags);

	QT_GL_SetAttribute(QT_GL_SHARE_WITH_CURRENT_CONTEXT, 1);
	QT_GL_SetAttribute(QT_GL_DOUBLEBUFFER, 1);
	QT_GL_SetAttribute(QT_GL_RED_SIZE, 8);
	QT_GL_SetAttribute(QT_GL_GREEN_SIZE, 8);
	QT_GL_SetAttribute(QT_GL_BLUE_SIZE, 8);

	if (needAlpha) {
		QT_GL_SetAttribute(QT_GL_ALPHA_SIZE, 8);
	}

	if (m_stereoVisual) {
		QT_GL_SetAttribute(QT_GL_STEREO, 1);
	}
*/

/*
	if (m_window == nullptr) {
		m_hidden_window = QT_CreateWindow("Offscreen Context Windows",
																			 QT_WINDOWPOS_UNDEFINED,
																			 QT_WINDOWPOS_UNDEFINED,
																			 1,
																			 1,
																			 QT_WINDOW_OPENGL | QT_WINDOW_BORDERLESS |
																					 QT_WINDOW_HIDDEN);

		m_window = m_hidden_window;
	}
*/
	m_context = QT_GL_CreateContext(this,m_window);

	GHOST_TSuccess success;

	if (m_context != nullptr) {
		std::cout << "GHOST_ContextQT initializeDrawingContext"<< m_context << std::endl;

		if (!s_sharedContext) {
			s_sharedContext = m_context;
		}
		s_sharedCount++;

		success = (QT_GL_MakeCurrent(this, m_window, m_context) < 0) ? GHOST_kFailure : GHOST_kSuccess;

		std::cout << "GHOST_ContextQT initClearGL" << std::endl;
		initClearGL();
		QT_GL_SwapWindow(this,m_window);
		success = GHOST_kSuccess;
	}
	else {
		std::cout << "ERROR: GHOST_ContextQT initializeDrawingContext failure" << std::endl;
		success = GHOST_kFailure;
	}

	return success;
}

GHOST_TSuccess GHOST_ContextQT::releaseNativeHandles()
{
	m_window = nullptr;
	return GHOST_kSuccess;
}

GHOST_TSuccess GHOST_ContextQT::setSwapInterval(int interval)
{
	//if (QT_GL_SetSwapInterval(interval) == -1) {
	//	return GHOST_kFailure;
	//}
	return GHOST_kSuccess;
}

GHOST_TSuccess GHOST_ContextQT::getSwapInterval(int &intervalOut)
{
	//intervalOut = QT_GL_GetSwapInterval();  // TODO
	return GHOST_kSuccess;
}

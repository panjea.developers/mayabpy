/* SPDX-License-Identifier: GPL-2.0-or-later */

/** \file
 * \ingroup GHOST
 */

#include "GHOST_WindowQT.h"
#include "GHOST_ContextQT.h"
#include "GHOST_QT.h"

#include <cassert>
#include <iostream>

#define BLENDERQT_HEADER_OFFSET 32

void QT_SetWindowSize(QT_Window *ptr, int w, int h){
	std::cout << "GHOST_WindowQT QT_SetWindowSize: w="<<w <<" h="<<h << std::endl;
	//ptr->setWidth(w);
	//ptr->setHeight(h);
	ptr->resize(w,h);
}
void QT_GetWindowSize(QT_Window *ptr, int &w, int &h){
	w=ptr->width();
	h=ptr->height();
}
void QT_GetWindowPosition(QT_Window *ptr, int &x, int &y){
	x=ptr->x();
	y=ptr->y();
}
int QT_GetWindowDisplayIndex(QT_Window *ptr){ return 0;}

GHOST_WindowQT::GHOST_WindowQT(GHOST_SystemQT *system,
								 const char *title,
								 int32_t left,
								 int32_t top,
								 uint32_t width,
								 uint32_t height,
								 GHOST_TWindowState state,
								 GHOST_TDrawingContextType type,
								 const bool stereoVisual,
								 const bool exclusive,
								 const GHOST_IWindow * /*parentWindow*/)
	: GHOST_Window(width, height, state, stereoVisual, exclusive),
	  m_system(system),
	  m_valid_setup(false),
	  m_invalid_window(false)
{

  /* creating the window _must_ come after setting attributes */
/*
  m_qt_win = QT_CreateWindow(title,
							   left,
							   top,
							   width,
							   height,
							   QT_WINDOW_RESIZABLE | QT_WINDOW_OPENGL | QT_WINDOW_SHOWN);

*/
	std::cout << "GHOST_WindowQT construct" << std::endl;

	m_qt_win = new QtBlenderWindow(); //new QWindow();
	std::cout << "GHOST_WindowQT QWindow pointer:"<<m_qt_win << std::endl;

	m_qt_win->resize(width,height);
	m_qt_win->show();
	m_qt_win->setAnimating(true);

  /* now set up the rendering context. */
  if (setDrawingContextType(type) == GHOST_kSuccess) {
	std::cout << "GHOST_WindowQT setDrawingContextType OK" << std::endl;
	m_valid_setup = true;
	GHOST_PRINT("Created window\n");
  } else {
	std::cout << "ERROR: GHOST_WindowQT setDrawingContextType FAIL" << std::endl;
  }

  //if (exclusive) {
	m_qt_win->show();
  //}

  setTitle(title);
}

GHOST_WindowQT::~GHOST_WindowQT()
{
	std::cout << "GHOST_WindowQT delete" << std::endl;
  releaseNativeHandles();
  delete m_qt_win;
}

GHOST_Context *GHOST_WindowQT::newDrawingContext(GHOST_TDrawingContextType type)
{
	std::cout << "GHOST_WindowQT newDrawingContext" << std::endl;

  if (type == GHOST_kDrawingContextTypeOpenGL) {
	GHOST_Context *context = new GHOST_ContextQT(m_wantStereoVisual,
												  m_qt_win,
												  0,  // profile bit
												  3,
												  3,
												  GHOST_OPENGL_QT_CONTEXT_FLAGS,
												  GHOST_OPENGL_QT_RESET_NOTIFICATION_STRATEGY);
	std::cout << "GHOST_WindowQT context ptr="<<context << std::endl;

	if (context->initializeDrawingContext()) {
		std::cout << "GHOST_WindowQT initializeDrawingContext OK" << std::endl;
	  return context;
	} else {
		std::cout << "ERROR: GHOST_WindowQT initializeDrawingContext FAIL" << std::endl;
	}
	delete context;
  }

  return nullptr;
}

GHOST_TSuccess GHOST_WindowQT::invalidate()
{
	std::cout << "GHOST_WindowQT invalidate" << std::endl;
  if (m_invalid_window == false) {
	m_system->addDirtyWindow(this);
	m_invalid_window = true;
  }

  return GHOST_kSuccess;
}

GHOST_TSuccess GHOST_WindowQT::setState(GHOST_TWindowState state)
{
	std::cout << "GHOST_WindowQT setState" << std::endl;

	/*
  switch (state) {
	case GHOST_kWindowStateNormal:
	  QT_SetWindowFullscreen(m_qt_win, QT_FALSE);
	  QT_RestoreWindow(m_qt_win);
	  break;
	case GHOST_kWindowStateMaximized:
	  QT_SetWindowFullscreen(m_qt_win, QT_FALSE);
	  QT_MaximizeWindow(m_qt_win);
	  break;
	case GHOST_kWindowStateMinimized:
	  QT_MinimizeWindow(m_qt_win);
	  break;
	case GHOST_kWindowStateFullScreen:
	  QT_SetWindowFullscreen(m_qt_win, QT_TRUE);
	  break;
	default:
	  break;
  }
*/
  return GHOST_kSuccess;
}

GHOST_TWindowState GHOST_WindowQT::getState() const {
	#ifdef WITH_QT_DEBUG
		std::cout << "GHOST_WindowQT getState" << std::endl;
	#endif

	// TODO move this hack somewhere else
	m_qt_win->make_current();  // testing workaround for toonz

  //Uint32 flags = QT_GetWindowFlags(m_qt_win);
/*
  if (flags & QT_WINDOW_FULLSCREEN) {
	return GHOST_kWindowStateFullScreen;
  }
  if (flags & QT_WINDOW_MAXIMIZED) {
	return GHOST_kWindowStateMaximized;
  }
  if (flags & QT_WINDOW_MINIMIZED) {
	return GHOST_kWindowStateMinimized;
  }
*/
  return GHOST_kWindowStateNormal;
}

bool GHOST_WindowQT::getValid() const
{
	std::cout << "GHOST_WindowQT getValid" << std::endl;

  return GHOST_Window::getValid() && m_valid_setup;
}

void GHOST_WindowQT::setTitle(const char *title) {
	std::cout << "GHOST_WindowQT setTitle"<< title << std::endl;
	m_qt_win->setTitle( QString::fromStdString( std::string("BlenderQT:")+std::string(title).c_str() ) );
}

std::string GHOST_WindowQT::getTitle() const
{
  return m_qt_win->title().toStdString();
}

void GHOST_WindowQT::getWindowBounds(GHOST_Rect &bounds) const
{
  getClientBounds(bounds);
}

void GHOST_WindowQT::getClientBounds(GHOST_Rect &bounds) const
{
	int x=0;
	int y=0;
	int w=0;
	int h=0;
	QT_GetWindowSize(m_qt_win, w, h);
	QT_GetWindowPosition(m_qt_win, x, y);

	bounds.m_l = x;
	bounds.m_r = x + (w*2);
	bounds.m_t = y;
	bounds.m_b = y + ((h*2)-BLENDERQT_HEADER_OFFSET);   // TODO
}

GHOST_TSuccess GHOST_WindowQT::setClientWidth(uint32_t width)
{
	int w=0;
	int height=0;
	QT_GetWindowSize(m_qt_win, w, height);
	QT_SetWindowSize(m_qt_win, width, height);
	return GHOST_kSuccess;
}

GHOST_TSuccess GHOST_WindowQT::setClientHeight(uint32_t height)
{
	int width=0;
	int h=0;
	QT_GetWindowSize(m_qt_win, width, h);
	QT_SetWindowSize(m_qt_win, width, height);
	return GHOST_kSuccess;
}

GHOST_TSuccess GHOST_WindowQT::setClientSize(uint32_t width, uint32_t height)
{
	QT_SetWindowSize(m_qt_win, width, height);
	return GHOST_kSuccess;
}

void GHOST_WindowQT::screenToClient(int32_t inX, int32_t inY, int32_t &outX, int32_t &outY) const
{
	/* XXXQT_WEAK_ABS_COORDS */
	int x_win=0;
	int y_win=0;
	QT_GetWindowPosition(m_qt_win, x_win, y_win);
	outX = inX - x_win;
	outY = inY - y_win;
}
void GHOST_WindowQT::clientToScreen(int32_t inX, int32_t inY, int32_t &outX, int32_t &outY) const
{
	/* XXXQT_WEAK_ABS_COORDS */
	//int x_win, y_win;
	//QT_GetWindowPosition(m_qt_win, &x_win, &y_win);
	int x_win=0;
	int y_win=0;
	QT_GetWindowPosition(m_qt_win, x_win, y_win);

	outX = inX + x_win;
	outY = inY + y_win;
}

GHOST_TSuccess GHOST_WindowQT::setWindowCursorGrab(GHOST_TGrabCursorMode /*mode*/)
{
  return GHOST_kSuccess;
}

GHOST_TSuccess GHOST_WindowQT::setWindowCursorShape(GHOST_TStandardCursor shape)
{
  return GHOST_kSuccess;
}

GHOST_TSuccess GHOST_WindowQT::hasCursorShape(GHOST_TStandardCursor shape)
{
  return GHOST_kFailure;
}

GHOST_TSuccess GHOST_WindowQT::setWindowCustomCursorShape(uint8_t *bitmap,
														   uint8_t *mask,
														   int sizex,
														   int sizey,
														   int hotX,
														   int hotY,
														   bool /*canInvertColor*/)
{
  return GHOST_kSuccess;
}

GHOST_TSuccess GHOST_WindowQT::setWindowCursorVisibility(bool visible)
{
  return GHOST_kSuccess;
}

uint16_t GHOST_WindowQT::getDPIHint()
{
	return 227;  // MENDER HACK FOR OSX
  int displayIndex = QT_GetWindowDisplayIndex(m_qt_win);
  if (displayIndex < 0) {
	return 96;
  }

  float ddpi;
  //if (QT_GetDisplayDPI(displayIndex, &ddpi, nullptr, nullptr) != 0) {
	return 96;
  //}

  return int(ddpi);
}

/* SPDX-License-Identifier: GPL-2.0-or-later */

/** \file
 * \ingroup GHOST
 */
#include <sys/time.h>
//#include <unistd.h>

#include <cassert>
#include <stdexcept>
#include <iostream>

#include "GHOST_ContextQT.h"
#include "GHOST_SystemQT.h"
#include "GHOST_WindowQT.h"

#include "GHOST_WindowManager.h"

#include "GHOST_EventButton.h"
#include "GHOST_EventCursor.h"
#include "GHOST_EventKey.h"
#include "GHOST_EventWheel.h"

#include <QtGui/QScreen>

GHOST_SystemQT::GHOST_SystemQT() : GHOST_System() {
	/*
	QT_GL_SetAttribute(QT_GL_DOUBLEBUFFER, 1);
	QT_GL_SetAttribute(QT_GL_RED_SIZE, 8);
	QT_GL_SetAttribute(QT_GL_GREEN_SIZE, 8);
	QT_GL_SetAttribute(QT_GL_BLUE_SIZE, 8);
	QT_GL_SetAttribute(QT_GL_ALPHA_SIZE, 8);
	*/
	std::cout << "new GHOST_SystemQT" << std::endl;

	/* compute the initial time */
	timeval tv;
	if (gettimeofday(&tv, nullptr) == -1) {
	GHOST_ASSERT(false, "Could not instantiate timer!");
	}

	/* Taking care not to overflow the `tv.tv_sec * 1000`. */
	m_start_time = uint64_t(tv.tv_sec) * 1000 + tv.tv_usec / 1000;

}

GHOST_SystemQT::~GHOST_SystemQT()
{
	//QT_Quit();
	std::cout << "delete GHOST_SystemQT" << std::endl;
}

GHOST_IWindow *GHOST_SystemQT::createWindow(const char *title,
																						 int32_t left,
																						 int32_t top,
																						 uint32_t width,
																						 uint32_t height,
																						 GHOST_TWindowState state,
																						 GHOST_TDrawingContextType type,
																						 GHOST_GLSettings glSettings,
																						 const bool exclusive,
																						 const bool /* is_dialog */,
																						 const GHOST_IWindow *parentWindow)
{
	GHOST_WindowQT *window = nullptr;
	std::cout << "GHOST_SystemQT createWindow" << std::endl;

	window = new GHOST_WindowQT(this,
															 title,
															 left,
															 top,
															 width,
															 height,
															 state,
															 type,
															 ((glSettings.flags & GHOST_glStereoVisual) != 0),
															 exclusive,
															 parentWindow);

	std::cout << "GHOST_SystemQT window pointer=" << window << std::endl;


	if (window) {
		/*
		if (GHOST_kWindowStateFullScreen == state) {
			QT_Window *sdl_win = window->getQTWindow();
			QT_DisplayMode mode;

			static_cast<GHOST_DisplayManagerQT *>(m_displayManager)->getCurrentDisplayModeQT(mode);

			QT_SetWindowDisplayMode(sdl_win, &mode);
			QT_ShowWindow(sdl_win);
			QT_SetWindowFullscreen(sdl_win, QT_TRUE);
		}
		*/
		if (window->getValid()) {
			std::cout << "GHOST_SystemQT window is valid OK" << std::endl;
			m_windowManager->addWindow(window);
			//pushEvent(new GHOST_Event(getMilliSeconds(), GHOST_kEventWindowSize, window));
			std::cout << "GHOST_SystemQT window addWindow OK" << std::endl;
		}
		else {
			std::cout << "ERROR: GHOST_SystemQT invalid window" << std::endl;
			delete window;
			window = nullptr;
		}
		m_current_window = window;
	}
	return window;
}

GHOST_TSuccess GHOST_SystemQT::init()
{
	std::cout << "GHOST_SystemQT init" << std::endl;
	GHOST_TSuccess success = GHOST_System::init();

	if (success) {
		std::cout << "GHOST_SystemQT init OK" << std::endl;
		m_displayManager = new GHOST_DisplayManagerQT(this);
		if (m_displayManager) {
			std::cout << "GHOST_SystemQT display manager OK" << std::endl;
			return GHOST_kSuccess;
		} else {
			std::cout << "GHOST_SystemQT no display manager" << std::endl;
		}
	} else {
		std::cout << "ERROR: GHOST_SystemQT init failed" << std::endl;
	}

	return GHOST_kFailure;
}

/**
 * Returns the dimensions of the main display on this system.
 * \return The dimension of the main display.
 */
void GHOST_SystemQT::getAllDisplayDimensions(uint32_t &width, uint32_t &height) const
{
	auto xy = QGuiApplication::primaryScreen()->size();
	int x = xy.width();
	int y = xy.height();

	#ifdef WITH_QT_GHOST_DEBUG
		std::cout << "GHOST_SystemQT getAllDisplayDimensions:"<<x<<","<<y<< std::endl;
	#endif

	width = (uint32_t)x;
	height = (uint32_t)y;
}

void GHOST_SystemQT::getMainDisplayDimensions(uint32_t &width, uint32_t &height) const
{
	auto xy = QGuiApplication::primaryScreen()->size();
	int x = xy.width();
	int y = xy.height();
	std::cout << "GHOST_SystemQT getMainDisplayDimensions:"<<x<<","<<y<< std::endl;
	width = (uint32_t)x;
	height = (uint32_t)y;
}


uint8_t GHOST_SystemQT::getNumDisplays() const
{
	std::cout << "GHOST_SystemQT getNumDisplays" << std::endl;
	//return QT_GetNumVideoDisplays();
	return 1;
}



GHOST_IContext *GHOST_SystemQT::createOffscreenContext(GHOST_GLSettings /*glSettings*/)
{
	std::cout << "GHOST_SystemQT createOffscreenContext" << std::endl;
	//throw std::runtime_error(std::string("TODO offscreen context"));

	GHOST_Context *context = new GHOST_ContextQT(false,
																								nullptr,
																								0, // Profile bit.
																								3,
																								3,
																								GHOST_OPENGL_QT_CONTEXT_FLAGS,
																								GHOST_OPENGL_QT_RESET_NOTIFICATION_STRATEGY);

	if (context->initializeDrawingContext()) {
		return context;
	} else {
		std::cout << "ERROR: GHOST_SystemQT context initializeDrawingContext failed" << context << std::endl;

	}
	delete context;

	return nullptr;
}

GHOST_TSuccess GHOST_SystemQT::disposeContext(GHOST_IContext *context)
{
	std::cout << "GHOST_SystemQT disposeContext" << context << std::endl;

	delete context;
	return GHOST_kSuccess;
}


GHOST_TSuccess GHOST_SystemQT::getModifierKeys(GHOST_ModifierKeys &keys) const
{
	std::cout << "GHOST_SystemQT getModifierKeys" << std::endl;

/*
	QT_Keymod mod = QT_GetModState();

	keys.set(GHOST_kModifierKeyLeftShift, (mod & KMOD_LSHIFT) != 0);
	keys.set(GHOST_kModifierKeyRightShift, (mod & KMOD_RSHIFT) != 0);
	keys.set(GHOST_kModifierKeyLeftControl, (mod & KMOD_LCTRL) != 0);
	keys.set(GHOST_kModifierKeyRightControl, (mod & KMOD_RCTRL) != 0);
	keys.set(GHOST_kModifierKeyLeftAlt, (mod & KMOD_LALT) != 0);
	keys.set(GHOST_kModifierKeyRightAlt, (mod & KMOD_RALT) != 0);
	keys.set(GHOST_kModifierKeyLeftOS, (mod & KMOD_LGUI) != 0);
	keys.set(GHOST_kModifierKeyRightOS, (mod & KMOD_RGUI) != 0);
*/
	return GHOST_kSuccess;
}



/**
 * Events don't always have valid windows,
 * but GHOST needs a window _always_. fallback to the GL window.
 */
/*
static QT_Window *QT_GetWindowFromID_fallback(Uint32 id)
{
	QT_Window *sdl_win = QT_GetWindowFromID(id);
	if (sdl_win == nullptr) {
		sdl_win = QT_GL_GetCurrentWindow();
	}
	return sdl_win;
}
*/

GHOST_TSuccess GHOST_SystemQT::getCursorPosition(int32_t &x, int32_t &y) const
{
	x = 0;
	y = 0;
	return GHOST_kSuccess;
}

GHOST_TSuccess GHOST_SystemQT::setCursorPosition(int32_t x, int32_t y)
{
	//int x_win, y_win;
	//QT_Window *win = QT_GetMouseFocus();
	//QT_GetWindowPosition(win, &x_win, &y_win);
	//QT_WarpMouseInWindow(win, x - x_win, y - y_win);
	return GHOST_kSuccess;
}

bool GHOST_SystemQT::generateWindowExposeEvents()
{
	std::cout << "GHOST_SystemQT generateWindowExposeEvents" << std::endl;

	std::vector<GHOST_WindowQT *>::iterator w_start = m_dirty_windows.begin();
	std::vector<GHOST_WindowQT *>::const_iterator w_end = m_dirty_windows.end();
	bool anyProcessed = false;

	for (; w_start != w_end; ++w_start) {
		//GHOST_Event *g_event = new GHOST_Event(getMilliSeconds(), GHOST_kEventWindowUpdate, *w_start);

		//(*w_start)->validate();

		//if (g_event) {
			// printf("Expose events pushed\n");
		//	pushEvent(g_event);
		//	anyProcessed = true;
		//}
	}

	m_dirty_windows.clear();
	return anyProcessed;
}

bool GHOST_SystemQT::processEvents(bool waitForEvent)
{
	/* Get all the current events - translate them into
	 * ghost events and call base class #pushEvent() method. */
	//std::cout << "GHOST_SystemQT processEvents - wait="<<waitForEvent << std::endl;
	bool anyProcessed = false;
	if (QCoreApplication::hasPendingEvents()){
		MenderApplication->processEvents();
		anyProcessed=true;
		//std::cout << "GHOST_SystemQT processEvents" << std::endl;
	} else {
		std::cout << "GHOST_SystemQT no events" << std::endl;
	}
	if (m_current_window){
		auto g_event = new GHOST_Event(getMilliSeconds(), GHOST_kEventWindowUpdate,m_current_window);
		pushEvent(g_event);	
		pushEvent(new GHOST_Event(getMilliSeconds(), GHOST_kEventWindowSize, m_current_window));

	}
	return anyProcessed;
}

GHOST_WindowQT *GHOST_SystemQT::findGhostWindow(QT_Window *sdl_win)
{
	std::cout << "GHOST_SystemQT findGhostWindow"<< sdl_win << std::endl;

	if (sdl_win == nullptr) {
		return nullptr;
	}
	/* It is not entirely safe to do this as the backptr may point
	 * to a window that has recently been removed.
	 * We should always check the window manager's list of windows
	 * and only process events on these windows. */

	const std::vector<GHOST_IWindow *> &win_vec = m_windowManager->getWindows();

	std::vector<GHOST_IWindow *>::const_iterator win_it = win_vec.begin();
	std::vector<GHOST_IWindow *>::const_iterator win_end = win_vec.end();

	for (; win_it != win_end; ++win_it) {
		GHOST_WindowQT *window = static_cast<GHOST_WindowQT *>(*win_it);
		if (window->getQTWindow() == sdl_win) {
			return window;
		}
	}
	return nullptr;
}

void GHOST_SystemQT::addDirtyWindow(GHOST_WindowQT *bad_wind)
{
	std::cout << "GHOST_SystemQT addDirtyWindow"<< bad_wind << std::endl;

	GHOST_ASSERT((bad_wind != nullptr), "addDirtyWindow() nullptr ptr trapped (window)");

	m_dirty_windows.push_back(bad_wind);
}

GHOST_TSuccess GHOST_SystemQT::getButtons(GHOST_Buttons &buttons) const
{
/*
	Uint8 state = QT_GetMouseState(nullptr, nullptr);
	buttons.set(GHOST_kButtonMaskLeft, (state & QT_BUTTON_LMASK) != 0);
	buttons.set(GHOST_kButtonMaskMiddle, (state & QT_BUTTON_MMASK) != 0);
	buttons.set(GHOST_kButtonMaskRight, (state & QT_BUTTON_RMASK) != 0);
*/
	return GHOST_kSuccess;
}

char *GHOST_SystemQT::getClipboard(bool /*selection*/) const
{
	//return (char *)QT_GetClipboardText();
	return "TODO CLIPBOARD";
}

void GHOST_SystemQT::putClipboard(const char *buffer, bool /*selection*/) const
{
	//QT_SetClipboardText(buffer);
}

uint64_t GHOST_SystemQT::getMilliSeconds()
{
	//std::cout << "GHOST_SystemQT getMilliSeconds" << std::endl;
	timeval tv;
	if (gettimeofday(&tv, nullptr) == -1) {
		GHOST_ASSERT(false, "Could not compute time!");
	}
	/* Taking care not to overflow the tv.tv_sec * 1000 */
	return uint64_t(tv.tv_sec) * 1000 + tv.tv_usec / 1000 - m_start_time;
}


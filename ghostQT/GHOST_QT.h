#pragma once

// building with blender requires epoxy is included first
#include <epoxy/gl.h>

// note: x11 includes will define Bool
//#undef Bool
//#undef Status
//#undef None
// note: these undefs still fail

//#include <QtCore/QDataStream>
#include <QtGui/QWindow>
#include <QtOpenGL/QGL>
//#include <QtWidgets/QOpenGLWidget>

#define QT_GLContext QOpenGLContext*

//#define QT_Window QWindow

//#include <QtOpenGL/QGL>
//#include <QtWidgets/QOpenGLWidget>
//struct QtBlenderWindow : QWindow {
//    QOpenGLWidget* opengl_widget;
//};

#include <QtGui/QSurfaceFormat>
#include <QtGui/QGuiApplication>
#include <QtGui/QOpenGLPaintDevice>
#include <QtGui/QPainter>
#include <QtGui/QCloseEvent>
#include <QtGui/QExposeEvent>
#include <QtGui/QPaintEvent>
#include <QtWidgets/QApplication>

#include <iostream>

//extern QGuiApplication* MenderApplication;
extern QApplication* MenderApplication;
extern QApplication* ToonzApplication;


struct QtBlenderWindow : QWindow {
	bool m_animating = false;
	QOpenGLContext *m_context = nullptr;
	QOpenGLPaintDevice *m_device = nullptr;

	QtBlenderWindow(QWindow *parent=nullptr) : QWindow(parent){
		std::cout<<"new QtBlenderWindow"<<std::endl;
		setSurfaceType(QWindow::OpenGLSurface);
	}
	~QtBlenderWindow() {
		std::cout<<"delete QtBlenderWindow"<<std::endl;
	}

	void render(QPainter *painter){
		std::cout<<"QtBlenderWindow QPainter"<<std::endl;
	}
	void render(){
		std::cout<<"QtBlenderWindow render"<<std::endl;
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		glClearColor(0, 1, 0, 1);

		//if (!m_device) m_device = new QOpenGLPaintDevice;
		//m_device->setSize(size() * devicePixelRatio());
		//m_device->setDevicePixelRatio(devicePixelRatio());
		//QPainter painter(m_device);
		//render(&painter);

	}
	void initialize() {}
	void setAnimating(bool animating){
		m_animating = animating;
		if (animating) renderLater();
	}
	void set_context(QOpenGLContext* ctx){
		m_context=ctx;
		m_context->makeCurrent(this);
	}
	void make_current(){
		m_context->makeCurrent(this);		
	}
	void make_context(){
		if(m_context==nullptr){
			std::cout<<"QtBlenderWindow new render context"<<std::endl;
			m_context = new QOpenGLContext(this);
			m_context->setFormat(requestedFormat());
			m_context->create();
			std::cout<<"QtBlenderWindow context:"<< m_context <<std::endl;			
		} else {
			std::cout<<"WARN: calling make_context - QtBlenderWindow already has context:"<< m_context <<std::endl;						
		}
	}
	//public slots:
	void renderLater(){
		requestUpdate();
	}
	void renderNow(){
		if (!isExposed())
		    return;
		std::cout<<"QtBlenderWindow renderNow"<<std::endl;

		bool needsInitialize = false;

		if (!m_context) {
			make_context();
		    needsInitialize = true;
		}

		m_context->makeCurrent(this);

		if (needsInitialize) {
		    //initializeOpenGLFunctions();
		    initialize();
		}

		render();

		m_context->swapBuffers(this);

		//if (m_animating) renderLater();

	}
	void swap_buffers(){
		m_context->swapBuffers(this);
	}
	bool event(QEvent *event) override {
		switch (event->type()) {
			case QEvent::UpdateRequest:
				std::cout<<"QtBlenderWindow::event UpdateRequest - renderNow"<<std::endl;
				renderNow();
				return true;
			case QEvent::Close:
				std::cout<<"closing QtBlenderWindow"<<std::endl;
				MenderApplication->quit();
				throw "never reached";
				return true;
			case QEvent::Paint:
				std::cout<<"Paint event"<<std::endl;
				return QWindow::event(event);
			case QEvent::Expose:
				std::cout<<"Expose event"<<std::endl;
				return QWindow::event(event);
			default:
				return QWindow::event(event);
			}
	}

	void exposeEvent(QExposeEvent *event) override{
		if (isExposed()) renderNow();
	}
	// TODO not called
	void closeEvent(QCloseEvent *evt) {
		std::cout<<"TODO closing QtBlenderWindow"<<std::endl;
		evt->accept();
		MenderApplication->quit();
		throw "never reached";
	}

};


#define QT_Window QtBlenderWindow

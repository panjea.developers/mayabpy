/* SPDX-License-Identifier: GPL-2.0-or-later */

/** \file
 * \ingroup GHOST
 * Declaration of GHOST_DisplayManagerQT class.
 */

#pragma once

#include "GHOST_DisplayManager.h"

struct QT_DisplayMode{
    int w;
    int h;
    int refresh_rate;
    int format;
};

class GHOST_SystemQT;

class GHOST_DisplayManagerQT : public GHOST_DisplayManager {
 public:
  GHOST_DisplayManagerQT(GHOST_SystemQT *system);

  GHOST_TSuccess getNumDisplays(uint8_t &numDisplays) const;

  GHOST_TSuccess getNumDisplaySettings(uint8_t display, int32_t &numSettings) const;

  GHOST_TSuccess getDisplaySetting(uint8_t display,
                                   int32_t index,
                                   GHOST_DisplaySetting &setting) const;

  GHOST_TSuccess getCurrentDisplaySetting(uint8_t display, GHOST_DisplaySetting &setting) const;

  GHOST_TSuccess getCurrentDisplayModeQT(QT_DisplayMode &mode) const;

  GHOST_TSuccess setCurrentDisplaySetting(uint8_t display, const GHOST_DisplaySetting &setting);

 private:
  GHOST_SystemQT *m_system;
  QT_DisplayMode m_mode;
};

/* SPDX-License-Identifier: GPL-2.0-or-later */

/** \file
 * \ingroup GHOST
 * Declaration of GHOST_SystemQT class.
 */

#pragma once

#include "../GHOST_Types.h"
#include "GHOST_DisplayManagerQT.h"
#include "GHOST_Event.h"
#include "GHOST_System.h"
#include "GHOST_TimerManager.h"
#include "GHOST_WindowQT.h"

//#include <QtGui/QWindow>
//#define QT_Window QWindow
#include "GHOST_QT.h"

struct QT_Event{

};
//void QT_Quit(){};

class GHOST_WindowQT;

class GHOST_SystemQT : public GHOST_System {
 public:
 	uint64_t m_start_time;
 	GHOST_WindowQT* m_current_window=nullptr;

	void addDirtyWindow(GHOST_WindowQT *bad_wind);

	GHOST_SystemQT();
	~GHOST_SystemQT();

	bool processEvents(bool waitForEvent);

	bool setConsoleWindowState(GHOST_TConsoleWindowState /*action*/)
	{
		return false;
	}

	GHOST_TSuccess getModifierKeys(GHOST_ModifierKeys &keys) const;

	GHOST_TSuccess getButtons(GHOST_Buttons &buttons) const;

	char *getClipboard(bool selection) const;

	void putClipboard(const char *buffer, bool selection) const;

	uint64_t getMilliSeconds();

	uint8_t getNumDisplays() const;

	GHOST_TSuccess getCursorPosition(int32_t &x, int32_t &y) const;

	GHOST_TSuccess setCursorPosition(int32_t x, int32_t y);

	void getAllDisplayDimensions(uint32_t &width, uint32_t &height) const;

	void getMainDisplayDimensions(uint32_t &width, uint32_t &height) const;

	GHOST_IContext *createOffscreenContext(GHOST_GLSettings glSettings);

	GHOST_TSuccess disposeContext(GHOST_IContext *context);

 private:
	GHOST_TSuccess init();

	GHOST_IWindow *createWindow(const char *title,
															int32_t left,
															int32_t top,
															uint32_t width,
															uint32_t height,
															GHOST_TWindowState state,
															GHOST_TDrawingContextType type,
															GHOST_GLSettings glSettings,
															const bool exclusive = false,
															const bool is_dialog = false,
															const GHOST_IWindow *parentWindow = NULL);

	/* QT specific */
	GHOST_WindowQT *findGhostWindow(QT_Window *sdl_win);

	bool generateWindowExposeEvents();

	void processEvent(QT_Event *sdl_event);

	/** The vector of windows that need to be updated. */
	std::vector<GHOST_WindowQT *> m_dirty_windows;
};

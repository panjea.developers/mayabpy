#!/usr/bin/bash
cd blender/build_files/build_environment/
./install_deps.sh --skip-boost --skip-tbb --skip-ocio --skip-openexr --skip-oiio --skip-llvm --skip-osl --skip-osd --skip-openvdb --skip-ffmpeg --skip-opencollada --skip-alembic --skip-embree --skip-oidn --skip-usd --skip-xr-openxr
#--skip-level-zero --skip-imath
#E: Unable to locate package libdecor-0-dev
#E: Unable to locate package libpystring-dev
#ERROR! apt-get failed to install requested packages, exiting.

## ubuntu 20 notes: if you get these errors, then you have to manually hack install_deps.sh
## and remove these: libwayland-dev libdecor-0-dev wayland-protocols libegl-dev libpystring-dev

## note: this works in ubuntu 20
#sudo apt-get install gawk cmake cmake-curses-gui build-essential libjpeg-dev libpng-dev libtiff-dev \
#             git libfreetype6-dev libfontconfig-dev libx11-dev flex bison libxxf86vm-dev \
#             libxcursor-dev libxi-dev wget libsqlite3-dev libxrandr-dev libxinerama-dev \
#             libxkbcommon-dev libdbus-1-dev linux-libc-dev \
#             libbz2-dev libncurses5-dev libssl-dev liblzma-dev libreadline-dev \
#             libopenal-dev libepoxy-dev yasm \
#             libsdl2-dev libfftw3-dev patch bzip2 libxml2-dev libtinyxml-dev libjemalloc-dev \
#             libgmp-dev libpugixml-dev libpotrace-dev libhpdf-dev libzstd-dev  \
#             libglfw3-dev

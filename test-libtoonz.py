#!/usr/bin/env python3

import os, sys, ctypes, subprocess, time

try:
	import bpy
except:
	bpy = None

if sys.platform=='darwin':
	ext = '.dylib'
else:
	## note install_name_tool is apple only
	ext = '.so'

#https://developer.apple.com/forums/thread/129795
#OSError: dlopen(libOpenToonz.dylib, 6): no suitable image found.  Did find:
#	file system relative paths not allowed in hardened programs
if sys.platform=='darwin':
	BLENDER = './buildblenderqt/bin/Blender.app/Contents/MacOS/Blender'
else:
	BLENDER = './buildblenderqt/bin/blender'

if '--blender-not-qt' in sys.argv:
	BLENDER = '/usr/bin/blender'
	if os.path.isfile( os.path.expanduser('~/blender-3.3.0-linux-x64/blender') ):
		BLENDER = os.path.expanduser('~/blender-3.3.0-linux-x64/blender')
	elif os.path.isfile( os.path.expanduser('~/blender-3.0.0-linux-x64/blender') ):
		BLENDER = os.path.expanduser('~/blender-3.0.0-linux-x64/blender')
	elif os.path.isfile('/Applications/Blender.app/Contents/MacOs/Blender'):
		BLENDER = '/Applications/Blender.app/Contents/MacOs/Blender'
	elif os.path.isfile( 'C:/Program Files/Blender Foundation/Blender 3.0/blender.exe' ):
		BLENDER = 'C:/Program Files/Blender Foundation/Blender 3.0/blender.exe'


def test_blender():
	assert os.path.isfile(BLENDER)
	cmd = [
		#'lldb',
		#'--',
		BLENDER, 
		#'--background', 
		'--python', __file__
	]
	print(cmd)
	time.sleep(1)
	subprocess.check_call(cmd)

if '--blender' in sys.argv:
	assert bpy is None
	test_blender()
	sys.exit()


OPENTOONZ_BUILD = './buildtoonz'
OPENTOONZ_PATCHED = './libtoonz'


def loadlib(lib):
	return ctypes.CDLL(lib.split('/')[-1])

if False:
	os.system(
		'cp -Rv /Applications/OpenToonz/OpenToonz_stuff/* %s/portablestuff/.' %OPENTOONZ_PATCHED
	)


## do this before os.chdir
libtoonz_path = os.path.abspath(os.path.join(OPENTOONZ_PATCHED,'libOpenToonz'+ext))
print(libtoonz_path)
assert os.path.isfile(libtoonz_path)

libtoonz = None
def start_toonz(*args):
	global libtoonz
	if libtoonz:
		return None
	os.chdir( OPENTOONZ_PATCHED )
	os.environ['TOONZROOT']='/tmp/portablestuff'
	#libtoonz = ctypes.CDLL('libOpenToonz'+ext)
	libtoonz = ctypes.CDLL(libtoonz_path)
	print(libtoonz)
	if bpy:
		bpy.app.timers.register(toonz_main_blender, first_interval=1)

	return None



def toonz_main_blender():
	run_toonz(mode=1)
	bpy.app.timers.register(toonz_update, first_interval=25, persistent=True)
	return None

def run_toonz(mode=0):
	if not libtoonz:
		start_toonz()
	libtoonz.opentoonz_main(mode)

def toonz_update():
	print('opentoonz update...')
	libtoonz.opentoonz_update()
	print('opentoonz update OK')
	return 0.3

if bpy:
	#import threading
	#threading._start_new_thread(run_toonz, tuple([])) ## this crashes

	#run_toonz(mode=1)
	#bpy.app.timers.register(toonz_update, first_interval=1, persistent=True)
	#bpy.app.timers.register(start_toonz, first_interval=5)

	## this fails
	#bpy.app.handlers.frame_change_pre.append(start_toonz)

	## this works
	#from multiprocessing import Process
	#proc = Process(target=run_toonz)
	#proc.start()

	## note: no extra startup required when inside of blender,
	## because libOpenToonz is linked into the compile step of blender now.
	#os.chdir( OPENTOONZ_PATCHED )
	#os.environ['TOONZROOT']='/tmp/portablestuff'
	print('opentoonz should be running')

else:
	start_toonz()
	#run_toonz(0)  ## this works
	## and this works
	run_toonz(1)
	time.sleep(1)
	while True:
		toonz_update()



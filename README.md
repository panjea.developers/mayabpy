# mayabpy

Blender ported to Qt,
and someday a C++ API for Blender internal.


## Getting Started

This compiles mypaint, opentoonz, and blender into a single exe
```bash
cd mayabpy
./makebpy.py --mypaint
./makebpy.py --toonz
./makebpy.py --qt --test

```

## Blender WASM

requires emscripten sdk. 
note the build will fail at known steps, and must be restarted like this:
```bash
cd mayabpy
./makebpy.py --wasm
./makebpy.py --wasm-dna
./makebpy.py --wasm
./makebpy.py --wasm-rna
./makebpy.py --wasm

```


## Maya Blender Plugin
Requires Maya2023 and HomeBrew

## OSX Install

```bash
cd mayabpy
./makebpy.py
```

## OSX Testing
```bash
cd mayabpy
./test-helloworld.sh

```

## OSX Run
```bash
cd mayabpy
./mayabpy.sh
```

## License
MIT License.

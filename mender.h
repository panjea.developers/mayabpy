#pragma once
#include <string>
#include <vector>
#include <sstream>

const char* GEN_STROKE = R"MARK(
import bpy
def mender_stroke_update(s):
	print(s)
	#if 'Cube' in bpy.data.objects.keys():
	#	bpy.data.objects['Cube'].scale=[0,0,0]
	#bpy.ops.object.gpencil_add(type='EMPTY')  ## segfaults CTX_wm_manager
	#layer = bpy.context.active_object.data.layers[0]

	g = bpy.data.grease_pencils.new(name='tnzdata')
	ob = bpy.data.objects.new(name='tnz', object_data=g)
	bpy.context.collection.objects.link( ob )

	#lframe = layer.frames[0]
	flayer = g.layers.new('lines')
	lframe = flayer.frames.new( 1 )

	bstr   = lframe.strokes.new()
	assert len(bstr.points)==0
	n = len(s)
	bstr.line_width = 70
	bstr.points.add(count=int(n/2))
	bidx = 0
	for i in range(0, n, 2):
		x = s[i]
		y = s[i+1]
		bstr.points[bidx].co.x = x * 0.05
		bstr.points[bidx].co.z = y * 0.05
		bstr.points[bidx].pressure = 1.0  ## width mult
		bstr.points[bidx].strength = 1.0  ## opacity
		bidx += 1

	mname = 'LINE-TOONZ'
	if mname in bpy.data.materials.keys():
		gp_mat = bpy.data.materials[mname]
	else:
		gp_mat = bpy.data.materials.new(mname)
		bpy.data.materials.create_gpencil_data(gp_mat)

	gp_mat.grease_pencil.color = (0,0,0, 1)
	#g.materials.append(gp_mat)
	ob.data.materials.append(gp_mat)
	#bpy.ops.wm.save_as_mainfile(filepath='/tmp/toonz-test.blend')
	mod = ob.grease_pencil_modifiers.new(name='subd', type='GP_SUBDIV')
	mod.level = 2

)MARK";


std::string test_gen_py_update(std::vector<float> stroke){
	std::stringstream s;
	s << GEN_STROKE;
	s << "mender_stroke_update([";
	for (float v: stroke){
		s << v;
		s << ",";
	}
	s << "])";
	return s.str();
}

// TODO python script to update texture
const char* GEN_TEXTURE = R"MARK(
import bpy, os, math
def ensure_render_mode():
	#for area in bpy.context.screen.areas:
	for window in bpy.context.window_manager.windows:
		for area in window.screen.areas:
			if area.type == 'VIEW_3D':
				for space in area.spaces: # iterate through spaces in current VIEW_3D area
					if space.type == 'VIEW_3D': # check if space is a 3D view
						#if space.shading.type != 'RENDERED':
						#	space.shading.type = 'RENDERED'  ## crashes TODO EEVEE offscreen qt
						if space.shading.color_type != 'TEXTURE':
							space.shading.color_type = 'TEXTURE'


def mender_texture_update(s):
	print('tex update:', s)

	gpath, gname = os.path.split(s)
	if gname not in bpy.data.images:
		img = bpy.data.images.new(name=gname, width=32, height=32)
		img.source = 'FILE'
		img.filepath = s

	bpy.data.images[ gname ].reload()

	if gname not in bpy.data.objects:
		#bpy.ops.mesh.primitive_plane_add()
		#plane = bpy.context.active_object  ## TODO
		plane = bpy.data.objects['Cube']
		plane.name = gname
		#plane.location.y = -0.05
		plane.location.y = 2
		#plane.rotation_euler.z = math.radians(30)
		#plane.rotation_euler.x = math.radians(90)
		#plane.scale = [0,0,0]
		#plane.keyframe_insert(data_path="scale", frame=bpy.context.scene.frame_current-1)
		#plane.scale = [1.35,1,1]
		#plane.scale *= 10
		plane.scale = [9,1.7,9]
		#plane.keyframe_insert(data_path="scale", frame=bpy.context.scene.frame_current)

		mat = bpy.data.materials.new(name=gname)
		mat.use_nodes = True
		#mat.blend_method = "CLIP"
		## note alpha blend works with grease objects when they are set to multiply blend mode
		mat.blend_method = "BLEND"
		#plane.data.materials.append(mat)
		plane.data.materials[0]=mat
		tex = mat.node_tree.nodes.new(type="ShaderNodeTexImage")
		tex.image = bpy.data.images[ gname ]
		shader = mat.node_tree.nodes['Principled BSDF']
		color  = shader.inputs['Base Color']
		mat.node_tree.links.new(color, tex.outputs[0])
		alpha = shader.inputs['Alpha']
		mat.node_tree.links.new(alpha, tex.outputs[1])

	ensure_render_mode()
	print(bpy.context.active_object)


)MARK";

std::string test_gen_py_update_mypaint(std::string info){
	std::stringstream s;
	s << GEN_TEXTURE;
	s << "mender_texture_update('" << info << "')";
	return s.str();
}


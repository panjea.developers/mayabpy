#!/bin/bash
cd ./buildwbpy/source/creator

~/emsdk/upstream/bin/clang++ -target wasm32-unknown-emscripten -fignore-exceptions -fvisibility=default \
-mllvm -combiner-global-alias-analysis=false -mllvm -enable-emscripten-sjlj -mllvm -disable-lsr -DEMSCRIPTEN \
-I/Users/rapter/emsdk/upstream/emscripten/cache/sysroot/include/SDL \
-I/Users/rapter/emsdk/upstream/emscripten/cache/sysroot/include/freetype2 \
--sysroot=$HOME/emsdk/upstream/emscripten/cache/sysroot \
-Xclang -iwithsysroot/include/compat \
-DNDEBUG -DWITH_ASSERT_ABORT -DWITH_HEADLESS -DWITH_OPENGL -D__LITTLE_ENDIAN__ \
-I/Users/rapter/mayabpy/blender/intern/clog -I/Users/rapter/mayabpy/blender/intern/guardedalloc \
-I/Users/rapter/mayabpy/blender/source/blender/blenkernel -I/Users/rapter/mayabpy/blender/source/blender/blenlib \
-I/Users/rapter/mayabpy/blender/source/blender/blenloader -I/Users/rapter/mayabpy/blender/source/blender/depsgraph \
-I/Users/rapter/mayabpy/blender/source/blender/editors/include -I/Users/rapter/mayabpy/blender/source/blender/gpu \
-I/Users/rapter/mayabpy/blender/source/blender/imbuf -I/Users/rapter/mayabpy/blender/source/blender/makesdna \
-I/Users/rapter/mayabpy/blender/source/blender/makesrna -I/Users/rapter/mayabpy/blender/source/blender/render \
-I/Users/rapter/mayabpy/blender/source/blender/windowmanager -I/Users/rapter/mayabpy/blender/intern/mikktspace/. \
-Wall -Wc++20-designator -Wno-tautological-compare -Wno-unknown-pragmas -Wno-char-subscripts -Wno-overloaded-virtual \
-Wno-sign-compare -Wno-invalid-offsetof -Wno-suggest-override \
-fmacro-prefix-map=$HOME/mayabpy/blender/= \
-fmacro-prefix-map=$HOME/mayabpy/buildwbpy/= \
-O2 -DNDEBUG -std=c++17 \
-MD -MT source/creator/CMakeFiles/blender.dir/weblender.cpp.o -MF CMakeFiles/blender.dir/weblender.cpp.o.d \
-c ~/mayabpy/blender/source/creator/weblender.cpp -o CMakeFiles/blender.dir/weblender.cpp.o


# https://lld.llvm.org/WebAssembly.html
#--allow-undefined \

~/emsdk/upstream/bin/wasm-ld \
--unresolved-symbols=report-all --warn-unresolved-symbols \
-o /tmp/blender.wasm \
CMakeFiles/blender.dir/weblender.cpp.o \
CMakeFiles/blender.dir/creator_args.c.o CMakeFiles/blender.dir/creator_signals.c.o \
../../lib/libbf_intern_memutil.a ../../lib/libbf_gpencil.a ../../lib/libbf_blenloader.a \
../../lib/libextern_curve_fit_nd.a ../../lib/libextern_rangetree.a ../../lib/libextern_clew.a \
../../lib/libbf_geometry.a ../../lib/libbf_intern_iksolver.a ../../lib/libbf_intern_opencolorio.a \
../../lib/libbf_blenkernel.a ../../lib/libbf_nodes_texture.a ../../lib/libbf_dna.a ../../lib/libbf_dna_blenlib.a \
../../lib/libextern_lzma.a ../../lib/libbf_ikplugin.a ../../lib/libbf_blenlib.a ../../lib/libbf_intern_guardedalloc.a \
../../lib/libbf_modifiers.a ../../lib/libbf_bmesh.a ../../lib/libbf_nodes.a ../../lib/libbf_gpencil_modifiers.a \
../../lib/libbf_intern_eigen.a ../../lib/libextern_hipew.a ../../lib/libbf_intern_libc_compat.a ../../lib/libextern_cuew.a \
../../lib/libextern_minilzo.a ../../lib/libbf_stl.a ../../lib/libbf_intern_clog.a \
../../lib/libbf_intern_sky.a ../../lib/libbf_depsgraph.a ../../lib/libbf_functions.a ../../lib/libbf_io_common.a \
../../lib/libbf_nodes_function.a ../../lib/libextern_wcwidth.a ../../lib/libbf_nodes_geometry.a ../../lib/libbf_imbuf.a \
../../lib/libbf_blenloader.a ../../lib/libbf_geometry.a ../../lib/libbf_blenkernel.a ../../lib/libbf_nodes_texture.a \
../../lib/libbf_modifiers.a ../../lib/libbf_bmesh.a ../../lib/libbf_nodes.a ../../lib/libbf_depsgraph.a \
../../lib/libbf_nodes_shader.a ../../lib/libbf_blenloader.a ../../lib/libbf_geometry.a ../../lib/libbf_blenkernel.a \
../../lib/libbf_nodes_texture.a ../../lib/libbf_modifiers.a ../../lib/libbf_bmesh.a ../../lib/libbf_nodes.a \
../../lib/libbf_depsgraph.a ../../lib/libbf_nodes_geometry.a ../../lib/libbf_imbuf.a ../../lib/libbf_nodes_shader.a \
../../lib/libextern_lzma.a ../../lib/libbf_ikplugin.a ../../lib/libbf_intern_iksolver.a ../../lib/libbf_gpencil_modifiers.a \
../../lib/libextern_minilzo.a ../../lib/libbf_intern_clog.a ../../lib/libbf_blenfont.a \
../../lib/libbf_intern_ghost.a ../../lib/libbf_intern_libmv.a \
../../lib/libbf_intern_opensubdiv.a ../../lib/libbf_rna.a ../../lib/libbf_dna.a \
../../lib/libextern_rangetree.a ../../lib/libbf_nodes_function.a ../../lib/libbf_nodes_composite.a \
../../lib/libbf_intern_memutil.a ../../lib/libbf_intern_opencolorio.a ../../lib/libbf_intern_sky.a \
../../lib/libbf_functions.a ../../lib/libbf_blenlib.a ../../lib/libbf_intern_guardedalloc.a \
../../lib/libbf_intern_eigen.a ../../lib/libbf_intern_libc_compat.a ../../lib/libextern_wcwidth.a \
-L$HOME/emsdk/upstream/emscripten/cache/sysroot/lib/wasm32-emscripten \
~/emsdk/upstream/emscripten/cache/sysroot/lib/wasm32-emscripten/libfreetype.a \
~/emsdk/upstream/emscripten/cache/sysroot/lib/wasm32-emscripten/libpng.a \
~/emsdk/upstream/emscripten/cache/sysroot/lib/wasm32-emscripten/libz.a \
~/emsdk/upstream/emscripten/cache/sysroot/lib/wasm32-emscripten/libjpeg.a \
-lGL-webgl2-full_es3 -lal -lhtml5 -lstubs -lnoexit -lc -ldlmalloc -lcompiler_rt -lc++-noexcept -lc++abi-noexcept \
-lsockets -mllvm -combiner-global-alias-analysis=false -mllvm -enable-emscripten-sjlj -mllvm -disable-lsr \
--import-undefined --strip-debug --export-if-defined=main --export-if-defined=__start_em_asm --export-if-defined=__stop_em_asm --export-if-defined=__start_em_js --export-if-defined=__stop_em_js --export-if-defined=__main_argc_argv \
--export=stackSave --export=stackRestore --export=stackAlloc --export=__wasm_call_ctors --export=__errno_location \
--export=__get_temp_ret --export=__set_temp_ret --export=malloc --export=__cxa_is_pointer_type --export=__dl_seterr \
--export=emscripten_builtin_memalign --export=free --export=saveSetjmp --export=setThrew \
--export=blender_main \
--export-table -z stack-size=5242880 --initial-memory=16777216 --no-entry --max-memory=16777216 --global-base=1024


ls -lh /tmp/blender.wasm
cd /tmp
gzip -c blender.wasm > blender.wasm.gz
ls -lh blender.wasm.gz

ls -l /tmp/blender.wasm
ls -l blender.wasm.gz


/* SPDX-License-Identifier: GPL-2.0-or-later
 * Copyright 2001-2002 NaN Holding BV. All rights reserved. */

/** \file
 * \ingroup creator
 */
#include <thread>
#include <stdlib.h>
#include <string.h>

#include "MEM_guardedalloc.h"
#include "CLG_log.h"
#include "DNA_genfile.h"

#include "BLI_string.h"
#include "BLI_system.h"
#include "BLI_task.h"
#include "BLI_threads.h"
#include "BLI_utildefines.h"

/* Mostly initialization functions. */
#include "BKE_appdir.h"
#include "BKE_blender.h"
#include "BKE_brush.h"
#include "BKE_cachefile.h"
#include "BKE_callbacks.h"
#include "BKE_context.h"
#include "BKE_global.h"
#include "BKE_gpencil_modifier.h"
#include "BKE_idtype.h"
#include "BKE_main.h"
#include "BKE_material.h"
#include "BKE_modifier.h"
#include "BKE_node.h"
#include "BKE_particle.h"
#include "BKE_shader_fx.h"
#include "BKE_sound.h"
#include "BKE_vfont.h"
#include "BKE_volume.h"

//#include "BLI_args.h"

#include "DEG_depsgraph.h"
#include "IMB_imbuf.h" /* For #IMB_init. */
#include "RE_engine.h"
#include "RE_texture.h"
#include "ED_datafiles.h"
#include "RNA_define.h"

#include <signal.h>

#ifdef WITH_BINRELOC
	#include "binreloc.h"
#endif

#include "creator_intern.h" /* Own include. */
#include <emscripten/emscripten.h>

extern "C" EMSCRIPTEN_KEEPALIVE int blender_main(int argc, char **argv){
	std::cout << "hello world weblender" << std::endl;
	bContext* C = CTX_create();
	std::cout << C << std::endl;

	/* Initialize path to executable. */
	std::cout<<"blender: BKE_appdir_program_path_init..." << std::endl;
	BKE_appdir_program_path_init(argv[0]);
	std::cout<<"blender: BLI_threadapi_init ..." << std::endl;
	BLI_threadapi_init();
	std::cout<<"blender: DNA_sdna_current_init ..." << std::endl;
	DNA_sdna_current_init();
	std::cout<<"blender: BKE_blender_globals_init ..." << std::endl;
	BKE_blender_globals_init(); /* blender.c */

	BKE_idtype_init();
	BKE_cachefiles_init();
	BKE_modifier_init();
	BKE_gpencil_modifier_init();
	BKE_shaderfx_init();
	BKE_volumes_init();
	DEG_register_node_types();

	BKE_brush_system_init();
	RE_texture_rng_init();

	BKE_callback_global_init();

	/* Using preferences or user startup makes no sense for #WITH_PYTHON_MODULE. */
	G.factory_startup = true;

	/* After parsing #ARG_PASS_ENVIRONMENT such as `--env-*`,
	 * since they impact `BKE_appdir` behavior. */
	BKE_appdir_init();

	/* After parsing number of threads argument. */
	BLI_task_scheduler_init();

	/* Initialize sub-systems that use `BKE_appdir.h`. */
	std::cout<<"blender: sub systems init ..." << std::endl;
	IMB_init();

	std::cout<<"blender: RNA_init ..." << std::endl;

	RNA_init();

	RE_engines_init();
	BKE_node_system_init();
	BKE_particle_init_rng();
	/* End second initialization. */

	/* Python module mode ALWAYS runs in background-mode (for now). */
	G.background = true;

	//BKE_vfont_builtin_register(datatoc_bfont_pfb, datatoc_bfont_pfb_size);

	/* Initialize FFMPEG if built in, also needed for background-mode if videos are
	 * rendered via FFMPEG. */
	//BKE_sound_init_once();
	std::cout<<"blender: BKE_materials_init ..." << std::endl;

	BKE_materials_init();


}



#!/bin/bash

./makebpy.py --maya-plugin

cp -v QTmender.py /tmp/mender.py
mkdir /tmp/mender_plugin
mkdir /tmp/mender_plugin/plug-ins
mkdir /tmp/mender_plugin/scripts
mkdir /tmp/mender_plugin/icons

cp -Rv bpy_maya_plugin/* /tmp/mender_plugin/plug-ins/.
mv -v /tmp/mender_plugin/plug-ins/__init__.so /tmp/mender_plugin/plug-ins/mender.bundle
cp -Rv Maya.env ~/Library/Preferences/Autodesk/maya/2023/.
#/Applications/Autodesk/maya2023/Maya.app/Contents/MacOS/mayapy /tmp/mayabpy.py
/Applications/Autodesk/maya2023/Maya.app/Contents/MacOS/Maya -noAutoloadPlugins -script `pwd`/test-mender.mel